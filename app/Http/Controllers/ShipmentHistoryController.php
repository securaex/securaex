<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\BranchPincode;
use DB;

use Excel;

class ShipmentHistoryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //

    public function index(){
		return view('shipments.importExport');
    }


	/**
     * Return View file
     *
     * @var array
     */
	public function importExport()
	{
		return view('consignments.importExport');
	}

	/**
     * File Export Code
     *
     * @var array
     */
	public function downloadExcel(Request $request, $type)
	{
		$data = Consignment::get()->toArray();
		return Excel::create('consignments_api_data', function($excel) use ($data) {
			$excel->sheet('data', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download($type);
	}

	/**
     * Import file into database Code
     *
     * @var array
     */
	public function importExcel(Request $request)
	{

		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();

			$data = Excel::load($path, function($reader) {})->get();

			if(!empty($data) && $data->count()){

				$mydata[] = $data->toArray(); 						
				$ids = array();
				foreach ($mydata as $key => $value) {
					
					if(!empty($value)){
						
						foreach ($value as $v) {
							// Build an ids array 
							array_push($ids, $v['awb']);
						}

					}

				}

		        $columns = [
		        'awb',
		        'order_id',
		        'item_description',
		        'weight', 
		        DB::raw('"" as shipment_type'),
		        'pcs',
		        'collectable_value',
		        DB::raw('"" as shipment_value'),
		        DB::raw('"" as origin_code'),
		        DB::raw('"" as destination_code'),
		        DB::raw('"" as shipper_name'),
		        'consignee',
		        'consignee_address',
		        'mobile',
		        'data_received_date',
		        'current_status',
                        'remarks',
        		'updated_at',
		        'remarks',
		        DB::raw('"" as received_by'),
		        DB::raw('"" as delivery_date'),
		        'rto_awb',
		        DB::raw('"" as rto_status')
		        ];


				$data = Consignment::whereIn('awb', $ids)->get($columns);
		        return Excel::create('consignments_data', function($excel) use ($data) {
		            $excel->sheet('data', function($sheet) use ($data)
		            {
		                //$sheet->fromArray($data);
		                
		                $sheet->fromArray($data, null, 'A1', false, false);
		                // Add before first row

		                $sheet->prependRow(1, array(
		                    'Air waybill No', 
		                    'Reference No',
		                    'Item Description',
		                    'Weight',
		                    'Shipment Type',
		                    'Quantity',
		                    'COD Amount',
		                    'Declare Value',
		                    'Origin',
		                    'Destination',
		                    'Shipper ( Client Name )',
		                    'Consignee Name',
		                    'Consignee Address',
		                    'Consignee Contact',
		                    'Process Date',
		                    'Status',
                                    'Remarks',
		                    'Updated Date',
		                    'Reason',
		                    'Received by',
		                    'Delivery Date & Time',
		                    'RTO AWB Number',
		                    'RTO Status'
		                ));
		                

		        $sheet->cells('A1:AH1', function($cells) {

		            // manipulate the range of cells
		                   
		        // Set font
		        $cells->setFont(array(
		            'family'     => 'Calibri',
		            'size'       => '11',
		            'bold'       =>  true,
		            'background' => 'blue'
		        ));    

		        // Set all borders (top, right, bottom, left)
		        $cells->setBorder('solid', 'none', 'none', 'solid');

		        });


		            });
		        })->download('xls');



			}

		}

		return back()->with('error','Please Check your file, Something is wrong there.');
	}



}
