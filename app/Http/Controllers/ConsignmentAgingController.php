<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Customer;
use App\BranchPincode;
use App\User;
use App\Drs;

use Illuminate\Support\Facades\Input;
use DB;
use Auth;

use Excel;


class ConsignmentAgingController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function regularConsignments(Request $request)
    {
        $customers = User::where('user_type', '5')->get();
        $branches = User::where('user_type', '4')->where('is_searchable','1')->get();
		
		
        $aged = \Carbon\Carbon::now()->subDays(2)->toDateString();
		$aged2 = \Carbon\Carbon::now()->subDays(1)->toDateString();
		$results = Consignment::where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->whereDate('created_at', '<', $aged2);


        			
        // no query executed, just give us a builder

        if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'))->orderBy('created_at', 'asc');

        if($request->has('customer_name'))
             $results = $results->where('customer_name', $request->input('customer_name'))->orderBy('created_at', 'asc');

        if($request->has('current_status'))
             $results = $results->where('current_status', $request->input('current_status'))->orderBy('created_at', 'asc');
			 
		if($request->has('aging'))
		{
			
			if($request->input('aging')=="2 Days")
            	$results = $results->whereDate('created_at', '>=', $aged)->whereDate('created_at', '<', $aged2)->orderBy('created_at', 'asc');
			else
				$results = $results->whereDate('created_at', '<', $aged)->orderBy('created_at', 'asc');
		}
			/*DB::enableQueryLog();
			$results = $results->get();
			dd(DB::getQueryLog());*/
        	$consignments = $results->paginate(10);
		
		return view('consignments-aging.regularConsignments', compact('consignments','customers','branches'));
    }


    public function rtoConsignments(Request $request)
    {
        $customers = User::where('user_type', '5')->get();
        $branches = User::where('user_type', '4')->where('is_searchable','1')->get();
		
		
        $aged = \Carbon\Carbon::now()->subDays(2)->toDateString();
		$aged2 = \Carbon\Carbon::now()->subDays(1)->toDateString();

		DB::enableQueryLog();
		/*$results = Consignment::with('consignment_updates')->whereHas('consignment_updates', function($q){
									$q->where('current_status','Process to be RTO');
								})->where('current_status', 'like', '%RTO%')->where('current_status', 'not like', 'RTO Final Status')->get();
		
		$consIds = ConsignmentUpdate::select('consignment_id')->where('current_status', 'Process to be RTO')->whereDate('created_at', '<', $aged2)->get();
		
		$results = Consignment::whereIn('id', $consIds)->get();*/
		
				$rtoCons = Consignment::with('consignment_updates')->where('current_status', 'like', '%RTO%')->where('current_status', 'not like', 'RTO Final Status')->get();
		$consIds = $rtoDate = array();
		if($request->has('aging'))
		{
			
			if($request->input('aging')=="2 Days")
			{
            	//$results = $results->whereDate('created_at', '>=', $aged)->whereDate('created_at', '<', $aged2)->orderBy('created_at', 'asc');
				foreach($rtoCons as $rtoCon)
					{
						foreach($rtoCon->consignment_updates as $update)
						{
							if($update->current_status == "Process to be RTO")
							{
								if(strtotime($update->created_at) >= strtotime($aged) && strtotime($update->created_at)  < strtotime($aged2))
									$consIds[] = $update->consignment_id;
							}
						}
					}
			}
			else
			{
				//$results = $results->whereDate('created_at', '<', $aged)->orderBy('created_at', 'asc');
				foreach($rtoCons as $rtoCon)
					{
						foreach($rtoCon->consignment_updates as $update)
						{
							if($update->current_status == "Process to be RTO")
							{
								if(strtotime($update->created_at) < strtotime($aged))
									$consIds[] = $update->consignment_id;
							}
						}
					}
			}
		}
		else
		{
			foreach($rtoCons as $rtoCon)
			{
				foreach($rtoCon->consignment_updates as $update)
				{
					if($update->current_status == "Process to be RTO")
					{
						if(strtotime($update->created_at) < strtotime($aged2))
							$consIds[] = $update->consignment_id;
					}
				}
			}
		}

		$results = Consignment::with('consignment_updates')->whereIn('id', $consIds);

        if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'))->orderBy('created_at', 'asc');

        if($request->has('customer_name'))
             $results = $results->where('customer_name', $request->input('customer_name'))->orderBy('created_at', 'asc');

        if($request->has('current_status'))
             $results = $results->where('current_status', $request->input('current_status'))->orderBy('created_at', 'asc');
			 
			/*DB::enableQueryLog();
			$results = $results->get();
			dd(DB::getQueryLog());*/
        	$consignments = $results->paginate(10);
		
		return view('consignments-aging.rtoConsignments', compact('consignments','customers','branches'));
    }

    public function exportExcel(Request $request){
        
			$columns = [
			'awb',
			'order_id',
			'item_description',
			'weight', 
			'pcs',
			'payment_mode',
			'collectable_value',
			'price',
			DB::raw('"" as shipper_name'),
			'consignee',
			'consignee_address',
			'pincode',
			'mobile',
			'data_received_date',
			'branch',
			'current_status',
			'created_at',
			'updated_at',
			'remarks',
			'received_by',
			'delivered_date_time',
			'bag_code',
			'no_of_attempts',
			'rto_awb',
			DB::raw('"" as rto_status'),
			'undelivered_reason',                
			'rto_reason',                
			DB::raw('"" as comment'),
			DB::raw('"" as rto_awb_date'),
			];
			
		$aged = \Carbon\Carbon::now()->subDays(2)->toDateString();
		$aged2 = \Carbon\Carbon::now()->subDays(1)->toDateString();
		$results = Consignment::where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->whereDate('created_at', '<', $aged2);
        // no query executed, just give us a builder


        if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'))->orderBy('created_at', 'asc');

        if($request->has('customer_name'))
             $results = $results->where('customer_name', $request->input('customer_name'))->orderBy('created_at', 'asc');

        if($request->has('current_status'))
             $results = $results->where('current_status', $request->input('current_status'))->orderBy('created_at', 'asc');
			 
		if($request->has('aging'))
		{
			
			if($request->input('aging')=="2 Days")
            	$results = $results->whereDate('created_at', '>=', $aged)->whereDate('created_at', '<', $aged2)->orderBy('created_at', 'asc');
			else
				$results = $results->whereDate('created_at', '<', $aged)->orderBy('created_at', 'asc');
		}	 
			 
			 //DB::enableQueryLog();
			 
     	 $consignments = $results->get();
	  
			
			//dd(DB::getQueryLog());
	  			$dev_boy = $age = array();
			foreach($consignments as $result)
			{
				$temp = Drs::select('delivery_boy')->where('id',$result->drs_id)->first();
				if($temp)
				$dev_boy[] = $temp->delivery_boy;
				else
				$dev_boy[] = "";
				
				$age[] = $result->created_at->diffInDays()." Days";
			}
     		$consignments = $results->get($columns);
			$temps=$consignments->toArray();
			$i=0;
			foreach($temps as &$temp)
			{
				$temp['delivery_boy'] = $dev_boy[$i];
				$temp['Aging'] = $age[$i];
				/*$dateTemp = \Carbon\Carbon::parse($temp['created_at']);
				$temp['created_at'] = $dateTemp->format('d-M-Y');
				
				$dateTemp2 = \Carbon\Carbon::parse($temp['updated_at']);
				$temp['updated_at'] = $dateTemp2->format('d-M-Y');*/
				$i++;
			}
			//echo "<pre>"; print_r($temps); die;
			$data=collect($temps);
			/*echo "<pre>"; print_r($ab); die;
			$data=$temps;*/
			
			return Excel::create('consignments_data', function($excel) use ($data) {
            //$data->chunk(300);																			
            $excel->sheet('data', function($sheet) use ($data)
            {
				
                $sheet->fromArray($data, null, 'A1', false, false);
                // Add before first row

					$sheet->prependRow(1, array(
                    'Air waybill No', 
                    'Reference No',
                    'Item Description',
                    'Weight(kg)',
                    'Quantity',
					'Payment Mode',
                    'COD Amount',
                    'Declared Value',
                    'Shipper ( Client Name )',
                    'Consignee Name',
                    'Consignee Address',
					'Pincode',
                    'Consignee Contact',
                    'Process Date',
					'Branch',
                    'Status',
					'Created Date',
                    'Updated Date',
                    'Remarks',
                    'Received by',
                    'Delivery Date & Time',
                    'Bag Code',
                    'No Of Attempt',
                    'RTO AWB Number',
                    'RTO Status',
                    'Undelivered Reason',                                        
                    'RTO Reason',                    
                    'Comment',
                    'RTO AWB Date',
					'Delivery Boy Name',
					'Age',
                ));
                

				$sheet->cells('A1:AH1', function($cells) {
				
					// manipulate the range of cells
						   
				// Set font
				$cells->setFont(array(
					'family'     => 'Calibri',
					'size'       => '11',
					'bold'       =>  true,
					'background' => 'blue'
				));    
				
				// Set all borders (top, right, bottom, left)
				$cells->setBorder('solid', 'none', 'none', 'solid');

});
            });
        })->download('xls');

    }
	
	
    public function exportExcelRto(Request $request){
        
			$columns = [
			'awb',
			'order_id',
			'item_description',
			'weight', 
			'pcs',
			'payment_mode',
			'collectable_value',
			'price',
			DB::raw('"" as shipper_name'),
			'consignee',
			'consignee_address',
			'pincode',
			'mobile',
			'data_received_date',
			'branch',
			'current_status',
			'created_at',
			'updated_at',
			'remarks',
			'received_by',
			'delivered_date_time',
			'bag_code',
			'no_of_attempts',
			'rto_awb',
			DB::raw('"" as rto_status'),
			'undelivered_reason',                
			'rto_reason',                
			DB::raw('"" as comment'),
			DB::raw('"" as rto_awb_date'),
			];
			
		$aged = \Carbon\Carbon::now()->subDays(2)->toDateString();
		$aged2 = \Carbon\Carbon::now()->subDays(1)->toDateString();
		$results = Consignment::where('current_status', 'like', '%RTO%')->where('current_status', '!=', 'RTO Final Status')->whereDate('created_at', '<', $aged2);
        // no query executed, just give us a builder


        if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'))->orderBy('created_at', 'asc');

        if($request->has('customer_name'))
             $results = $results->where('customer_name', $request->input('customer_name'))->orderBy('created_at', 'asc');

        if($request->has('current_status'))
             $results = $results->where('current_status', $request->input('current_status'))->orderBy('created_at', 'asc');
			 
		if($request->has('aging'))
		{
			
			if($request->input('aging')=="2 Days")
            	$results = $results->whereDate('created_at', '>=', $aged)->whereDate('created_at', '<', $aged2)->orderBy('created_at', 'asc');
			else
				$results = $results->whereDate('created_at', '<', $aged)->orderBy('created_at', 'asc');
		}	 
			 
			 //DB::enableQueryLog();
			 
     	 $consignments = $results->get();
	  
			
			//dd(DB::getQueryLog());
	  			$dev_boy = $age = array();
			foreach($consignments as $result)
			{
				$temp = Drs::select('delivery_boy')->where('id',$result->drs_id)->first();
				if($temp)
				$dev_boy[] = $temp->delivery_boy;
				else
				$dev_boy[] = "";
				
				$age[] = $result->created_at->diffInDays()." Days";
			}
     		$consignments = $results->get($columns);
			$temps=$consignments->toArray();
			$i=0;
			foreach($temps as &$temp)
			{
				$temp['delivery_boy'] = $dev_boy[$i];
				$temp['Aging'] = $age[$i];
				/*$dateTemp = \Carbon\Carbon::parse($temp['created_at']);
				$temp['created_at'] = $dateTemp->format('d-M-Y');
				
				$dateTemp2 = \Carbon\Carbon::parse($temp['updated_at']);
				$temp['updated_at'] = $dateTemp2->format('d-M-Y');*/
				$i++;
			}
			//echo "<pre>"; print_r($temps); die;
			$data=collect($temps);
			/*echo "<pre>"; print_r($ab); die;
			$data=$temps;*/
			
			return Excel::create('consignments_data', function($excel) use ($data) {
            //$data->chunk(300);																			
            $excel->sheet('data', function($sheet) use ($data)
            {
				
                $sheet->fromArray($data, null, 'A1', false, false);
                // Add before first row

					$sheet->prependRow(1, array(
                    'Air waybill No', 
                    'Reference No',
                    'Item Description',
                    'Weight(kg)',
                    'Quantity',
					'Payment Mode',
                    'COD Amount',
                    'Declared Value',
                    'Shipper ( Client Name )',
                    'Consignee Name',
                    'Consignee Address',
					'Pincode',
                    'Consignee Contact',
                    'Process Date',
					'Branch',
                    'Status',
					'Created Date',
                    'Updated Date',
                    'Remarks',
                    'Received by',
                    'Delivery Date & Time',
                    'Bag Code',
                    'No Of Attempt',
                    'RTO AWB Number',
                    'RTO Status',
                    'Undelivered Reason',                                        
                    'RTO Reason',                    
                    'Comment',
                    'RTO AWB Date',
					'Delivery Boy Name',
					'Age',
                ));
                

				$sheet->cells('A1:AH1', function($cells) {
				
					// manipulate the range of cells
						   
				// Set font
				$cells->setFont(array(
					'family'     => 'Calibri',
					'size'       => '11',
					'bold'       =>  true,
					'background' => 'blue'
				));    
				
				// Set all borders (top, right, bottom, left)
				$cells->setBorder('solid', 'none', 'none', 'solid');

});
            });
        })->download('xls');

    }
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function show($id)
    {
        DB::enableQueryLog();
        $consignment = Consignment::with('consignment_updates')->where('id', $id)->first();
        //$consignment = Consignment::findorFail($id)->consignmentupdates;
        //echo "<pre>"; print_r($consignment); die;
		
		if(!$consignment->drs_id || $consignment->drs_id!="")
		{
		 $dev_boy = Drs::select('delivery_boy')->where('id',$consignment->drs_id)->first();
		 return view('consignments.show', compact('consignment', 'dev_boy'));
		}
		else
        return view('consignments.show', compact('consignment'));
        
    }

}
