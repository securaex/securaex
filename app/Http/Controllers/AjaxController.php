<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ReverseConsignment;
use App\Drs;
use App\Bag;
use App\ReverseDrs;

class AjaxController extends Controller
{
    //

	public function searchConsignments(Request $request){

		$field = ($request->param == 'drd') ? 'data_received_date' : 'awb';
		$value = $request->keywords;
        $consignments = Consignment::where($field, $request->keywords)->paginate(10);
			$dev_boy = array();
			foreach($consignments as $result)
			{
				$temp = Drs::select('delivery_boy')->where('id',$result->drs_id)->first();
				if($temp)
				$dev_boy[] = $temp->delivery_boy;
				else
				$dev_boy[] = "";
			}
//echo $value; die;
		//$consignment = Consignment::findorFail($request->keyword);
        return view('ajax.consignments-table', compact('consignments', 'dev_boy', 'field', 'value'));

	}

	public function searchReverseConsignments(Request $request){

		$field = ($request->param == 'drd') ? 'data_received_date' : 'awb';
		$value = $request->keywords;
        $consignments = ReverseConsignment::where($field, 'like', '%' . $request->keywords . '%')->paginate(10);
			$dev_boy = array();
			foreach($consignments as $result)
			{
				$temp = ReverseDrs::select('delivery_boy')->where('id',$result->drs_id)->first();
				if($temp)
				$dev_boy[] = $temp->delivery_boy;
				else
				$dev_boy[] = "";
			}

		//$consignment = Consignment::findorFail($request->keyword);
        return view('ajax.reverse-consignments-table', compact('consignments', 'dev_boy', 'field', 'value'));

	}

	/*public function verifyConsignment(Request $request){
		dd($request->keywords);
         $consignments = Consignment::where('awb', 'like', '%' . $request->keywords . '%')->first();
		 $consignments = Consignment::where('awb', $request->keywords)->first();
		echo "faraz"; die;
        if ($consignments) {

        	if($consignments->current_status != \Config::get('constants.softDataupload')):
        	
        		return response()->json(['status' => 'error' ,'message' => 'AWB NO : ' . $request->keywords .  ' is already scanned !']);         	
        	        	
        	endif;

			return response()->json(['status' => 'success' ,'data' => $consignments->toArray()]); 
        
        }else{
			return response()->json(['status' => 'error' ,'message' => 'AWB NO : ' . $request->keywords .  ' not found !']);         	
        }

	}*/
	
	
	public function verifyConsignment(Request $request){
		//dd($request->keywords);
		$awbs = array_map("trim", preg_split("/(\r\n|\n|\r)/",$request->keywords));
		//echo "<pre>"; print_r($awbs); die;
		$founds=$notFounds=$alreadyScanneds=array();
		//$data = array();
		foreach($awbs as $awb)
		{
			 $consignment = Consignment::select('current_status')->where('awb', $awb)->first();
			//echo "faraz"; die;
			if ($consignment) {
	
				if($consignment->current_status != \Config::get('constants.softDataupload'))
					//$alreadyScanneds[]=$awb;
					$alreadyScanneds[] = $awb;
				else
				{
					$result = Consignment::select('id','awb','created_at','customer_name','pincode','branch','payment_mode')->where('awb', $awb)->first();
					$founds[] = $result->toArray();
				}
			}else{
				$notFounds[] = $awb;         	
			}
		}
		
		//echo "<pre>"; print_r($data); die;
		
		return response()->json(['status' => 'success' ,'alreadyScanneds' => $alreadyScanneds ,'founds' => $founds, 'notFounds' => $notFounds]);

	}
	
	public function showbagging(Request $request){

        $consignments = Consignment::where('awb', $request->keywords)->first();
//echo "faraz"; die;
        if ($consignments)
			return response()->json(['status' => 'success' ,'data' => $consignments->toArray()]); 
        else
			return response()->json(['status' => 'error' ,'message' => 'AWB NO : ' . $request->keywords .  ' not found !']);         	
	}
	
	public function searchRtoConsignments(Request $request){

		//$field = ($request->param == 'drd') ? 'data_received_date' : 'awb';
		
		$awbs = preg_split("/(\r\n|\n|\r)/",$request->keywords);

        //$consignments = Consignment::whereIn('awb', $awbs)->where('branch', Auth::user()->branch)->paginate(100);
		$consignments = Consignment::whereIn('awb', $awbs)->orderBy('updated_at', 'DESC')->whereNotNull('rto_awb')->paginate(100);
        return view('ajax.rto-consignments-table', compact('consignments'));
        //return response()->json(['status' => 'error' ,'message' => 'AWB NO : not found !']);

	}
	
	public function getBag(Request $request){

		$date_from = \Carbon\Carbon::today()->toDateString();
		$MyDateCarbon = \Carbon\Carbon::parse($date_from);
		$date_to = $MyDateCarbon->addDays(1)->toDateString();
		
		$bag = Bag::where('to_branch', $request->branch)->whereDate('created_at', '>=' ,$date_from)->whereDate('created_at', '<' ,$date_to)->orderBy('id', 'desc')->first();
		
		if($bag)
		{
			$arr = explode('/',$bag->bag_code);
		}
		else
			$arr=0;
		
		if(count($arr)==4)
		{
			$last = $arr[3]+1;
			$bag_code = "Bag/".$request->branch."/".$date_from."/".$last;
				
		}
		else
			$bag_code = "Bag/".$request->branch."/".$date_from."/1";
		
		$bag_code = str_replace(" ","-",trim($bag_code));
		
		/*Bag::create([
					'bag_code' => $bag_code,
					'to_branch' => $request->branch
				]);*/
		
		//echo $bagCode; die;
       
        return view('ajax.get-bag', compact('bag_code'));

	}	

}




