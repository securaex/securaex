<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use Auth;

class InscanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('inscan.index');                

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if(isset($request->action) && $request->action == 'show'){
        // Get the consignment AWB which has atleast 1 update
        $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $request->txtwaybill)->first();
        }elseif(isset($request->action) && $request->action == 'verify'){
            // Get the consignment AWB which has atleast 1 update
            $status = array();
            $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->with('bag')->where('awb', $request->txtwaybill)->first();
            foreach($consignment->consignment_updates as $update){
                $status[] = $update->current_status;                                        
            }
            if(!in_array(\Config::get('constants.inScanbranch'), $status) && in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
            ConsignmentUpdate::create([
                'consignment_id' => $consignment->id,
                'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                'location' => $consignment->bag->to_branch,
                'last_updated_by' => Auth::user()->username,
                'current_status' => \Config::get('constants.inScanbranch'),
                'remarks' => '',
                'bag_code' => $consignment->bag->bag_code,
                'drs_code' => ''
            ]);

            $consignment = Consignment::findorFail($consignment->id);
            $consignment->update([
                'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                'last_updated_by' => Auth::user()->username,
                'current_status'  => \Config::get('constants.inScanbranch'),
                'prev_status'     => \Config::get('constants.bagVerified'),
                'bag_code'        => $consignment->bag->bag_code,
                'bag_id'          => $consignment->bag->id
            ]);
            
            }

        }


        if($consignment){

                return view('inscan.index', compact('consignment'));

        }else{

                return view('inscan.index', ['awb' => $request->txtwaybill]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
