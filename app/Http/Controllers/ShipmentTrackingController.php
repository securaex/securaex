<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Customer;
use App\BranchPincode;
use App\User;


use Illuminate\Support\Facades\Input;
use DB;
use Auth;

class ShipmentTrackingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	
		public function showData(Request $request)
	{
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }

        $results = Consignment::whereDate('created_at', '=', $date_to)->orderBy('created_at', 'desc'); 
		
        $consignments = $results->paginate(600);
		
		$notOutScanned = Consignment::whereDate('updated_at', '=', $date_to)->where('current_status', '=', 'Soft Data Upload');
		//dd($notOutScanned->count());

        return view('shipment-tracking.showData', compact(array('consignments', 'notOutScanned')));
	}

}
