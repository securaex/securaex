<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Consignment;
use App\ConsignmentUpdate;
use App\OutsideConsignment;
use App\OutsideConsignmentUpdate;
use App\Customer;
use App\BranchPincode;
use App\User;
use App\Drs;
use App\Driver;

use Illuminate\Support\Facades\Input;
use DB;
use Auth;

class BranchWiseReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

		//DB::enableQueryLog();
        $branches = User::where('user_type', '4')->where('is_searchable','1')->get(); 
        if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        //
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }
		DB::enableQueryLog();
		$cons_id = array();
			$consignmentUpdates = ConsignmentUpdate::where('current_status', \Config::get('constants.inScanbranch'))->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->groupBy('consignment_id')->get();
			//dd(DB::getQueryLog());
			
			foreach($consignmentUpdates as $consignmentUpdate)
				$cons_id[] = $consignmentUpdate->consignment_id;
			
			$consignmentUpdates2 = ConsignmentUpdate::whereIn('consignment_id', $cons_id )->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->get();
			
			$consignmentUpdates3 = ConsignmentUpdate::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->get();
			$branchStatus = array();
			$totalShpmtRcvd=0;
			$status=[\Config::get('constants.inScanbranch'), \Config::get('constants.outScanbranch'), \Config::get('constants.deliveryStatus1'), \Config::get('constants.deliveryStatus2'), \Config::get('constants.deliveryStatus3'), \Config::get('constants.reattemptBranch'), \Config::get('constants.rtoProcess'), \Config::get('constants.rtoInitiated'), \Config::get('constants.rtoInitiatedbyBranch'), \Config::get('constants.rtoIntransittoHub'), \Config::get('constants.rtoVerifiedbyHub'), \Config::get('constants.rtoCompletedbyHub'), \Config::get('constants.rtoFinalstatus')];
			
			foreach($branches as $branch)
				{
					$shpmtRcvd=$ofd=$dlvrd=$undlvrd=$cncld=$reattempt=$rto=$ofd2=$dlvrd2=$undlvrd2=$cncld2=0;
					$cons_id=$ofdAr=$dlvrdAr=$undlvrdAr=$cncldAr=$reattemptAr=$rtoAr=$ofd2Ar=$dlvrd2Ar=$undlvrd2Ar=$cncld2Ar=array();
					DB::enableQueryLog();
					$consignments = ConsignmentUpdate::where('location', $branch->branch)->whereIn('current_status',$status)->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->get();
					//dd(DB::getQueryLog());
					foreach($consignments as $consignment)
					{
							//echo $value->current_status."<br>";
							if($consignment->current_status == \Config::get('constants.inScanbranch'))
							{
								$shpmtRcvd++;
								$cons_id[] = $consignment->consignment_id;
							}
							if(!empty($cons_id))
							{
								if(in_array($consignment->consignment_id, $cons_id))
								{
									if($consignment->current_status == \Config::get('constants.outScanbranch'))
									 $ofdAr[] = $consignment->consignment_id;
									if($consignment->current_status == \Config::get('constants.deliveryStatus1'))
									 $dlvrdAr[] = $consignment->consignment_id;
									if($consignment->current_status == \Config::get('constants.deliveryStatus3'))
									 $undlvrdAr[] = $consignment->consignment_id;
									if($consignment->current_status == \Config::get('constants.deliveryStatus2'))
									 $cncldAr[] = $consignment->consignment_id;
								}
							}
							if($consignment->current_status == \Config::get('constants.outScanbranch'))
						     $ofd2Ar[] = $consignment->consignment_id;
						    if($consignment->current_status == \Config::get('constants.deliveryStatus1'))
						     $dlvrd2Ar[] = $consignment->consignment_id;
						    if($consignment->current_status == \Config::get('constants.deliveryStatus3'))
						     $undlvrd2Ar[] = $consignment->consignment_id;
						    if($consignment->current_status == \Config::get('constants.deliveryStatus2'))
						     $cncld2Ar[] = $consignment->consignment_id;
						    if($consignment->current_status == \Config::get('constants.reattemptBranch'))
						     $reattemptAr[] = $consignment->consignment_id;
						    if($consignment->current_status == \Config::get('constants.rtoProcess') || $consignment->current_status == \Config::get('constants.rtoInitiated') || $consignment->current_status == \Config::get('constants.rtoInitiatedbyBranch') || $consignment->current_status == \Config::get('constants.rtoIntransittoHub') || $consignment->current_status == \Config::get('constants.rtoVerifiedbyHub') || $consignment->current_status == \Config::get('constants.rtoCompletedbyHub') || $consignment->current_status == \Config::get('constants.rtoFinalstatus'))
						     $rtoAr[] = $consignment->consignment_id;
					}
					/*echo $branch->branch."<br>";
					
					echo "Shipment Recieved= ".$shpmtRcvd."<br>";
					echo "OFD <pre>"; print_r($ofdAr); 
					echo "Total OFD <pre>"; print_r($ofd2Ar);
					echo "Delivered <pre>"; print_r($dlvrdAr);
					echo "Total Delivered <pre>"; print_r($dlvrd2Ar); 
					echo "Undelivered <pre>"; print_r($undlvrdAr);
					echo "Total Unelivered <pre>"; print_r($undlvrd2Ar);
					echo "Cancelled <pre>"; print_r($cncldAr);
					echo "Total Cancelled <pre>"; print_r($cncld2Ar); die;*/
					
					if(!empty($ofdAr))
						{
							$unique = array_unique($ofdAr);
							$ofd = count($unique);
						}
					else
						$ofd=0;
						
					if(!empty($ofd2Ar))
						{
							$unique = array_unique($ofd2Ar);
							$ofd2 = count($unique);
						}
					else
						$ofd2=0;
						
					if(!empty($dlvrdAr))
						{
							$unique = array_unique($dlvrdAr);
							$dlvrd = count($unique);
						}
					else
						$dlvrd=0;
						
					if(!empty($dlvrd2Ar))
						{
							$unique = array_unique($dlvrd2Ar);
							$dlvrd2 = count($unique);
						}
					else
						$dlvrd2=0;	
						
					if(!empty($undlvrdAr))
						{
							$unique = array_unique($undlvrdAr);
							$undlvrd = count($unique);
						}
					else
						$undlvrd=0;	
						
					if(!empty($undlvrd2Ar))
						{
							$unique = array_unique($undlvrd2Ar);
							$undlvrd2 = count($unique);
						}
					else
						$undlvrd2=0;
						
					if(!empty($cncldAr))
						{
							$unique = array_unique($cncldAr);
							$cncld = count($unique);
						}
					else
						$cncld=0;
						
					if(!empty($cncld2Ar))
						{
							$unique = array_unique($cncld2Ar);
							$cncld2 = count($unique);
						}
					else
						$cncld2=0;	
						
					if(!empty($reattemptAr))
						{
							$unique = array_unique($reattemptAr);
							$reattempt = count($unique);
						}
					else
						$reattempt=0;
						
					if(!empty($rtoAr))
						{
							$unique = array_unique($rtoAr);
							$rto = count($unique);
						}
					else
						$rto=0;	
					
					$branchStatus["$branch->branch"]['Shipment Received'] = $shpmtRcvd;
					$branchStatus["$branch->branch"]['OFD'] = $ofd;
					$branchStatus["$branch->branch"]['Delivered'] = $dlvrd;
					$branchStatus["$branch->branch"]['Undelivered'] = $undlvrd;
					$branchStatus["$branch->branch"]['Cancelled'] = $cncld;
					$branchStatus["$branch->branch"]['Total OFD'] = $ofd2;// - ($dlvrd2 + $undlvrd2 + $cncld2);
					$branchStatus["$branch->branch"]['Total Delivered'] = $dlvrd2;
					$branchStatus["$branch->branch"]['Total Undelivered'] = $undlvrd2;
					$branchStatus["$branch->branch"]['Total Cancelled'] = $cncld2;
					$branchStatus["$branch->branch"]['Reattempt'] = $reattempt;
					$branchStatus["$branch->branch"]['Rto'] = $rto;
					$totalShpmtRcvd=$totalShpmtRcvd+$shpmtRcvd;
					
				}
				//echo "<pre>"; print_r($branchStatus); die;
				$totalOsShpmtRcvd=0;
				foreach($branches as $branch)
				{
					$branchStatus["$branch->branch"]['Outside Shipment Received'] = $temp = OutsideConsignmentUpdate::where('current_status', \Config::get('constants.inScanbranch'))->where('location', $branch->branch)->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->count();
					
					$totalOsShpmtRcvd=$totalOsShpmtRcvd+$temp;
					
					$con_ids = OutsideConsignmentUpdate::select("outside_consignment_id")->where('current_status', \Config::get('constants.inScanbranch'))->where('location', $branch->branch)->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->get();
					
					
					
					$temp = OutsideConsignmentUpdate::select("outside_consignment_id")->whereIn("outside_consignment_id", $con_ids)->where('current_status', \Config::get('constants.outScanbranch'))->groupBy("outside_consignment_id")->get();
					
					$branchStatus["$branch->branch"]['Outside OFD'] = $temp->count();
					
					
					$branchStatus["$branch->branch"]['Total Outside OFD'] = OutsideConsignmentUpdate::where('current_status', \Config::get('constants.outScanbranch'))->where('location', $branch->branch)->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->count();
					
					
					$temp = OutsideConsignmentUpdate::select("outside_consignment_id")->whereIn("outside_consignment_id", $con_ids)->where('current_status', \Config::get('constants.deliveryStatus1'))->groupBy("outside_consignment_id")->get();
					//dd(DB::getQueryLog());
					$branchStatus["$branch->branch"]['Outside Delivered'] = $temp->count();
					
					
					$branchStatus["$branch->branch"]['Total Outside Delivered'] = OutsideConsignmentUpdate::where('current_status', \Config::get('constants.deliveryStatus1'))->where('location', $branch->branch)->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->count();
					
					
					$temp = OutsideConsignmentUpdate::select("outside_consignment_id")->whereIn("outside_consignment_id", $con_ids)->where('current_status', \Config::get('constants.deliveryStatus3'))->groupBy("outside_consignment_id")->get();
					//dd(DB::getQueryLog());
					$branchStatus["$branch->branch"]['Outside Undelivered'] = $temp->count();
					
					
					$branchStatus["$branch->branch"]['Total Outside Undelivered'] = OutsideConsignmentUpdate::where('current_status', \Config::get('constants.deliveryStatus3'))->where('location', $branch->branch)->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->count();
					
					$branchStatus["$branch->branch"]['In Stock'] = OutsideConsignment::where('current_status', \Config::get('constants.deliveryStatus3'))->where('branch', $branch->branch)->count();
				}
			//echo "<pre>";print_r($branchStatus); die;
				//$prevUrl = $request->fullUrl();
				//echo $url; die;

        return view('branchwise-report.index', compact('branchStatus','branches', 'totalShpmtRcvd', 'totalOsShpmtRcvd', 'date_from', 'date_to'));                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function showDetails(Request $request)
    {

		DB::enableQueryLog();
        $branch = $request->branch;
		$date_from = $request->date_from;
		$date_to = $request->date_to;
		DB::enableQueryLog();
		$drivers = Driver::select('driver_name')->where('branch',$branch)->get();
		$totalDrivers = count($drivers);
		$drs = Drs::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->get();
		
		$consignments = Consignment::where('drs_code','!=','')->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->get();
		//dd(DB::getQueryLog());
		$driver_names = array();
		foreach($drivers as $driver)
		{
			$totalDrs=$totalConsignments=$ofd=$dlvrd=$undlvrd=$cncld=0;
			foreach($drs as $value)
			{
				if($value->delivery_boy == $driver->driver_name)
				{
					$totalDrs++;
					foreach($consignments as $consignment)
					{
						
						if($consignment->drs_code == $value->drs_code)
						{
							$totalConsignments++;
							if($consignment->current_status == \Config::get('constants.outScanbranch'))
								$ofd++;  
							 if($consignment->current_status == \Config::get('constants.deliveryStatus1'))
								$dlvrd++;
							 if($consignment->current_status == \Config::get('constants.deliveryStatus2'))
								$cncld++;
							 if($consignment->current_status == \Config::get('constants.deliveryStatus3'))
								$undlvrd++;	
						}
					}
					$driver_names["$driver->driver_name"]['totalOfd']=$ofd;
					$driver_names["$driver->driver_name"]['totalDelivered']=$dlvrd;
					$driver_names["$driver->driver_name"]['totalUndelivered']=$undlvrd;
					$driver_names["$driver->driver_name"]['totalCancelled']=$cncld;

				}

			}
			
			if($totalDrs == 0)
			{
				$driver_names["$driver->driver_name"]['totalDrs']=0;
				$driver_names["$driver->driver_name"]['totalConsignments']=0;
				$driver_names["$driver->driver_name"]['totalOfd']=0;
				$driver_names["$driver->driver_name"]['totalDelivered']=0;
				$driver_names["$driver->driver_name"]['totalUndelivered']=0;
				$driver_names["$driver->driver_name"]['totalCancelled']=0;
			}
			else
			{
				$driver_names["$driver->driver_name"]['totalDrs']=$totalDrs;
				$driver_names["$driver->driver_name"]['totalConsignments']=$totalConsignments;
			}
			

		}
		
		return view('branchwise-report.showDetails', compact('branch','driver_names','totalDrivers'));
		
	}
}
