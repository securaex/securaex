<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\User;
use Auth;
use Excel;


class AuditController extends Controller
{
	
	public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		
		$from_date = $request->input('from_date');
		$from_date_minus = date('Y-m-d', strtotime('-1 day', strtotime($from_date)));
		
		$to_date = $request->input('to_date');
		$to_date_minus = date('Y-m-d', strtotime('-1 day', strtotime($to_date)));
		
		//select username from users where user_type = '4' and is_searchable = '1' order by username;
		$data_array = array();
		$branches = \DB::table('users')
						 ->select('username')                          // Get All the branches
						 ->where('user_type', '4')
						 ->where('is_searchable', '1')
						 ->orderBy('username')
						 ->get();
		foreach($branches as $row)
		{
			$data_array[strtolower($row->username)]['branch'] = $row->username;             // create an array for scanning data
			$data_array[strtolower($row->username)]['awb_to_be_scanned'] = 0;
			$data_array[strtolower($row->username)]['awb_scanned'] = 0;
		}
		
		$counts = array();
		$counts['awb_scanned'] = 0;
		$counts['awb_to_be_scanned'] = 0;
		$counts['left_to_be_scan'] = 0;
		
		if(!empty($from_date) && !empty($to_date))
		{
			$start = date($from_date);
            $end = date($to_date);
            
            $to = \Carbon\Carbon::createFromFormat('Y-m-d', $to_date);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d', $from_date);
            $days = $to->diffInDays($from);

			
			if($days == 0)
			{
			  $awb_to_be_scanned = \DB::table('inventory_scan')                
									->select('branch', \DB::raw('count(distinct awb) as awb_to_be_scanned'))
									->where(\DB::raw('date(created_at)'), '>=', $from_date)
									->where(\DB::raw('date(created_at)'), '<=', $to_date)
									->groupBy('branch')
									->get();  				
															 // All Shipments which are to be scanned branch wise for current date
					 
			  $awb_scanned = \DB::table('inventory_scan')      
									->select('branch', \DB::raw('count(distinct awb) as awb_scanned'))
									->where(\DB::raw('date(created_at)'), '>=', $from_date)
									->where(\DB::raw('date(created_at)'), '<=', $to_date)
									->where('scanning_status', 'yes')
									->groupBy('branch')
									->get(); 
			}
            else
			{
				$message = "Only Single day Report can be fetched";
				return view('audit.index', compact('message', 'data_array', 'counts'));
			}				
		}
		else
		{  
			$current_date = \Carbon\Carbon::today()->toDateString();
			//$current_date_minus = date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
			
			$awb_to_be_scanned = \DB::table('inventory_scan')                
									->select('branch', \DB::raw('count(distinct awb) as awb_to_be_scanned'))
									->where(\DB::raw('date(created_at)'), $current_date)
									->groupBy('branch')
									->get();  				
															 // All Shipments which are to be scanned branch wise for current date
					 
			$awb_scanned = \DB::table('inventory_scan')      
									->select('branch', \DB::raw('count(distinct awb) as awb_scanned'))
									->where(\DB::raw('date(created_at)'), $current_date)
									->where('scanning_status', 'yes')
									->groupBy('branch')
									->get();                // Shipments which are scanned today branch wise for current date
		}
		
		$scan_data = array();
		
		
		
		foreach($awb_to_be_scanned as $row)
		{
			    $data_array[strtolower($row->branch)]['awb_to_be_scanned'] = $row->awb_to_be_scanned;
				$data_array[strtolower($row->branch)]['awb_scanned'] = 0;
				$counts['awb_to_be_scanned'] = $counts['awb_to_be_scanned'] + $row->awb_to_be_scanned;
		}
		
		foreach($awb_scanned as $row)
		{
			$data_array[strtolower($row->branch)]['awb_scanned'] = $row->awb_scanned;
		    $counts['awb_scanned'] = $counts['awb_scanned'] + $row->awb_scanned;
		}
		
	
		
		$counts['left_to_be_scan'] = $counts['awb_to_be_scanned'] - $counts['awb_scanned'];
	    return view('audit.index', compact('counts', 'data_array', 'from_date', 'to_date'));                
	}

  
  
    
	public function inventory_scanned(Request $request)
	{
		$branch = $request->input('branch');
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		
		if(!empty($branch) && empty($from_date) && empty($to_date))
		{
			$current_date = \Carbon\Carbon::today()->toDateString();
			
			$scanned = \DB::table('inventory_scan')
			                ->where(\DB::raw('date(created_at)'), $current_date)
							->where('branch', $branch)
							->where('scanning_status', 'yes')
							->groupBy('awb')
							->get();		 
				
			return view('audit.inventory_scanned', compact('scanned', 'from_date', 'to_date'));
		}
		elseif(!empty($branch) && !empty($from_date) && !empty($to_date))
		{
		    $scanned  = \DB::table('inventory_scan')
			                ->where(\DB::raw('date(created_at)'), '>=',  $from_date)
							->where(\DB::raw('date(created_at)'), '<=',  $to_date)
							->where('branch', $branch)
							->where('scanning_status', 'yes')
							->groupBy('awb')
							->get();			
							 
			return view('audit.inventory_scanned', compact('scanned', 'from_date', 'to_date'));
		}
	}
  
  
  
    public function inventory_not_scanned(Request $request)
	{
		$branch = $request->input('branch');
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		
		$current_date = \Carbon\Carbon::today()->toDateString();
		
		if(!empty($branch) && empty($from_date) && empty($to_date))
		{
			$not_scanned = \DB::table('inventory_scan')
			                ->where(\DB::raw('date(created_at)'), $current_date)
							->where('branch', $branch)
							->where('scanning_status', 'no')
							->groupBy('awb')
							->get();	
					
		    return view('audit.inventory_not_scanned', compact('not_scanned', 'from_date', 'to_date'));
		}
        elseif(!empty($branch) && !empty($from_date) && !empty($to_date))
		{
			$not_scanned = \DB::table('inventory_scan')
			                ->where(\DB::raw('date(created_at)'), '>=',  $from_date)
							->where(\DB::raw('date(created_at)'), '<=',  $to_date)
							->where('branch', $branch)
							->where('scanning_status', 'no')
							->groupBy('awb')
							->get();			
			 
			return view('audit.inventory_not_scanned', compact('not_scanned', 'from_date', 'to_date'));
		}		
			
		
	}
  
}
