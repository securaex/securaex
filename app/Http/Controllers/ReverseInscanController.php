<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReverseConsignment;
use App\ReverseConsignmentUpdate;
use App\ReverseBag;
use Auth;

class ReverseInscanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('reverse-inscan.index');                

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if(isset($request->action) && $request->action == 'show'){
        // Get the consignment AWB which has atleast 1 update
        $consignment = ReverseConsignment::with('reverse_consignment_updates')->where('awb', $request->txtwaybill)->first();
        }elseif(isset($request->action) && $request->action == 'verify'){
            // Get the consignment AWB which has atleast 1 update
            $status = array();
            $consignment = ReverseConsignment::with('reverse_consignment_updates')->with('reverse_bag')->where('awb', $request->txtwaybill)->first();
            foreach($consignment->reverse_consignment_updates as $update){
                $status[] = $update->current_status;                                        
            }
            if(!in_array(\Config::get('constants.inScanhubRev'), $status) && in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.collected'), $status)){
            ReverseConsignmentUpdate::create([
                'reverse_consignment_id' => $consignment->id,
                'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                'location' => 'New Delhi',
                'last_updated_by' => Auth::user()->username,
                'current_status' => \Config::get('constants.inScanhubRev'),
                'remarks' => '',
                'bag_code' => $consignment->reverse_bag->bag_code,
                'drs_code' => ''
            ]);

            $consignment = ReverseConsignment::findorFail($consignment->id);
            $consignment->update([
                'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                'last_updated_by' => Auth::user()->username,
                'current_status'  => \Config::get('constants.inScanhubRev'),
                'prev_status'     => \Config::get('constants.bagVerified'),
                'reverse_bag_code'        => $consignment->reverse_bag->bag_code,
                'reverse_bag_id'          => $consignment->reverse_bag->id
            ]);
            
            }

        }


        if($consignment){

                return view('reverse-inscan.index', compact('consignment'));

        }else{

                return view('reverse-inscan.index', ['awb' => $request->txtwaybill]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
