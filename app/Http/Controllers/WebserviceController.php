<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Transformer\ConsignmentTransformer;
use App\Transformer\ConsignmentShowTransformer;
use App\Transformer\ReverseConsignmentTransformer;
use App\ReverseConsignment;
use App\ReverseConsignmentUpdate;
use App\BranchPincode;

use Excel;
use Auth;
use DB;

class WebserviceController extends Controller
{
    protected $respose;
 
    public function __construct(Response $response)
    {
        $this->response = $response;
    }
 
    public function index(Request $request)
    {
        //Get all task
		if($request->has('awb'))
		{
			$consignments = Consignment::where('awb', $request->input('awb'))->first();
			// Return a collection of $task with pagination
			if (!$consignments) {
				return $this->response->errorNotFound('Consignment Not Found');
			}
			return $this->response->withItem($consignments, new  ConsignmentTransformer());
		}
		else
			return $this->response->errorNotFound('AWB Number Required');
    }
 
    public function show($id)
    {
        //Get the task
        $consignment = Consignment::find($id);
        if (!$consignment) {
            return $this->response->errorNotFound('Consignment Not Found');
        }
        // Return a single task
        return $this->response->withItem($consignment, new  ConsignmentTransformer());
    }

    public function showAwb($awb)
    {
        //Get the task
        $consignment = Consignment::where('awb',$awb)->with('consignment_updates')->get();
        if (!$consignment) {
            return $this->response->errorNotFound('Consignment Not Found');
        }
        // Return a single task
        return $this->response->withItem($consignment->first(), new  ConsignmentShowTransformer());
    }


    public function destroy($id)
    {
        //Get the task
        $consignment = Consignment::find($id);
        if (!$consignment) {
            return $this->response->errorNotFound('Consignment Not Found');
        }
 
        if($consignment->delete()) {
             return $this->response->withItem($consignment, new  ConsignmentTransformer());
        } else {
            return $this->response->errorInternalError('Could not delete a Consignment');
        }
 
    }
 
    public function store(Request $request)  {
		//echo "<pre>"; print_r($_REQUEST); die;
        if ($request->isMethod('put')) {
            //Get the task
            $consignment = Consignment::find($request->consignment_id);
            if (!$consignment) {
                return $this->response->errorNotFound('Consignment Not Found');
            }
        } else {
			
			
			if(isset($_REQUEST['api_key'])){
				
				if($request->input('api_key') == 'c41c30a32ee11f6e514e' || $request->input('api_key') == 'fwlihgamte9u1xk2by1f' || $request->input('api_key') == 'f881fc1e3cdde340c5e5' || $request->input('api_key') == 'c801fe1e3cdde140c7e9' || $request->input('api_key') == 'c960cd446341146dca99' || $request->input('api_key') == 'b8ziqbazld66hicpebwh' || $request->input('api_key') == 'juckjs5ooo7m1ip26srf' || $request->input('api_key') == 'jOmnDTxa3v8Cwy2HFU8c' || $request->input('api_key') == 'ry7Is6ZSGgfirXo6jHFJ'){
					$consignment = new Consignment;
				}
				else {
					return $this->response->errorNotFound('Invalid API Key');
				}
				
			} else{
			
				return $this->response->errorNotFound('API Key Required');
			}

		}
		if(Consignment::where('customer_name', 'like', $request->input('customer_code'))->where('awb', 'like', $request->input('awb'))->exists())
			return $this->response->errorNotFound('Duplicate Entry.');
		
        $awb = $request->input('awb');//die;
		
        $pincode = $request->input('consignee_pincode');
        if(isset($awb) && !empty($awb)) $check_branch = BranchPincode::where('pincode', $pincode)->first();    
		
		/*if(isset($check_branch)){
            $check_branch = $check_branch->branch;
            if($check_branch == '' || $check_branch == 'ODA') $check_branch = 'Other';
        } else {
            $check_branch = 'Other';
        }*/
		
		if(isset($check_branch)){
            $check_branch = $check_branch->branch;
            if($check_branch == '' || $check_branch == 'ODA') 
				return $this->response->errorInternalError('Error. Pincode not found.');
        } else {
            return $this->response->errorInternalError('Error. Pincode not found.');
        }

        //$consignment->id = $request->input('consignment_id');
        $consignment->awb = $request->input('awb');
        $consignment->data_received_date = \Carbon\Carbon::parse($request->input('shipment_datedd_mm_yyyy'))->toDateString();
        $consignment->order_id = $request->input('shippers_reference_number');
        $consignment->customer_name = $request->input('customer_code');
        $consignment->collectable_value = $request->input('cod_amount');
        $consignment->payment_mode = $request->input('payment_mode');
        $consignment->item_description = $request->input('content_description');
        $consignment->consignee = $request->input('consignee_name');
        $consignment->consignee_address = $request->input('consignee_address_line_1').''. $request->input('consignee_address_line_2'). ' ' .$request->input('consignee_address_line_3'). ' ' .$request->input('consignee_address_line_4');
        $consignment->destination_city = $request->input('consignee_city');
        $consignment->pincode = $request->input('consignee_pincode');
        $consignment->state = '';
        $consignment->mobile = $request->input('consignee_phone_number').' / '.$request->input('consignee_mobile');
        $consignment->current_status = 'Soft Data Upload';
        $consignment->prev_status = '';
        $consignment->prev_sub_status = '';
        $consignment->last_updated_on = \Carbon\Carbon::now()->toDateString();
        $consignment->last_updated_by = $request->input('customer_code') . ' - API';
        $consignment->pcs = $request->input('piece');
        $consignment->weight = $request->input('box_weightkgs');
        $consignment->price = $request->input('declared_value');
        $consignment->branch = $check_branch;

        if($consignment->save()) {

            $insertedId = $consignment->id;
           

                        ConsignmentUpdate::create([
                            'consignment_id' => $insertedId,
                            'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                            'location' => 'New Delhi',
                            'last_updated_by' => $request->input('customer_code') . ' - API',
                            'current_status' => 'Soft Data Upload',
                            'remarks' => '',
                            'bag_code' => '',
                            'drs_code' => ''

                        ]);



            return $this->response->withItem($consignment, new  ConsignmentTransformer());
        } else {
             return $this->response->errorInternalError('Could not update/create the consignment');
        }
 
    }
 
/*Reverse Consignment Api Code*/

    public function indexReverse()
    {
        //Get all task
        $consignments = ReverseConsignment::paginate(15);
        // Return a collection of $task with pagination
        return $this->response->withPaginator($consignments, new  ConsignmentTransformer());
    }
 
    public function showReverse($id)
    {
        //Get the task
        $consignment = ReverseConsignment::find($id);
        if (!$consignment) {
            return $this->response->errorNotFound('Consignment Not Found');
        }
        // Return a single task
        return $this->response->withItem($consignment, new  ConsignmentTransformer());
    }
 
    public function destroyReverse($id)
    {
        //Get the task
        $consignment = ReverseConsignment::find($id);
        if (!$consignment) {
            return $this->response->errorNotFound('Consignment Not Found');
        }
 
        if($consignment->delete()) {
             return $this->response->withItem($consignment, new  ConsignmentTransformer());
        } else {
            return $this->response->errorInternalError('Could not delete a Consignment');
        }
 
    }
 
    public function storeReverse(Request $request)  {
        if ($request->isMethod('put')) {
            //Get the task
            $consignment = ReverseConsignment::find($request->consignment_id);
            if (!$consignment) {
                return $this->response->errorNotFound('Consignment Not Found');
            }
        } else {
            $consignment = new ReverseConsignment;
        }
        $awb = $request->input('awb');//die;
        $pincode = $request->input('pincode');
        if(isset($awb) && !empty($awb)) $check_branch = BranchPincode::where('pincode', $pincode)->first();    
        if(isset($check_branch)){
            $check_branch = $check_branch->branch;
            if($check_branch == '' || $check_branch == 'ODA') $check_branch = 'Other';
        } else {
            $check_branch = 'Other';
        }


        //$consignment->id = $request->input('consignment_id');
        $consignment->awb = $request->input('air_waybill_number');
        $consignment->data_received_date = \Carbon\Carbon::parse($request->input('datedd_mm_yyyy'))->toDateString();
        $consignment->order_number = $request->input('order_numberref_no.');
        $consignment->customer_name = $request->input('customer_code');
        $consignment->product = $request->input('product');
        $consignment->consignee = $request->input('consignee');
        $consignment->consignee_address = $request->input('consignee_address1').''. $request->input('consignee_address2'). ' ' .$request->input('consignee_address1');
        $consignment->destination_city = $request->input('destination_city');
        $consignment->pincode = $request->input('pincode');
        $consignment->state = $request->input('state');
        $consignment->mobile = $request->input('mobile').' / '.$request->input('telephone');
		$consignment->item_description = $request->input('item_description');
		$consignment->pcs = $request->input('pieces');
		$consignment->collectable_value = $request->input('collectable_value');
		$consignment->declared_value = $request->input('declared_value');
		$consignment->actual_weight = $request->input('actual_weightg');
		$consignment->volumetric_weight = $request->input('volumetric_weightg');
		$consignment->length = $request->input('lengthcms');
		$consignment->breadth = $request->input('breadthcms');
		$consignment->height = $request->input('heightcms');
		$consignment->issue_category = $request->input('issue_category');
		$consignment->merchant_id = $request->input('merchant_id');
		$consignment->merchant_name = $request->input('merchant_name');
		$consignment->merchant_mobile = $request->input('merchant_mobile_no');
		$consignment->merchant_address = $request->input('merchant_address');
		$consignment->merchant_city = $request->input('merchant_city');
		$consignment->merchant_state = $request->input('merchant_state');
		$consignment->merchant_pincode = $request->input('merchant_pincode');
		$consignment->request_id = $request->input('reqid');
		$consignment->request_type = $request->input('requesttype');
		$consignment->reason_of_return = $request->input('reason_of_return');
		$consignment->sku = $request->input('sku');
		$consignment->brand = $request->input('brand');
		$consignment->category = $request->input('category');
		$consignment->size = $request->input('size');
		$consignment->color = $request->input('color');
		$consignment->paytm_code = $request->input('paytm_code');
		$consignment->serial_or_imei_no = $request->input('serialorimeino');
        $consignment->current_status = 'Soft Data Upload';
        $consignment->prev_status = '';
		$consignment->branch = $check_branch;
        $consignment->last_updated_on = \Carbon\Carbon::now()->toDateString();
        /*$consignment->created_at = \Carbon\Carbon::parse($request->input('shipment_datedd_mm_yyyy'));
        $consignment->updated_at = \Carbon\Carbon::parse($request->input('shipment_datedd_mm_yyyy'));*/
        

        if($consignment->save()) {

            $insertedId = $consignment->id;
           

                        ReverseConsignmentUpdate::create([
                            'reverse_consignment_id' => $insertedId,
                            'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                            'location' => 'New Delhi',
                           // 'last_updated_by' => Auth::user()->username . ' - API',
                            'current_status' => 'Soft Data Upload',
                            'remarks' => '',
                            'bag_code' => '',
                            'drs_code' => ''

                        ]);



            return $this->response->withItem($consignment, new  ReverseConsignmentTransformer());
        } else {
             return $this->response->errorInternalError('Could not updated/created a consignment');
        }
 
    }




}
