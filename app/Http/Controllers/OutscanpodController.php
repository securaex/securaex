<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Pod;

use Auth;

class OutscanpodController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $pods = Pod::where('created_by', Auth::user()->username)->get();
        return view('outscanpod.index',compact('pods'));                

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//print_r($request->all()); die();
       $awbs = preg_split("/(\r\n|\n|\r)/",$request->awbs);

        foreach ($awbs as $awb) {

            $consignment = Consignment::with('consignment_updates')->with('bag')->where('awb', $awb)->first();
            $status = array();
//print_r($consignment); die;

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                /*if(!in_array(\Config::get('constants.rtoCompletedbyHub'), $status) &&  in_array(\Config::get('constants.rtoVerifiedbyHub'), $status) &&  in_array(\Config::get('constants.rtoIntransittoHub'), $status) &&  in_array(\Config::get('constants.rtoInitiatedbyBranch'), $status) &&  in_array(\Config::get('constants.rtoInitiated'), $status) && in_array(\Config::get('constants.rtoProcess'), $status) && in_array(\Config::get('constants.deliveryStatus3'), $status)  && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status) && in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){*/
                if(!in_array(\Config::get('constants.rtoCompletedbyHub'), $status) &&  in_array(\Config::get('constants.rtoVerifiedbyHub'), $status) &&  in_array(\Config::get('constants.rtoIntransittoHub'), $status) &&  in_array(\Config::get('constants.rtoInitiatedbyBranch'), $status) &&  in_array(\Config::get('constants.rtoInitiated'), $status) && in_array(\Config::get('constants.rtoProcess'), $status) && in_array(\Config::get('constants.deliveryStatus3'), $status)  && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => ($consignment->customer_name == 'WOW') ? $consignment->branch : $consignment->bag->to_branch,
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.rtoCompletedbyHub'),
                    'remarks' => $consignment->rto_reason,
					'drs_code' => $consignment->drs_code,
                    'pod_code' => $request->pod_code,
                    'bag_code' => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag_code
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.rtoCompletedbyHub'),
                    'prev_status' => \Config::get('constants.rtoVerifiedbyHub'),
                    'pod_code' => $request->pod_code,
                    'pod_id' => $request->pod_id,
                    'bag_code'        => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag_code,
                    'bag_id'          => ($consignment->customer_name == 'WOW') ? NULL : $consignment->bag->id
                ]);
                \Session::flash('success_message','RTO completed by HUB.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"RTO can't be completed by HUb for Consignment with AWB : $awb ! Reason : Consignment is at step : ". $consignment->current_status ." ! please check this consignment History for details"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }
        }

        return redirect('out-scan-pod');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
