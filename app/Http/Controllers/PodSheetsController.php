<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use Illuminate\Support\Facades\Input;

use App\Bag;
use App\Pod;

use Auth;

class PodSheetsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //
        if(Input::get('date')){
            $date = Input::get('date');
        }else{
            $date = \Carbon\Carbon::today()->toDateString();
        }

        $pods = Pod::whereDate('created_at', $date)->orderBy('updated_at', 'DESC')->has('pod_consignments', '>=', 1)->with('pod_consignments')->paginate(10); 
        return view('podsheets.index', compact('pods'));                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bags = Bag::all();        
        return view('runsheets.create',compact('bags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       $awbs = explode(PHP_EOL, $request->awbs);

        foreach ($awbs as $awb) {

            $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $awb)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => 'New Delhi',
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'remarks' => '',
                    'bag_code' => $request->bag_code,
                    'drs_code' => ''
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'prev_status' => \Config::get('constants.inScanhub'),
                    'bag_code' => $request->bag_code,
                    'bag_id'   => $request->bag_id
                ]);
                \Session::flash('success_message','Bagging Created successfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Consignment AWB : $awb  has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }
        }

        return redirect('runsheets/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pod = Pod::with('pod_consignments')->findorFail($id);      
        return view('podsheets.show', compact('pod'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $pod = Pod::with('pod_consignments')->findorFail($id); 
        return view('podsheets.edit', compact('pod'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $awb = $request->awb;
        if($request->action){

            $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->with('bag')->where('awb', $awb)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array(\Config::get('constants.rtoCompletedbyHub'), $status) &&  in_array(\Config::get('constants.rtoVerifiedbyHub'), $status) &&  in_array(\Config::get('constants.rtoIntransittoHub'), $status) &&  in_array(\Config::get('constants.rtoInitiatedbyBranch'), $status) &&  in_array(\Config::get('constants.rtoInitiated'), $status)  && (in_array(\Config::get('constants.deliveryStatus1'), $status) || in_array(\Config::get('constants.deliveryStatus2'), $status) || in_array(\Config::get('constants.deliveryStatus3'), $status) || in_array(\Config::get('constants.deliveryStatus4'), $status)) && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status) && in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => $consignment->bag->to_branch,
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.rtoCompletedbyHub'),
                    'remarks' => '',
                    'pod_code' => $request->pod_code,
                    'bag_code' => $consignment->bag_code,
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.rtoCompletedbyHub'),
                    'prev_status' => \Config::get('constants.rtoVerifiedbyHub'),
                    'pod_code' => $request->pod_code,
                    'pod_id' => $request->pod_id,
                    'bag_code'        => $consignment->bag_code,
                    'bag_id'          => $consignment->bag->id
                ]);
                \Session::flash('success_message','Consignment added to podsheet successfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Consignment AWB : $awb  has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }

        }else{

            $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $awb)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){
                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(in_array(\Config::get('constants.rtoCompletedbyHub'), $status) &&  in_array(\Config::get('constants.rtoVerifiedbyHub'), $status) &&  in_array(\Config::get('constants.rtoIntransittoHub'), $status) &&  in_array(\Config::get('constants.rtoInitiatedbyBranch'), $status) &&  in_array(\Config::get('constants.rtoInitiated'), $status)  && (in_array(\Config::get('constants.deliveryStatus1'), $status) || in_array(\Config::get('constants.deliveryStatus2'), $status) || in_array(\Config::get('constants.deliveryStatus3'), $status) || in_array(\Config::get('constants.deliveryStatus4'), $status)) && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.inScanbranch'), $status) && in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){

                    $consignment_update = ConsignmentUpdate::where('consignment_id', $consignment->id)->where('current_status', \Config::get('constants.rtoCompletedbyHub'));
                    
                    if($consignment_update)
                    $consignment_update->forceDelete();


                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.rtoVerifiedbyHub'),
                        'prev_status' => \Config::get('constants.rtoIntransittoHub'),
                        'pod_code' => null,
                        'pod_id' => null,
                        'bag_code'        => $consignment->bag_code,
                        'bag_id'          => $consignment->bag->id
                    ]);
                    \Session::flash('success_message','Consignment removed successfully from Pod Sheet.'); //<--FLASH MESSAGE
                }else { 
                    \Session::flash('error_message',"Consignment AWB : $awb  has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); 
                }

                    if($consignment_update)
                    $consignment_update->forceDelete();
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }

        }
        return redirect('podsheets');

    }

    public function editStatus($id){

        $bag = Bag::with('bag_consignments')->findorFail($id); 
        return view('runsheets.status', compact('bag'));

    }

    public function verifyBag($id){
        $bag = Bag::with('bag_consignments')->findorFail($id); 
        return view('runsheets.verify', compact('bag'));

    }
    public function updateStatus(Request $request, $id){

        $status = ($request->status)? '1' : '0';
                    $bag = Bag::has('bag_consignments', '>=', 1)->with('bag_consignments')->findorFail($id);
                    $bag->update([
                        'in_transit' => $status,
                    ]);


        if($status){

            foreach ($bag->bag_consignments as $bag_consignment) {

                $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->consignment_updates)){

                    foreach($consignment->consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(!in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                    ConsignmentUpdate::create([
                        'consignment_id' => $consignment->id,
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'location' => 'New Delhi',
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inTransit'),
                        'remarks' => '',
                        'bag_code' => $bag->bag_code,
                        'drs_code' => ''
                    ]);
                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inTransit'),
                        'prev_status' => \Config::get('constants.baggingCreated'),
                        'bag_code' => $bag->bag_code,
                        'bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging In Transit.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }

        }else{

            foreach ($bag->bag_consignments as $bag_consignment) {

                $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->consignment_updates)){

                    foreach($consignment->consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                    
                    $consignment_update = ConsignmentUpdate::where('consignment_id', $consignment->id)->where('current_status', \Config::get('constants.inTransit'));
                    
                    if($consignment_update)
                    $consignment_update->forceDelete();

                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.baggingCreated'),
                        'prev_status' => \Config::get('constants.inScanhub'),
                        'bag_code' => $bag->bag_code,
                        'bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging In Transit Removed.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }



        }

        return redirect('runsheets');


    }


    public function updateBagVerification(Request $request, $id){

        $status = ($request->verify)? '1' : '0';
                    $bag = Bag::has('bag_consignments', '>=', 1)->with('bag_consignments')->findorFail($id);
                    $bag->update([
                        'is_verified' => $status,
                    ]);


        if($status){

            foreach ($bag->bag_consignments as $bag_consignment) {

                $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->consignment_updates)){

                    foreach($consignment->consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(!in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                    ConsignmentUpdate::create([
                        'consignment_id' => $consignment->id,
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'location' => $bag->to_branch,
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.bagVerified'),
                        'remarks' => '',
                        'bag_code' => $bag->bag_code,
                        'drs_code' => ''
                    ]);
                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.bagVerified'),
                        'prev_status' => \Config::get('constants.inTransit'),
                        'bag_code' => $bag->bag_code,
                        'bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging Verified successfully.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }

        }else{

            foreach ($bag->bag_consignments as $bag_consignment) {

                $consignment = Consignment::has('consignment_updates', '>=', 1)->with('consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->consignment_updates)){

                    foreach($consignment->consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                    
                    $consignment_update = ConsignmentUpdate::where('consignment_id', $consignment->id)->where('current_status', \Config::get('constants.bagVerified'));
                    
                    if($consignment_update)
                    $consignment_update->forceDelete();

                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inTransit'),
                        'prev_status' => \Config::get('constants.baggingCreated'),
                        'bag_code' => $bag->bag_code,
                        'bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging Verified Removed.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }



        }

        return redirect('runsheets');        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
