<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use Auth;

class ConsignmentVerificationController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('consignment-verification.index');                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->action) && $request->action == 'show'){
        // Get the consignment AWB which has atleast 1 update
        $consignment = Consignment::with('consignment_updates')->where('awb', $request->txtwaybill)->first();

        }elseif(isset($request->action) && $request->action == 'verify'){
            // Get the consignment AWB which has atleast 1 update
            $status = array();
            $consignment = Consignment::with('consignment_updates')->where('awb', $request->txtwaybill)->first();
            foreach($consignment->consignment_updates as $update){
                $status[] = $update->current_status;                                        
            }
            if(!in_array(\Config::get('constants.inScanhub'), $status) && in_array(\Config::get('constants.softDataupload'), $status)){
            ConsignmentUpdate::create([
                'consignment_id' => $consignment->id,
                'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                'location' => 'New Delhi',
                'last_updated_by' => Auth::user()->username,
                'current_status' => \Config::get('constants.inScanhub'),
                'remarks' => '',
                'bag_code' => '',
                'drs_code' => ''
            ]);

            $consignment = Consignment::findorFail($consignment->id);
            $consignment->update([
                'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                'last_updated_by' => Auth::user()->username,
                'current_status' => \Config::get('constants.inScanhub'),
                'prev_status' => \Config::get('constants.softDataupload')/*,
                'weight' => $request->weight,
                'price' => $request->price*/
            ]);
            
            }

        }


        if($consignment){

                return view('consignment-verification.index', compact('consignment'));

        }else{

                return view('consignment-verification.index', ['awb' => $request->txtwaybill]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
