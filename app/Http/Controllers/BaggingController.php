<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use Illuminate\Support\Facades\Input;
use App\Bag;
use Auth;
use App\User;

class BaggingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //
        if(Input::get('date')){
            $date = Input::get('date');
        }else{
            $date = \Carbon\Carbon::today()->toDateString();
        }

        $bags = Bag::whereDate('created_at', $date)->orderBy('updated_at', 'DESC')->has('bag_consignments', '>=', 1)->with('bag_consignments')->paginate(500);       
        return view('bagging.index', compact('bags'));                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$bags = Bag::where('in_transit','!=', 1)->where('is_verified','!=', 1)->whereDate('created_at', \Carbon\Carbon::today()->toDateString())->get();  
		
		$branches = User::where('user_type', '4')->where('is_searchable','1')->orderBy('username', 'asc')->get();

        return view('bagging.create',compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // 
	   //json_decode($json, true);
	    if(Auth::user()->username === 'Sunil_KR' || Auth::user()->username === 'aman-hub' || Auth::user()->username === 'pankaj-hub' || Auth::user()->username === 'ved_prakash' || Auth::user()->username === 'mahender-hub')
	   {
		   $awbs = preg_split("/(\r\n|\n|\r)/",$request->awbs);
	   }
	   else
	   {
		  $awbs = json_decode($request->awbsSingle, true); 
		  $awbs = array_map('trim',$awbs);
	   }
		$bag = Bag::create([
					'bag_code' => $request->bag_code,
					'seal_number' => $request->seal_number,
					'to_branch' => $request->to_branch
				]);
		$bag_id = $bag->id;
		//echo "<pre>";print_r($awbs);die;
        foreach ($awbs as $awb) {

            $consignment = Consignment::with('consignment_updates')->where('awb', $awb)->first();
            $status = array();

            if($consignment){
				
                if(trim($consignment->branch) == trim($request->to_branch) || isset($request->override)){

                    if(isset($consignment->consignment_updates)){

                        foreach($consignment->consignment_updates as $update){
                            $status[] = $update->current_status;                    
                        }

                        if(!in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                        ConsignmentUpdate::create([
                            'consignment_id' => $consignment->id,
                            'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                            'location' => 'New Delhi',
                            'last_updated_by' => Auth::user()->username,
                            'current_status' => \Config::get('constants.baggingCreated'),
                            'remarks' => '',
                            'bag_code' => $request->bag_code,
                            'drs_code' => '',
                            'remarks' => ($consignment->branch == $request->to_branch) ? '' : 'Branch Overridden from ' . $consignment->branch . ' to ' . $request->to_branch                            
                        ]);
                        $consignment = Consignment::findorFail($consignment->id);
                        $consignment->update([
                            'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                            'last_updated_by' => Auth::user()->username,
                            'current_status' => \Config::get('constants.baggingCreated'),
                            'prev_status' => \Config::get('constants.inScanhub'),
                            'bag_code' => $request->bag_code,
                            'bag_id'   => $bag_id,
                            'branch' => ($consignment->branch == $request->to_branch) ? $consignment->branch : $request->to_branch
                        ]);

                        \Session::flash('success_message','Bagging Created successfully.'); //<--FLASH MESSAGE
                        }else { 
                            \Session::flash('error_message',"Bagging can't be created for Consignment with AWB : $awb ! Reason : Consignment is at step : ". $consignment->current_status ." ! please check this consignment History for details"); 
                            return redirect('bagging/create');
                        }
                    } 

                } else{ 
                    \Session::flash('error_message',"Consignment Branch & Bag Branch Mismatch for AWB : $awb"); 
                    return redirect('bagging/create');
                }
            
            }else{
                    \Session::flash('error_message',"Consignment not found for AWB : $awb");
                    return redirect('bagging/create'); 
            }

        
        } // End Foreach
        return redirect('bagging');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bag = Bag::with('bag_consignments')->findorFail($id); 
        return view('bagging.edit', compact('bag'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $awb = $request->awb;
        if($request->action){

            $consignment = Consignment::with('consignment_updates')->where('awb', $awb)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){

                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                ConsignmentUpdate::create([
                    'consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => 'New Delhi',
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'remarks' => '',
                    'bag_code' => $request->bag_code,
                    'drs_code' => ''
                ]);
                $consignment = Consignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.baggingCreated'),
                    'prev_status' => \Config::get('constants.inScanhub'),
                    'bag_code' => $request->bag_code,
                    'bag_id'   => $request->bag_id
                ]);
                \Session::flash('success_message','Bagging Created successfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Consignment AWB : $awb  has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }

        }else{

            $consignment = Consignment::with('consignment_updates')->where('awb', $awb)->first();
            $status = array();

            if(isset($consignment->consignment_updates)){
                foreach($consignment->consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){

                    $consignment_update = ConsignmentUpdate::where('consignment_id', $consignment->id)->where('current_status', \Config::get('constants.baggingCreated'));
                    
                    if($consignment_update)
                    $consignment_update->forceDelete();


                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inScanhub'),
                        'prev_status' => \Config::get('constants.softDataupload'),
                        'bag_code' => null,
                        'bag_id'   => null
                    ]);
                    \Session::flash('success_message','Bagging Created successfully.'); //<--FLASH MESSAGE
                }else { 
                    \Session::flash('error_message',"Consignment AWB : $awb  has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); 
                }

                    if($consignment_update)
                    $consignment_update->forceDelete();
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }

        }
        return redirect('bagging');

    }

    public function editStatus($id){

        $bag = Bag::with('bag_consignments')->findorFail($id); 
        return view('bagging.status', compact('bag'));

    }

    public function verifyBag($id){
        $bag = Bag::with('bag_consignments')->findorFail($id); 
        return view('bagging.verify', compact('bag'));

    }
    public function updateStatus(Request $request, $id){

        $status = ($request->status)? '1' : '0';
                    $bag = Bag::has('bag_consignments', '>=', 1)->with('bag_consignments')->findorFail($id);
                    $bag->update([
                        'in_transit' => $status,
                    ]);


        if($status){

            foreach ($bag->bag_consignments as $bag_consignment) {

                $consignment = Consignment::with('consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->consignment_updates)){

                    foreach($consignment->consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(!in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                    ConsignmentUpdate::create([
                        'consignment_id' => $consignment->id,
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'location' => 'New Delhi',
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inTransit'),
                        'remarks' => '',
                        'bag_code' => $bag->bag_code,
                        'drs_code' => ''
                    ]);
                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inTransit'),
                        'prev_status' => \Config::get('constants.baggingCreated'),
                        'bag_code' => $bag->bag_code,
                        'bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging In Transit.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }

        }else{

            foreach ($bag->bag_consignments as $bag_consignment) {

                $consignment = Consignment::with('consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->consignment_updates)){

                    foreach($consignment->consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                    
                    $consignment_update = ConsignmentUpdate::where('consignment_id', $consignment->id)->where('current_status', \Config::get('constants.inTransit'));
                    
                    if($consignment_update)
                    $consignment_update->forceDelete();

                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.baggingCreated'),
                        'prev_status' => \Config::get('constants.inScanhub'),
                        'bag_code' => $bag->bag_code,
                        'bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging In Transit Removed.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }



        }

        return redirect('bagging');


    }


    public function updateBagVerification(Request $request, $id){

        $status = ($request->verify)? '1' : '0';
                    $bag = Bag::has('bag_consignments', '>=', 1)->with('bag_consignments')->findorFail($id);
                    $bag->update([
                        'is_verified' => $status,
                    ]);


        if($status){

            foreach ($bag->bag_consignments as $bag_consignment) {

                $consignment = Consignment::with('consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->consignment_updates)){

                    foreach($consignment->consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(!in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                    ConsignmentUpdate::create([
                        'consignment_id' => $consignment->id,
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'location' => $bag->to_branch,
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.bagVerified'),
                        'remarks' => '',
                        'bag_code' => $bag->bag_code,
                        'drs_code' => ''
                    ]);
                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.bagVerified'),
                        'prev_status' => \Config::get('constants.inTransit'),
                        'bag_code' => $bag->bag_code,
                        'bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging Verified successfully.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }

        }else{

            foreach ($bag->bag_consignments as $bag_consignment) {

                $consignment = Consignment::with('consignment_updates')->where('awb', $bag_consignment->awb)->first();
                $status = array();

                if(isset($consignment->consignment_updates)){

                    foreach($consignment->consignment_updates as $update){
                        $status[] = $update->current_status;                    
                    }

                    if(in_array(\Config::get('constants.bagVerified'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inScanhub'), $status)){
                    
                    $consignment_update = ConsignmentUpdate::where('consignment_id', $consignment->id)->where('current_status', \Config::get('constants.bagVerified'));
                    
                    if($consignment_update)
                    $consignment_update->forceDelete();

                    $consignment = Consignment::findorFail($consignment->id);
                    $consignment->update([
                        'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                        'last_updated_by' => Auth::user()->username,
                        'current_status' => \Config::get('constants.inTransit'),
                        'prev_status' => \Config::get('constants.baggingCreated'),
                        'bag_code' => $bag->bag_code,
                        'bag_id'   => $bag->id
                    ]);
                    \Session::flash('success_message','Bagging Verified Removed.'); //<--FLASH MESSAGE
                    }else { \Session::flash('error_message',"Consignment AWB :   has not been Scanned at HUB <a href='/consignment-verification'>In Scan</a>"); }
                }else{ \Session::flash('error_message',"No Consignment Found with AWB : "); }
            }



        }

        return redirect('bagging');        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
