<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Customer;
use App\BranchPincode;
use App\User;
use App\Drs;

use Illuminate\Support\Facades\Input;
use DB;
use Auth;

use Excel;


class CallerConsignmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $branches = User::where('user_type', '4')->where('is_searchable','1')->orderBy('username', 'asc')->get();    
        //
        if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        //
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }
		$MyDateCarbon = \Carbon\Carbon::parse($date_to);
		$date_to = $MyDateCarbon->addDays(1)->toDateString();
        $results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to)->where('current_status','Undelivered')->orderBy('reviewed','asc')->orderBy('no_of_attempts', 'desc');
        if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'))->orderBy('no_of_attempts', 'desc');

        	$consignments = $results->paginate(150);
		
		return view('caller-consignments.index', compact('consignments','branches'));
    }


    public function create()
    {
        //
        return view('consignments.create');
    }

    public function store(Request $request)
    {
        //
        Consignment::create($request->all());
        return redirect('consignments');

    }

    public function show($id)
    {
        DB::enableQueryLog();
        $consignment = Consignment::with('consignment_updates')->where('id', $id)->first();
		
		if(!$consignment->drs_id || $consignment->drs_id!="")
		{
			 $dev_boy = Drs::select('delivery_boy','mobile')->where('id',$consignment->drs_id)->first();
			 return view('consignments.show', compact('consignment', 'dev_boy'));
		}
		else
        	return view('consignments.show', compact('consignment'));
        
    }

    public function edit($id)
    {
        //
        //$branches = User::where('user_type', '4')->get();    

        $consignment = Consignment::findorFail($id);
        return view('caller-consignments.edit', compact('consignment'));

    }

    public function update(Request $request, $id)
    {
        //
		//echo "<pre>"; print_r($request->toArray()); 
		/*if($request->caller_remarks == "")
			$request['caller_remarks'] = null;*/
		//echo "<pre>"; print_r($request->toArray()); die;
        $consignment = Consignment::findorFail($id);
		$consignment->updateUntouched($request->all());
        return redirect('caller-consignments');


    }

    public function destroy($id)
    {
        //
        $user = Consignment::findorFail($id);
        $user->forceDelete();
        return redirect('consignments');        
    }
}
