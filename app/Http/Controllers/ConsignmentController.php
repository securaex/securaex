<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Customer;
use App\BranchPincode;
use App\User;
use App\Drs;

use Illuminate\Support\Facades\Input;
use DB;
use Auth;

use Excel;


class ConsignmentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
/* 
    public function index()
    {
        //
        if(Input::get('date')){
            $date = Input::get('date');
            //$date = \Carbon\Carbon::createFromFormat('Y-m-d', $date)->toDateTimeString(); 

        }else{
            $date = \Carbon\Carbon::today()->toDateString();
        }

        if(Input::get('branch')){
            $branch = Input::get('branch');
        $consignments = Consignment::whereDate('created_at', $date)->where('branch', $branch)->orderBy('updated_at', 'desc')->paginate(10);
        
        }else{
        $consignments = Consignment::whereDate('created_at', $date)->orderBy('updated_at', 'desc')->paginate(10);

        }

        return view('consignments.index', compact('consignments'));                
    }

    public function index()
    {
        $customers = User::where('user_type', '5')->get();
        $branches = User::where('user_type', '4')->get();    
        //
        if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        //
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }

        if(Input::get('branch') && !Input::get('customer_name')){
            $branch = Input::get('branch');
            $consignments = Consignment::whereDate('created_at', '>=' ,$date_from)->whereDate('created_at', '<=' ,$date_to)->where('branch', $branch)->orderBy('updated_at', 'desc')->paginate(10);
        
        }else if(Input::get('customer_name') && !Input::get('branch')){
            $customer_name = Input::get('customer_name');
            $consignments = Consignment::whereDate('created_at', '>=' ,$date_from)->whereDate('created_at', '<=' ,$date_to)->where('customer_name', $customer_name)->orderBy('updated_at', 'desc')->paginate(10);

        }else if(Input::get('branch') && Input::get('customer_name')){
            $branch = Input::get('branch');
            $customer_name = Input::get('customer_name');
            $consignments = Consignment::whereDate('created_at', '>=' ,$date_from)->whereDate('created_at', '<=' ,$date_to)->where('branch', $branch)->where('customer_name', $customer_name)->orderBy('updated_at', 'desc')->paginate(10);

        }else{
        $consignments = Consignment::whereDate('created_at', '>=' ,$date_from)->whereDate('created_at', '<=' ,$date_to)->orderBy('updated_at', 'desc')->paginate(10);

        }

        return view('consignments.index', compact('consignments','customers','branches'));                
    }

*/

    public function index(Request $request)
    {
		 //echo "<pre>";print_r($request->all());die;
        $customers = User::select('name')->where('user_type', '5')->orderBy('name')->get();
        $branches = User::select('username')->where('user_type', '4')->where('is_searchable','1')->orderBy('username', 'asc')->get();    
        //
        if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        //
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }
		$MyDateCarbon = \Carbon\Carbon::parse($date_to);
		$date_to = $MyDateCarbon->addDays(1)->toDateString();
		
        			$results = Consignment::select('id','awb','customer_name','payment_mode','collectable_value','pincode','current_status','last_updated_by','branch','drs_id','no_of_attempts','rto_reason','is_locked','created_at','updated_at')->with(array('drs'=>function($query){$query->select('id','delivery_boy');}))->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<' ,$date_to);


        			
        // no query executed, just give us a builder
		
        if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'))->orderBy('updated_at', 'desc');

        if($request->has('customer_name'))
             $results = $results->where('customer_name', $request->input('customer_name'))->orderBy('updated_at', 'desc');

        if($request->has('current_status'))
             $results = $results->where('current_status', $request->input('current_status'))->orderBy('updated_at', 'desc');
		
		if($request->has('ex_wow'))
             $results = $results->where('customer_name', '!=', 'WOW')->orderBy('updated_at', 'desc');
		
			/*
			$results = $results->get();
			*/
		//DB::enableQueryLog();
        	$consignments = $results->paginate(10);
		//dd(DB::getQueryLog());
		return view('consignments.index', compact('consignments','customers','branches'));
    }


    public function exportExcel(Request $request){
		
		ini_set('max_execution_time', 1200);
		ini_set('memory_limit', '512M');
        
        if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        //
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }
		
			//DB::enableQueryLog();

			
			return Excel::create('consignments_data', function($excel) use ($request,$date_from,$date_to) {
            																			
            $excel->sheet('datas', function($sheet) use ($request,$date_from,$date_to)
            {
				//DB::enableQueryLog();

				if($request->has('field') && $request->has('value'))
					$results = Consignment::where('id', '!=', NULL);
				else
					$results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to)->with(array('consignment_updates'=>function($query){$query->select('consignment_id','last_updated_by')->where('current_status','Soft Data Upload');}))->with('drs');
                //dd(DB::getQueryLog());
		
				if($request->has('branch'))
					 $results = $results->where('branch', $request->input('branch'))->orderBy('updated_at', 'desc');
		
				if($request->has('customer_name'))
					 $results = $results->where('customer_name', $request->input('customer_name'))->orderBy('updated_at', 'desc');
		
				if($request->has('current_status'))
					 $results = $results->where('current_status', $request->input('current_status'))->orderBy('updated_at', 'desc');
					 
				if($request->has('field') && $request->has('value'))
					 $results = $results->where($request->input('field'), 'like', '%'.$request->input('value').'%')->orderBy('updated_at', 'desc');	
				
				if($request->has('ex_wow'))
             		 $results = $results->where('customer_name', '!=', 'WOW')->orderBy('updated_at', 'desc');
					 
					 
				$arr =array();
                //echo "<pre>";print_r($results->get()->toArray());die;
				$results->chunk(1000, function($datas) use (&$arr) {
			  	foreach ($datas as $data) {
						if(isset($data->drs))
						{
                            foreach($data->consignment_updates as $v)
                                $last_updated_by=$v->last_updated_by;

								$data =  array($data->awb, $data->order_id, $data->item_description, $data->weight, $data->payment_mode,
									$data->collectable_value, $data->price, $data->customer_name, $data->consignee, $data->consignee_address, $data->destination_city, $data->pincode, $data->mobile, $data->branch, $data->current_status, $data->created_at->format('Y-m-d'), $data->updated_at, $data->updated_at->format('Y-m-d'), $data->first_attempt_date, $data->no_of_attempts, $data->remarks, $data->caller_remarks, $data->caller_name, $data->call_date, $data->bag_code, $data->undelivered_reason, $data->drs->delivery_boy, $last_updated_by);
						}
						else
						{
                             foreach($data->consignment_updates as $v)
                                $last_updated_by=$v->last_updated_by;

								$data =  array($data->awb, $data->order_id, $data->item_description, $data->weight, $data->payment_mode,
									$data->collectable_value, $data->price, $data->customer_name, $data->consignee, $data->consignee_address, $data->destination_city, $data->pincode, $data->mobile, $data->branch, $data->current_status, $data->created_at->format('Y-m-d'), $data->updated_at, $data->updated_at->format('Y-m-d'), $data->first_attempt_date, $data->no_of_attempts, $data->remarks, $data->caller_remarks, $data->caller_name, $data->call_date, $data->bag_code, $data->undelivered_reason, 'No Delivery Boy', $last_updated_by);
						}
						
						array_push($arr, $data);
			  		}
					
				});
				//echo "<pre>";print_r($arr);die;
				
                $sheet->fromArray($arr, null, 'A1', false, false);
                // Add before first row

					$sheet->prependRow(1, array(
                    'Air waybill No', 
                    'Reference No',
                    'Item Description',
                    'Weight(kg)',
					'Payment Mode',
                    'COD Amount',
                    'Declared Value',
                    'Shipper ( Client Name )',
                    'Consignee Name',
                    'Consignee Address',
					'City',
					'Pincode',
                    'Consignee Contact',
					'Branch Name',
                    'Current Status',
                    'Process Date',
                    'Updated Date & Time',
                    'Updated Date',
					'1st Attempt Date',
                    'No Of Attempt',
                    'Remarks',
					'Caller Remarks',
					'Caller Name',
					'Call Date',
                    'Bag Code',
                    'Undelivered Reason',                                        
					'Delivery Boy Name',
                    'Shipment Uploaded By',
                ));
                

				$sheet->cells('A1:AH1', function($cells) {
				
					// manipulate the range of cells
						   
				// Set font
				$cells->setFont(array(
					'family'     => 'Calibri',
					'size'       => '11',
					'bold'       =>  true,
					'background' => 'blue'
				));    
				
				// Set all borders (top, right, bottom, left)
				$cells->setBorder('solid', 'none', 'none', 'solid');

});
            });
        })->download('xls');

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('consignments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Consignment::create($request->all());
        return redirect('consignments');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //DB::enableQueryLog();
        $consignment = Consignment::select('id','awb','consignee','mobile','consignee_address','item_description','payment_mode','collectable_value','current_status','branch','remarks','caller_remarks','caller_name','weight','price','message_ofd_response','drs_id','created_at','updated_at')->with(array('consignment_updates'=>function($query){$query->select('consignment_id','current_status','created_at','location','last_updated_by','remarks','bag_code','drs_code','pod_code');}))->with(array('drs'=>function($query){$query->select('id','delivery_boy','mobile');}))->where('id', $id)->first();
        //$consignment = Consignment::findorFail($id)->consignmentupdates;
        //echo "<pre>"; print_r($consignment); die;
		return view('consignments.show', compact('consignment'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $branches = User::where('user_type', '4')->get();    

        $consignment = Consignment::findorFail($id);
        return view('consignments.edit', compact('consignment','branches'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $consignment = Consignment::findorFail($id);
        $consignment->update($request->all());
        return redirect('consignments');


    }



    public function unlock(Request $request, $id){

                $consignment = Consignment::findorFail($id);

                $consignment->updateUntouched([
                    'is_locked' => 0                    
                ]);

        return redirect('consignments');

        /*
        $model->awb = $model->awb . '-RA';
        $model->current_status = \Config::get('constants.inScanbranch');
        $model->prev_status = \Config::get('constants.bagVerified');

        $model->load('consignment_updates');

        $newModel = $model->replicate();
        $newModel->push();


        foreach($model->getRelations() as $relation => $items){
            unset($items[6], $items[7]);
            foreach($items as $item){
                unset($item->id);
                $newModel->{$relation}()->create($item->toArray());
            }

        }

        die('done');
        */

    }

    public function reset($id){
        
        $consignment = Consignment::with('consignment_updates')->where('id', $id)->first();
		//echo "<pre>";print_r($consignment);die;
			$branch = BranchPincode::where('pincode', $consignment->pincode)->first();	
			if(isset($branch)){
				$branch = $branch->branch;
				if($branch == '' || $branch == 'ODA')
					$branch = 'Other';
			}else{
				$branch = 'Other';
			}
		
        $status = array();

        $consignment = Consignment::findorFail($consignment->id);
        $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => 'API',
                    'current_status' => 'Soft Data Upload',
                    'prev_status' => '',
                    'remarks' => null,
                    'no_of_attempts' => 0,
                    'bag_code' => null,
                    'bag_id' => null,
                    'drs_code' => null,
                    'drs_id' => null,
                    'pod_code' => null,
                    'pod_id' => null,
                    'rto_awb' => null,
                    'rto_reason' => null,
					'branch' => $branch,
                    'undelivered_reason' => null,
                    're_attempt' => 0,
                    'delivered_date_time' => null,
                    'received_by' => null
        ]);

        if(isset($consignment->consignment_updates)){
            
            foreach($consignment->consignment_updates as $update){                
                ConsignmentUpdate::where('id', '=', $update->id)->where('current_status', '!=' , \Config::get('constants.softDataupload'))->delete();
            }

        }

        return redirect('consignments');

    }




    public function resetbranch(Request $request, $id){
        $consignment = Consignment::has('consignment_updates', '=', 1)->find($id);

        if(isset($consignment)){

            if($request->branch != 'other'){
                $consignment->update([
                            'branch' => 'other'
                ]);
            }else{

                    $branch = BranchPincode::where('pincode', $request->pincode)->first();   
                        if(isset($branch)){
                            $branch = $branch->branch;
                            if($branch == '' || $branch == 'ODA')
                                $branch = 'Other';
                        }else{
                            $branch = 'Other';
                        }   

                $consignment->update([
                            'branch' => $branch
                ]);
            }
        }else{
            
        }


        return redirect("consignments/$id/edit");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = Consignment::findorFail($id);
        $user->forceDelete();
        return redirect('consignments');        
    }
	
}
