<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReverseConsignment;
use App\ReverseConsignmentUpdate;
use App\ReversePor;

use Auth;

class OutscanporController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $pods = ReversePor::where('created_by', Auth::user()->username)->get();
        return view('outscanpor.index',compact('pods'));                

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//echo "<pre>"; print_r($request->all()); die();
       //$awbs = explode(PHP_EOL, $request->awbs);
	   $awbs = preg_split("/(\r\n|\n|\r)/",$request->awbs);
        foreach ($awbs as $awb) {

            $consignment = ReverseConsignment::with('reverse_consignment_updates')->where('awb', $awb)->first();
            $status = array();
//print_r($consignment); die;

            if(isset($consignment->reverse_consignment_updates)){

                foreach($consignment->reverse_consignment_updates as $update){
                    $status[] = $update->current_status;                    
                }

                if(!in_array(\Config::get('constants.outScanToClient'), $status) && in_array(\Config::get('constants.inScanhubRev'), $status) && in_array(\Config::get('constants.baggingCreated'), $status) && in_array(\Config::get('constants.inTransit'), $status) && in_array(\Config::get('constants.softDataupload'), $status)  && in_array(\Config::get('constants.outScanbranch'), $status) && in_array(\Config::get('constants.collected'), $status)){
                ReverseConsignmentUpdate::create([
                    'reverse_consignment_id' => $consignment->id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => 'New Delhi',
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.outScanToClient'),
					'remarks' => '',
                    'por_code' => $request->pod_code,
                ]);
                $consignment = ReverseConsignment::findorFail($consignment->id);
                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.outScanToClient'),
                    'prev_status' => $consignment->current_status,
                    'reverse_por_code' => $request->pod_code,
                    'reverse_por_id' => $request->pod_id,
                ]);
                \Session::flash('success_message','Out Scanned To Client Done Successfully.'); //<--FLASH MESSAGE
                }else { \Session::flash('error_message',"Out Scanned To Client can't be done for Consignment with AWB : $awb ! Reason : Consignment is at step : ". $consignment->current_status ." ! please check this consignment History for details"); }
            }else{ \Session::flash('error_message',"No Consignment Found with AWB : $awb"); }
        }

        return redirect('reverse-out-scan-por');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
