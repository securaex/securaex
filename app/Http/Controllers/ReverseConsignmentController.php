<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReverseConsignment;
use App\ReverseConsignmentUpdate;
use App\Customer;
use App\BranchPincode;
use App\User;
use App\ReverseDrs;

use Illuminate\Support\Facades\Input;
use DB;
use Auth;

use Excel;
class ReverseConsignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function __construct()
    {
        $this->middleware('auth');
    }
	 
	 
    public function index(Request $request)
    {
        $customers = User::where('user_type', '5')->get();
        $branches = User::where('user_type', '4')->where('is_searchable','1')->get();    
        //
        if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        //
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }
		
		DB::enableQueryLog();
        			$results = ReverseConsignment::with('reverse_drs')->whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to);


        			
        // no query executed, just give us a builder

        if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'))->orderBy('updated_at', 'desc');

        if($request->has('customer_name'))
             $results = $results->where('customer_name', $request->input('customer_name'))->orderBy('updated_at', 'desc');

        if($request->has('current_status'))
             $results = $results->where('current_status', $request->input('current_status'))->orderBy('updated_at', 'desc');
			
			/*DB::enableQueryLog();
			$results = $results->get();
			dd(DB::getQueryLog());*/
        	$consignments = $results->orderByRaw('case when branch LIKE "%Other%" then 0 else 1 end')->paginate(10);
			//echo "<pre>"; print_r($consignments); die;
			//dd(DB::getQueryLog());
		
        return view('reverse-consignments.index', compact('consignments','customers','branches'));                
    }
	
    public function exportExcel(Request $request){
        
        if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        //
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }


        $columns = [
        'awb',
        'order_number',
        'item_description',
        'actual_weight',
		'volumetric_weight',
        'pcs',
        'collectable_value',
		'declared_value',
		'merchant_name',
        'consignee',
        'consignee_address',
		'pincode',
        'mobile',
        'data_received_date',
		'branch',
        'current_status',
        'updated_at',
        'remarks',
        'received_by',
        'pickup_date_time',
        'reverse_bag_code',
        'no_of_attempts',
        'pickup_fail_reason',                
        DB::raw('"" as comments'),
        ];


		$results = ReverseConsignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to);
        // no query executed, just give us a builder


        if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'))->orderBy('updated_at', 'desc');

        if($request->has('customer_name'))
             $results = $results->where('customer_name', $request->input('customer_name'))->orderBy('updated_at', 'desc');

        if($request->has('current_status'))
             $results = $results->where('current_status', $request->input('current_status'))->orderBy('updated_at', 'desc');
			 
		if($request->has('field') && $request->has('value'))
             $results = $results->where($request->input('field'), $request->input('value'))->orderBy('updated_at', 'desc');	 
			 
      $consignments = $results->get();
	  			$dev_boy = array();
			foreach($consignments as $result)
			{
				$temp = ReverseDrs::select('delivery_boy')->where('id',$result->reverse_drs_id)->first();
				if($temp)
				$dev_boy[] = $temp->delivery_boy;
				else
				$dev_boy[] = "";
			}
     		$consignments = $results->get($columns);
			$temps=$consignments->toArray();
			$i=0;
			foreach($temps as &$temp)
			{
				$temp['delivery_boy'] = $dev_boy[$i];
				$i++;
			}
			$data=$temps;

        return Excel::create('consignments_data', function($excel) use ($data) {
            $excel->sheet('data', function($sheet) use ($data)
            {
                $sheet->fromArray($data, null, 'A1', false, false);
                // Add before first row

					$sheet->prependRow(1, array(
                    'Air waybill No', 
                    'Order Number',
                    'Item Description',
                    'Actual Weight',
					'Volumetric Weight',
                    'Quantity',
					'Collectable Value',
                    'Declared Value',
                    'Shipper ( Client Name )',
                    'Consignee Name',
                    'Consignee Address',
					'Pincode',
                    'Consignee Contact',
                    'Process Date',
					'Branch',
                    'Status',
                    'Updated Date',
                    'Remarks',
                    'Received by',
                    'Pickup Date & Time',
                    'Bag Code',
                    'No Of Attempt',
                    'Pickup Fail Reason',                                        
                    'Comments',
					'Delivery Boy Name',
                ));
                

$sheet->cells('A1:AH1', function($cells) {

    // manipulate the range of cells
           
// Set font
$cells->setFont(array(
    'family'     => 'Calibri',
    'size'       => '11',
    'bold'       =>  true,
    'background' => 'blue'
));    

// Set all borders (top, right, bottom, left)
$cells->setBorder('solid', 'none', 'none', 'solid');

});



            });
        })->download('xls');

    }
	

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reverse-consignments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        ReverseConsignment::create($request->all());
        return redirect('reverse-consignments');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        DB::enableQueryLog();
        $consignment = ReverseConsignment::with('reverse_consignment_updates')->where('id', $id)->first();
        //$consignment = Consignment::findorFail($id)->consignmentupdates;
        //echo "<pre>"; print_r($consignment); die;
		
		if(!$consignment->reverse_drs_id || $consignment->reverse_drs_id!="")
		{
		 $dev_boy = ReverseDrs::select('delivery_boy')->where('id',$consignment->reverse_drs_id)->first();
		 return view('reverse-consignments.show', compact('consignment', 'dev_boy'));
		}
		else
        return view('reverse-consignments.show', compact('consignment'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $branches = User::where('user_type', '4')->where('is_searchable','1')->get();    

        $consignment = ReverseConsignment::findorFail($id);
        return view('reverse-consignments.edit', compact('consignment','branches'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $consignment = ReverseConsignment::findorFail($id);
        $consignment->update($request->all());
        return redirect('reverse-consignments');


    }

    public function repeat(Request $request, $id){

                $consignment = ReverseConsignment::findorFail($id);

                ReverseConsignmentUpdate::create([
                    'reverse_consignment_id' => $id,
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'location' => $consignment->branch,
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.reattemptBranch'),
                    'bag_code'        => $consignment->bag->bag_code,
                ]);

                $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => Auth::user()->username,
                    'current_status' => \Config::get('constants.reattemptBranch'),
                    'prev_status' => $consignment->current_status,
                    're_attempt' => '1',                    
                    'reverse_bag_code'        => $consignment->bag->bag_code,
                    'reverse_bag_id'          => $consignment->bag->id
                ]);

        return redirect('reverse-consignments');
    }


    public function reset($id){
        
        $consignment = ReverseConsignment::with('reverse_consignment_updates')->where('id', $id)->first();
        $status = array();

        $consignment = ReverseConsignment::findorFail($consignment->id);
        $consignment->update([
                    'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
                    'last_updated_by' => 'API',
                    'current_status' => 'Soft Data Upload',
                    'prev_status' => '',
                    'remarks' => null,
                    'no_of_attempts' => 0,
                    'reverse_bag_code' => null,
                    'reverse_bag_id' => null,
                    'reverse_drs_code' => null,
                    'reverse_drs_id' => null,
                    'reverse_pod_code' => null,
                    'reverse_pod_id' => null,
                    'pickup_fail_reason' => null,
                    're_attempt' => 0,
                    'pickup_date_time' => null,
                    'received_by' => null
        ]);

        if(isset($consignment->reverse_consignment_updates)){
            
            foreach($consignment->reverse_consignment_updates as $update){                
                ReverseConsignmentUpdate::where('id', '=', $update->id)->where('current_status', '!=' , \Config::get('constants.softDataupload'))->delete();
            }

        }

        return redirect('reverse-consignments');

    }
	
	
    public function resetbranch(Request $request, $id){
        $consignment = ReverseConsignment::has('reverse_consignment_updates', '=', 1)->find($id);

        if(isset($consignment)){

            if($request->branch != 'other'){
                $consignment->update([
                            'branch' => 'other'
                ]);
            }else{

                    $branch = BranchPincode::where('pincode', $request->pincode)->first();   
                        if(isset($branch)){
                            $branch = $branch->branch;
                            if($branch == '' || $branch == 'ODA')
                                $branch = 'Other';
                        }else{
                            $branch = 'Other';
                        }   

                $consignment->update([
                            'branch' => $branch
                ]);
            }
        }else{
            
        }


        return redirect("reverse-consignments/$id/edit");

    }
	

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = ReverseConsignment::findorFail($id);
        $user->forceDelete();
        return redirect('reverse-consignments');        
    }
}
