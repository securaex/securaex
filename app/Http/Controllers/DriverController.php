<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\User;
use Auth;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$branches = User::where('user_type', '4')->where('is_searchable','1')->get();
        $drivers = Driver::orderBy('updated_at', 'DESC');
		if($request->has('branch'))
             $drivers = $drivers->where('branch', $request->input('branch'))->orderBy('updated_at', 'DESC');
		$drivers = $drivers->paginate(10);	 
        return view('drivers.index', compact('drivers','branches'));                
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       	$branches = User::where('user_type', '4')->where('is_searchable','1')->get(); 
		return view('drivers.create', compact('branches')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'driver_name' => 'required|unique:drivers,driver_name',
			'mobile' => 'required|unique:drivers,mobile',
			'vehicle_number' => 'required',
			'branch' => 'required'
        ]);
		
		$data = $request->all();
    	$data['driver_name'] = ucwords(strtolower(trim($data['driver_name'])));

        Driver::create($data);
        return redirect('drivers');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $driver = Driver::findorFail($id);
		$branches = User::where('user_type', '4')->where('is_searchable','1')->get(); 
        return view('drivers.edit', compact('driver','branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $driver = Driver::findorFail($id);
		$data = $request->all();
    	$data['driver_name'] = ucwords(strtolower(trim($data['driver_name'])));
		
        $driver->update($data);
        return redirect('drivers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $driver = Driver::findorFail($id);
        $driver->forceDelete();
        return redirect('drivers');        
    }
}
