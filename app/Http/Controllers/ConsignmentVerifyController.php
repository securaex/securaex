<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use Auth;
use DB;

class ConsignmentVerifyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('consignment-verify.index');                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	//DB::enableQueryLog();
		//print_r($request->awbs);die;
		$consignments = Consignment::whereIn('awb', $request->awbs)->get();
		//dd(DB::getQueryLog());
		//echo "<pre>"; print_r($consignments); die;
		$totalConsignments = count($request->awbs);
    	foreach ($request->awbs as $awb) {
			//echo $awb; die();
            $status = array();
            $consignment = Consignment::with('consignment_updates')->where('awb', $awb)->first();
			//echo "<pre>";print_r($consignment->toArray());die;
            foreach($consignment->consignment_updates as $update){
                $status[] = $update->current_status;                                        
            }
			//echo "<pre>";print_r($status);die;
            if(!in_array(\Config::get('constants.inScanhub'), $status) && in_array(\Config::get('constants.softDataupload'), $status)){
				//die("inside if");
	            ConsignmentUpdate::create([
	                'consignment_id' => $consignment->id,
	                'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
	                'location' => 'New Delhi',
	                'last_updated_by' => Auth::user()->username,
	                'current_status' => \Config::get('constants.inScanhub'),
	                'remarks' => '',
	                'bag_code' => '',
	                'drs_code' => ''
	            ]);

	            $consignment = Consignment::findorFail($consignment->id);
	            $consignment->update([
	                'last_updated_on' => \Carbon\Carbon::now()->toDateString(),
	                'last_updated_by' => Auth::user()->username,
	                'current_status' => \Config::get('constants.inScanhub'),
	                'prev_status' => \Config::get('constants.softDataupload')/*,
	                'weight' => $request->weight,
	                'price' => $request->price*/
	            ]);
            
            }

    	}
//die("faraz");

        \Session::flash('success_message',"Total Number of Consignments In Scanned: <strong>$totalConsignments</strong> <br /><br /> In Scan done Successfully for AWB ! : " . implode(" ", $request->awbs));
    	return view('consignment-verify.index')->with('consignments', $consignments);
        //}


        //if($consignment){

          //      return view('consignment-verify.index', compact('consignment'));

        //}else{

          //      return view('consignment-verify.index', ['awb' => $request->txtwaybill]);
        //}

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
