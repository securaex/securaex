<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Drs;
use Auth;
use DB;
use Excel;

class TrackingController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function consignment(Request $request)
    {

        if ($request->isMethod('post')) {

            //$trackingdata = explode(',', $request->trackingdata); 
            //$trackingdata  = preg_split('/\s*,\s*/', trim($request->trackingdata));
            $trackingdata = $array = preg_split("/\r\n|\n|\r/", trim($request->trackingdata));

           // print_r($trackingdata); die;
            switch ($request->trackingtype) {
                case '0':
                    # code...
					//DB::enableQueryLog();
                    $consignments = Consignment::whereIn('awb', $trackingdata)->orderBy('updated_at', 'desc')->get();
					//dd(DB::getQueryLog());
					//echo "<pre>"; print_r($consignments); die;
				    /*$consignments = Consignment
					::join('drs', 'drs.id', '=', 'consignments.drs_id')
					->select('consignments.*', 'drs.delivery_boy')
					->whereIn('awb', $trackingdata)
					->get();*/					
                    break;
                case '1':
                    # code...
                    $consignments = Consignment::whereIn('order_id', $trackingdata)->orderBy('updated_at', 'desc')->get();
				    /*$consignments = Consignment
					::join('drs', 'drs.id', '=', 'consignments.drs_id')
					->select('consignments.*', 'drs.delivery_boy')
					->whereIn('order_id', $trackingdata)
					->get();*/					
                    break;
                case '2':
                    # code...
                    $consignments = Consignment::whereIn('mobile', $trackingdata)->orderBy('updated_at', 'desc')->get();
				    /*$consignments = Consignment
					::join('drs', 'drs.id', '=', 'consignments.drs_id')
					->select('consignments.*', 'drs.delivery_boy')
					->whereIn('mobile', $trackingdata)
					->get();*/					
                    break;                                    
                default:
                    # code...
                    break;
            }
//echo "<pre>"; print_r($consignments->toArray()); die;
            if (!$consignments->isEmpty()) { 
                return view('tracking.consignment')->with('consignments', $consignments);
            }else{
                \Session::flash('error_message',"No Consignment Found with Search Data : $request->trackingdata");
            }

        }


//die("khan");
        return view('tracking.consignment');
    }



    public function exportExcel(Request $request){
        /*
        print_r($request->all()); die;

        if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        //
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }
        */

        $columns = [
        'awb',
        'order_id',
        'item_description',
        'weight', 
        'pcs',
        'collectable_value',
        'price',
        DB::raw('"" as shipper_name'),
        'consignee',
        'consignee_address',
        'mobile',
		'branch',
        'data_received_date',
        'current_status',
        'updated_at',
        'remarks',
        'received_by',
        'delivered_date_time',
        'bag_code',
        'no_of_attempts',
        'rto_awb',
        DB::raw('"" as rto_status'),
        'undelivered_reason',                
        'rto_reason',                
        DB::raw('"" as comment'),
        DB::raw('"" as rto_awb_date'),
        ];


        if ($request->isMethod('post')) {

            $trackingdata = $array = preg_split("/\r\n|\n|\r/", trim($request->trackingdata));

            switch ($request->trackingtype) {
                case '0':
                    # code...
                    $results = Consignment::whereIn('awb', $trackingdata)->orderBy('updated_at', 'desc');
                    break;
                case '1':
                    # code...
                    $results = Consignment::whereIn('order_id', $trackingdata)->orderBy('updated_at', 'desc');
                    break;
                case '2':
                    # code...
                    $results = Consignment::whereIn('mobile', $trackingdata)->orderBy('updated_at', 'desc');
                    break;                                    
                default:
                    # code...
                    break;
            }

            $data = $results->get($columns)->toArray();

        }

/*
        $results = Consignment::whereDate('updated_at', '>=' ,$date_from)->whereDate('updated_at', '<=' ,$date_to); 
        // no query executed, just give us a builder


        if($request->has('branch'))
             $results = $results->where('branch', $request->input('branch'))->orderBy('updated_at', 'desc');

        if($request->has('customer_name'))
             $results = $results->where('customer_name', $request->input('customer_name'))->orderBy('updated_at', 'desc');

        if($request->has('current_status'))
             $results = $results->where('current_status', $request->input('current_status'))->orderBy('updated_at', 'desc');

        $data = $results->get($columns)->toArray();

*/

        return Excel::create('consignments_data', function($excel) use ($data) {
            $excel->sheet('data', function($sheet) use ($data)
            {
                //$sheet->fromArray($data);
                
                $sheet->fromArray($data, null, 'A1', false, false);
                // Add before first row
  
                $sheet->prependRow(1, array(
                    'Air waybill No', 
                    'Reference No',
                    'Item Description',
                    'Weight(kg)',
                    'Quantity',
                    'COD Amount',
                    'Declared Value',
                    'Shipper ( Client Name )',
                    'Consignee Name',
                    'Consignee Address',
                    'Consignee Contact',
					'Branch',
                    'Process Date',
                    'Status',
                    'Updated Date',
                    'Remarks',
                    'Received by',
                    'Delivery Date & Time',
                    'Bag Code',
                    'No Of Attempt',
                    'RTO AWB Number',
                    'RTO Status',
                    'Undelivered Reason',                                        
                    'RTO Reason',                    
                    'Comment',
                    'RTO AWB Date',
                ));
                

            $sheet->cells('A1:AH1', function($cells) {

                // manipulate the range of cells
                       
            // Set font
            $cells->setFont(array(
                'family'     => 'Calibri',
                'size'       => '11',
                'bold'       =>  true,
                'background' => 'blue'
            ));    

            // Set all borders (top, right, bottom, left)
            $cells->setBorder('solid', 'none', 'none', 'solid');

            });



            });
        })->download('xls');

    }


}
