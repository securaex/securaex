<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Drs;
use Illuminate\Support\Facades\Input;
use Excel;
use Charts;
use DB;
use App\User;

class AgingReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showReports()
    {
		/*ini_set('max_execution_time', 1200);
		ini_set('memory_limit', '512M');*/
		$customers = User::where('user_type', '5')->get();
		
		$aged = \Carbon\Carbon::now()->subDays(2)->toDateString();
		$aged2 = \Carbon\Carbon::now()->subDays(1)->toDateString();
		$agedOutsideDelhi = \Carbon\Carbon::now()->subDays(4)->toDateString();
		$agedOutsideDelhi2 = \Carbon\Carbon::now()->subDays(3)->toDateString();
		$threeMonths = \Carbon\Carbon::now()->subDays(90)->toDateString();
		
		//DB::enableQueryLog();
		
		$twoDaysAgedDelhiNcr = Consignment::whereDate('created_at', '>=', $aged)->whereDate('created_at', '<', $aged2)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->whereIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->count();
		
		$moreDaysAgedDelhiNcr = Consignment::whereDate('created_at', '<', $aged)->whereDate('created_at', '>=', $threeMonths)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->whereIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->count();
		
		
		$threeDaysAgedOutsideDelhi = Consignment::whereDate('created_at', '>=', $agedOutsideDelhi)->whereDate('created_at', '<', $agedOutsideDelhi2)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->whereNotIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->count();
		
		$moreDaysAgedOutsideDelhi = Consignment::whereDate('created_at', '<', $aged)->whereDate('created_at', '>=', $threeMonths)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->whereNotIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->count();
		//dd(DB::getQueryLog());
		
		//$rtoCons = Consignment::with('consignment_updates')->where('current_status', 'like', '%RTO%')->where('current_status', 'not like', 'RTO Final Status');
		$clients=array();
		foreach($customers as $customer)
		{
			$clients[$customer->name]['twoDaysAged'] = Consignment::whereDate('created_at', '>=', $aged)->whereDate('created_at', '<', $aged2)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->where('customer_name', '=', $customer->name)->count();
			$clients[$customer->name]['moreDaysAged'] = Consignment::whereDate('created_at', '<', $aged)->whereDate('created_at', '>=', $threeMonths)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->where('customer_name', '=', $customer->name)->count();
		}
		
		/*echo "<pre>"; print_r($clients); 
		foreach($clients as $key => $value)
		echo $key.'=>'.$value['twoDaysAged'].', '.$value['moreDaysAged'].'<br>';
		die;*/
        
        return view('aging-reports.agingReports',[
			"twoDaysAgedDelhiNcr"=>$twoDaysAgedDelhiNcr,
			"moreDaysAgedDelhiNcr"=>$moreDaysAgedDelhiNcr,
			"threeDaysAgedOutsideDelhi"=>$threeDaysAgedOutsideDelhi,
			"moreDaysAgedOutsideDelhi"=>$moreDaysAgedOutsideDelhi,
			"clients"=>$clients
            ]);
    }



    public function exportExcel(Request $request){
        
		ini_set('max_execution_time', 1200);
		ini_set('memory_limit', '512M');

		if(Input::get('date_from')){
            $date_from = Input::get('date_from');
        }else{
            $date_from = \Carbon\Carbon::today()->toDateString();
        }

        //
        if(Input::get('date_to')){
            $date_to = Input::get('date_to');
        }else{
            $date_to = \Carbon\Carbon::today()->toDateString();
        }
		
		$MyDateCarbon = \Carbon\Carbon::parse($date_to);
		$date_to = $MyDateCarbon->addDays(1)->toDateString();
		
		//DB::enableQueryLog();
		
        $type = Input::get('type');
        switch ($type) {
            case 'aging':
                # code...
				$columns = [
				'awb',
				'customer_name',
				'created_at',
				'branch',
				'current_status',
				];
				
				$aged = \Carbon\Carbon::now()->subDays(2)->toDateString();
				$aged2 = \Carbon\Carbon::now()->subDays(1)->toDateString();
				$agedOutsideDelhi = \Carbon\Carbon::now()->subDays(4)->toDateString();
				$agedOutsideDelhi2 = \Carbon\Carbon::now()->subDays(3)->toDateString();
				$threeMonths = \Carbon\Carbon::now()->subDays(90)->toDateString();
				
				if(Input::get('agingDuration')=="Two Days(Delhi NCR)")
				{
                	$results = Consignment::whereDate('created_at', '>=', $aged)->whereDate('created_at', '<', $aged2)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->whereIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->orderBy('created_at','asc');
				}
				elseif(Input::get('agingDuration')=="More Than 2 Days(Delhi NCR)")
				{
					$results = Consignment::whereDate('created_at', '<', $aged)->whereDate('created_at', '>=', $threeMonths)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->whereIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->orderBy('created_at','asc');
				}
				elseif(Input::get('agingDuration')=="Three Days(Out Of Delhi)")
				{
					$results = Consignment::whereDate('created_at', '>=', $agedOutsideDelhi)->whereDate('created_at', '<', $agedOutsideDelhi2)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->whereNotIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->orderBy('created_at','asc');
				}
				else
				{
					$results = Consignment::whereDate('created_at', '<', $aged)->whereDate('created_at', '>=', $threeMonths)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->whereNotIn('branch', array('Jasola','Ghaziabad','Rangpuri','Noida','Dwarka','Old Delhi','Faridabad'))->orderBy('created_at','asc');
				}
                break;
            case 'clientWiseAging':
                # code...
				$columns = [
				'awb',
				'customer_name',
				'created_at',
				'branch',
				'current_status',
				];
				
				$aged = \Carbon\Carbon::now()->subDays(2)->toDateString();
				$aged2 = \Carbon\Carbon::now()->subDays(1)->toDateString();
				$threeMonths = \Carbon\Carbon::now()->subDays(90)->toDateString();
				
				$agingDuration = Input::get('agingDuration');
				$agingDuration = explode("-", $agingDuration);
				$clientName = trim($agingDuration[0]);
				$duration = trim($agingDuration[1]);
				
			
				if($duration=="Two Days")
				{
					$results = Consignment::whereDate('created_at', '>=', $aged)->whereDate('created_at', '<', $aged2)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->where('customer_name', '=', $clientName)->orderBy('created_at', 'asc');
				}
				else
				{
					$results = Consignment::whereDate('created_at', '<', $aged)->whereDate('created_at', '>=', $threeMonths)->where('current_status', 'not like', '%RTO%')->where('current_status', '!=', 'Delivered')->where('customer_name', '=', $clientName)->orderBy('created_at', 'asc');
				}
                break;
            default:
                # code...
                break;
        }
		
		//dd(DB::getQueryLog());
		

			$consignments = $results->get($columns);
			$data=$consignments->toArray();
			
			if(Input::get('type')=="aging")
			{
				$agingDuration = Input::get('agingDuration');
				$agingDuration = str_replace(" ","_",$agingDuration);
				$excelName = "Ageing_".$agingDuration;
			}
			else
			{
				$agingDuration = str_replace(" ","_",$duration);
				$excelName = "Ageing_".$clientName."_".$agingDuration;
			}
		

			return Excel::create($excelName, function($excel) use ($data) {
            																			
            $excel->sheet('data', function($sheet) use ($data)
            {
				
                $sheet->fromArray($data, null, 'A1', false, false);
                // Add before first row
						$sheet->prependRow(1, array(
						'Air waybill No',
						'Shipper ( Client Name )',
						'Created Date',
						'Branch Name',
						'Current Status',                
						));
				$sheet->cells('A1:AH1', function($cells) {
				
					// manipulate the range of cells
						   
				// Set font
				$cells->setFont(array(
					'family'     => 'Calibri',
					'size'       => '11',
					'bold'       =>  true,
					'background' => 'blue'
				));    
				
				// Set all borders (top, right, bottom, left)
				$cells->setBorder('solid', 'none', 'none', 'solid');

});
            });
        })->download('xls');

    }


}
