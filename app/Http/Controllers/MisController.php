<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Customer;
use App\BranchPincode;
use App\User;
use App\Drs;

use Illuminate\Support\Facades\Input;
use DB;
use Auth;

use Excel;


class MisController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
		$months = array();
		$months[0] = \Carbon\Carbon::now()->format('F');
		$months[1] = \Carbon\Carbon::now()->subMonth()->format('F');
		$months[2] = \Carbon\Carbon::now()->subMonths(2)->format('F');
		
		
        if(Input::get('date_from')){
            $month = Input::get('date_from');
			$first_date = \Carbon\Carbon::parse("first day of $month")->toDateString();
			$last_date = \Carbon\Carbon::parse("last day of $month")->toDateString();
        }else{
			$first_date = \Carbon\Carbon::parse("first day of $months[0]")->toDateString();
			$last_date = \Carbon\Carbon::parse("last day of $months[0]")->toDateString();
        }


        			$results = Consignment::with('drs')->whereDate('updated_at', '>=' ,$first_date)->whereDate('updated_at', '<=' ,$last_date)->orderBy('updated_at','desc');

        	$consignments = $results->paginate(10);
		
		return view('mis.index', compact('consignments','months'));
    }


    public function exportExcel(Request $request){
        
        if(Input::get('date_from')){
            $month = Input::get('date_from');
			$first_date = \Carbon\Carbon::parse("first day of $month")->toDateString();
			$last_date = \Carbon\Carbon::parse("last day of $month")->toDateString();
        }else{
			$month = \Carbon\Carbon::now()->format('F');
			$first_date = \Carbon\Carbon::parse("first day of $month")->toDateString();
			$last_date = \Carbon\Carbon::parse("last day of $month")->toDateString();
        }

        //
			$columns = [
			'awb',
			'order_id',
			'item_description',
			'weight', 
			'pcs',
			'payment_mode',
			'collectable_value',
			'price',
			'customer_name',
			'consignee',
			'consignee_address',
			'destination_city',
			'pincode',
			'mobile',
			'created_at',
			'branch',
			'current_status',
			'updated_at',
			'remarks',
			'received_by',
			'delivered_date_time',
			'bag_code',
			'no_of_attempts',
			'rto_awb',
			DB::raw('"" as rto_status'),
			'undelivered_reason',                
			'rto_reason',                
			DB::raw('"" as comment'),
			DB::raw('"" as rto_awb_date'),
			];
			
				$results = Consignment::whereDate('updated_at', '>=' ,$first_date)->whereDate('updated_at', '<=' ,$last_date)->orderBy('updated_at','desc');

			$data=$results->get($columns);
			
			return Excel::create('consignments_data', function($excel) use ($data) {
            																			
            $excel->sheet('data', function($sheet) use ($data)
            {
				
                $sheet->fromArray($data, null, 'A1', false, false);
                // Add before first row

					$sheet->prependRow(1, array(
                    'Air waybill No', 
                    'Reference No',
                    'Item Description',
                    'Weight(kg)',
                    'Quantity',
					'Payment Mode',
                    'COD Amount',
                    'Declared Value',
                    'Shipper ( Client Name )',
                    'Consignee Name',
                    'Consignee Address',
					'City',
					'Pincode',
                    'Consignee Contact',
                    'Process Date',
					'Branch',
                    'Status',
                    'Updated Date',
                    'Remarks',
                    'Received by',
                    'Delivery Date & Time',
                    'Bag Code',
                    'No Of Attempt',
                    'RTO AWB Number',
                    'RTO Status',
                    'Undelivered Reason',                                        
                    'RTO Reason',                    
                    'Comment',
                    'RTO AWB Date',
                ));
                

				$sheet->cells('A1:AH1', function($cells) {
				
					// manipulate the range of cells
						   
				// Set font
				$cells->setFont(array(
					'family'     => 'Calibri',
					'size'       => '11',
					'bold'       =>  true,
					'background' => 'blue'
				));    
				
				// Set all borders (top, right, bottom, left)
				$cells->setBorder('solid', 'none', 'none', 'solid');

});
            });
        })->download('xls');

    }
}
