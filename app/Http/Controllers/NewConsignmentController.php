<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consignment;
use App\ConsignmentUpdate;
use App\Customer;
use App\BranchPincode;
use App\User;
use App\Drs;

use Illuminate\Support\Facades\Input;
use DB;
use Auth;

use Excel;


class NewConsignmentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $customers = User::where('user_type', '5')->get();
        $branches = User::where('user_type', '4')->where('is_searchable','1')->orderBy('username', 'asc')->get();   
        $customers = \DB::table('consignments')->select(\DB::raw('distinct customer_name as customer_name'))->get();		
        //
		$all_data = array();
		
		if(Input::get('client_name'))
		{
		  $client_name = Input::get('client_name');	
		}
		
		if(Input::get('branch'))
		{
		  $branch = Input::get('branch');	
		  $mybranch = $branch;
		 
		}
		
		if(!empty($branch))
		{
          if(Input::get('date_from'))
		  {
            $date_from = Input::get('date_from');
			$from_date_flag = true;
		  }
		  else
		  {
            $current_date = \Carbon\Carbon::today()->toDateString();
          }

        
		  if(Input::get('date_to'))
		  {
			$date_to = Input::get('date_to');
			$to_date_flag = true;
		  }
		  else
		  {
			$current_date = \Carbon\Carbon::today()->toDateString();
		  }
		  
		  
		 if(!empty($date_from) && !empty($date_to)) 
		 {
		   $start = date($date_from);
           $end = date($date_to);
            
           $to = \Carbon\Carbon::createFromFormat('Y-m-d', $date_to);
           $from = \Carbon\Carbon::createFromFormat('Y-m-d', $date_from);
           $days = $to->diffInDays($from);
           if($days == 0)
		   {
		
		  if($branch != 'all')
		  {
			  if(empty($client_name))
			  {
				  if(!empty($from_date_flag) && !empty($to_date_flag))
				  {
					$shipment_recieved = \DB::table('consignment_updates')->select(\DB::raw('count(*) as shipment_recieved'))
					->where(\DB::raw('date(created_at)'), '>=', $date_from)
					->where(\DB::raw('date(created_at)'), '<=', $date_to)
					->where('current_status', 'Shipment Received At BRANCH')->where('location', $branch)->get();
				
					 
					 
					 
					 
					 $ofd = \DB::table('consignment_updates')->select(\DB::raw('count(distinct consignment_id) as ofd'))
															 ->where(\DB::raw('date(created_at)'), '>=', $date_from)
															 ->where(\DB::raw('date(created_at)'), '<=', $date_to)
															 ->where('current_status', 'Out for Delivery')->where('location', $branch)
															 ->get();
					 
					 
					 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, cu.drs_code, cu.last_updated_by, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in (select max(id) as 'id' from consignment_updates where (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out for Delivery') and (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and (date(created_at) between '$date_from' and '$date_to') and location = '$branch')) group by consignment_id) and cu.consignment_id = c.id order by cu.drs_code");
					 
					
					 
					 
					 
				  }
				  else
				  {
					 $shipment_recieved = \DB::table('consignment_updates')->select(\DB::raw('count(*) as shipment_recieved'))->where('created_at', 'LIKE', "%$current_date%")->where('current_status', 'Shipment Received At BRANCH')->where('location', $branch)->get();
					 
					 
					 $ofd = \DB::table('consignment_updates')->select(\DB::raw('count(distinct consignment_id) as ofd'))->where('created_at', 'LIKE', "%$current_date%")->where('current_status', 'Out for Delivery')->where('location', $branch)->get();
					 
					 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, c.item_description, cu.remarks, cu.drs_code, cu.last_updated_by, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in (select max(id) as 'id' from consignment_updates where date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered') and date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and date(created_at) = '$current_date' and location = '$branch')) group by consignment_id) and cu.consignment_id = c.id order by cu.drs_code");
					 
				  }
		      }
			  else
			  {
				if(!empty($from_date_flag) && !empty($to_date_flag))
				  {
					 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, cu.drs_code, cu.last_updated_by, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in (select max(id) as 'id' from consignment_updates where (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out for Delivery') and (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and (date(created_at) between '$date_from' and '$date_to') and location = '$branch')) group by consignment_id) and cu.consignment_id = c.id and c.customer_name = '$client_name'order by cu.drs_code");
					 
				  }
				  else
				  {
					 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, c.item_description, cu.remarks, cu.drs_code, cu.last_updated_by, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in (select max(id) as 'id' from consignment_updates where date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered') and date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and date(created_at) = '$current_date' and location = '$branch')) group by consignment_id) and cu.consignment_id = c.id order by cu.drs_code and c.customer_name = '$client_name'");
					 
				  }  
			  }
		}
		else if($branch == 'all')
		{
		  if(empty($client_name))
		  {			  
			if(!empty($from_date_flag) && !empty($to_date_flag))
			  {
				$shipment_recieved = \DB::table('consignment_updates')->select(\DB::raw('count(*) as shipment_recieved'))
				->where(\DB::raw('date(created_at)'), '>=', $date_from)
				->where(\DB::raw('date(created_at)'), '<=', $date_to)
				->where('current_status', 'Shipment Received At BRANCH')->get();
			
				 
				 $ofd = \DB::table('consignment_updates')->select(\DB::raw('count(distinct consignment_id) as ofd'))
														 ->where(\DB::raw('date(created_at)'), '>=', $date_from)
														 ->where(\DB::raw('date(created_at)'), '<=', $date_to)
														 ->where('current_status', 'Out for Delivery')
														 ->get();
														 
					
                 
				 
				
				 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, cu.drs_code, cu.last_updated_by, c.awb, c.customer_name, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c  where cu.id in (select max(id) from consignment_updates where (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out for Delivery') and (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and (date(created_at) between '$date_from' and '$date_to'))) group by consignment_id) and cu.consignment_id = c.id order by cu.drs_code");
				
			
				 
			  }
			  else
			  {
				 $shipment_recieved = \DB::table('consignment_updates')->select(\DB::raw('count(*) as shipment_recieved'))->where('created_at', 'LIKE', "%$current_date%")->where('current_status', 'Shipment Received At BRANCH')->get();
				 
				 
				 $ofd = \DB::table('consignment_updates')->select(\DB::raw('count(distinct consignment_id) as ofd'))->where('created_at', 'LIKE', "%$current_date%")->where('current_status', 'Out for Delivery')->get();
				 
				 $last_status = DB::select("c.updated_by_platform, select cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, c.item_description, cu.drs_code, cu.last_updated_by, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in (select max(id) as 'id' from consignment_updates where date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out for Delivery') and date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and date(created_at) = '$current_date')) group by consignment_id) and cu.consignment_id = c.id order by cu.drs_code");
				 
			  }
		   }
		   else
		   {
			  if(!empty($from_date_flag) && !empty($to_date_flag))
				  {
					 $last_status = DB::select("c.updated_by_platform, select cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, dr.delivery_boy, cu.last_updated_by, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in (select max(id) as 'id' from consignment_updates where (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out for Delivery') and (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and (date(created_at) between '$date_from' and '$date_to'))) group by consignment_id) and cu.consignment_id = c.id and c.customer_name = '$client_name' order cu.drs_code");
					 
				  }
				  else
				  {
					 $last_status = DB::select("c.updated_by_platform, select cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, c.item_description, cu.drs_code, cu.last_updated_by, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in (select max(id) as 'id' from consignment_updates where date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered') and date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and date(created_at) = '$current_date')) group by consignment_id) and cu.consignment_id = c.id and c.customer_name = '$client_name' order by cu.drs_code");
					 
				  }  
			  } 
		   }
		   
		   
		   
		   
		   $drs_array = array();
		   $current_drs = "";
		   $before_drs = "";
		   
		   foreach($last_status as $row)
		   {
			  $current_drs = $row->drs_code; 
			  {
				  if(!empty($current_drs))
				  {
					  if($current_drs != $before_drs)
					  $results = \DB::table('drs')->select('delivery_boy', 'mobile')	
									   ->where('drs_code', $row->drs_code)	
									   ->get();	
					  foreach($results as $result)
					  {
						$drs_array[$row->drs_code]['delivery_boy'] = $result->delivery_boy;
						$drs_array[$row->drs_code]['mobile'] = $result->mobile;
					  }
			      }
		      }
              $before_drs = $current_drs;			  
		   }
		   
		   return view('new_consignments.index', compact('shipment_recieved', 'ofd', 'customers', 'branches', 'last_status', 'date_from', 'date_to', 'mybranch', 'customers', 'drs_array'));
		   }
		   else
		   {
			  echo "<script>Only single day report could be fetched</script>";   
		   }
		  }
		}
		else
	    {
	      return view('new_consignments.index', compact('customers', 'branches', 'customers'));
	    }
    }
	
	
	
	
	
	
	
	


     public function exportxls(Request $request)
	 {
		
		ini_set('max_execution_time', 1200);
		ini_set('memory_limit', '512M');
        
		
		if(Input::get('branch'))
		{
		  $branch = Input::get('branch');	
		 
		}
		
		if(!empty($branch))
		{
        if(Input::get('date_from'))
		  {
            $date_from = Input::get('date_from');
			$from_date_flag = true;
		  }
		  else
		  {
            $current_date = \Carbon\Carbon::today()->toDateString();
          }

        
		  if(Input::get('date_to'))
		  {
			$date_to = Input::get('date_to');
			$to_date_flag = true;
		  }
		  else
		  {
			$current_date = \Carbon\Carbon::today()->toDateString();
		  }
		  
		 if(!empty($date_from) && !empty($date_to)) 
		
		 if(Input::get('client_name'))
		  {
			$client_name = Input::get('client_name');
		  }
		
		
		if($branch != 'all')
		{
		  if(empty($client_name))
		  {			  
		if(!empty($from_date_flag) && !empty($to_date_flag))
		  {
			
            $shipment_recieved = \DB::table('consignment_updates')->select(\DB::raw('count(*) as shipment_recieved'))
				->where(\DB::raw('date(created_at)'), '>=', $date_from)
				->where(\DB::raw('date(created_at)'), '<=', $date_to)
				->where('current_status', 'Shipment Received At BRANCH')->where('location', $branch)->get();
			
				 
				 $ofd = \DB::table('consignment_updates')->select(\DB::raw('count(distinct consignment_id) as ofd'))
														 ->where(\DB::raw('date(created_at)'), '>=', $date_from)
														 ->where(\DB::raw('date(created_at)'), '<=', $date_to)
														 ->where('current_status', 'Out for Delivery')->where('location', $branch)
														 ->get(); 
 
			$last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, cu.drs_code, cu.last_updated_by, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in(select max(id) as 'id' from consignment_updates where (date(created_at) between '$date_from' and '$date_to') and consignment_id in(select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out For Delivery') and (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and (date(created_at) between '$date_from' and '$date_to') and location = '$branch')) group by consignment_id) and cu.consignment_id = c.id order by cu.drs_code");
			 
		  }
		  else
		  {
			$shipment_recieved = \DB::table('consignment_updates')->select(\DB::raw('count(*) as shipment_recieved'))->where('created_at', 'LIKE', "%$current_date%")->where('current_status', 'Shipment Received At BRANCH')->where('location', $branch)->get();
				 
				 
				 $ofd = \DB::table('consignment_updates')->select(\DB::raw('count(distinct consignment_id) as ofd'))->where('created_at', 'LIKE', "%$current_date%")->where('current_status', 'Out for Delivery')->where('location', $branch)->get();
				 
			 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name,  c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, c.item_description, cu.drs_code, cu.last_updated_by, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in(select max(id) as 'id' from consignment_updates where date(created_at) = '$current_date' and consignment_id in(select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out for Delivery') and date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and date(created_at) = '$current_date' and location = '$branch')) group by consignment_id) and cu.consignment_id = c.id order by cu.drs_code");
			 
		  }
		 }
		 else
		 {
			if(!empty($from_date_flag) && !empty($to_date_flag))
				  {
					 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, cu.drs_code, cu.last_updated_by, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in(select max(id) as 'id' from consignment_updates where (date(created_at) between '$date_from' and '$date_to') and consignment_id in(select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out for Delivery') and (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and (date(created_at) between '$date_from' and '$date_to') and location = '$branch')) group by consignment_id) and cu.consignment_id = c.id and c.customer_name = '$client_name'order by cu.drs_code");
					 
				  }
				  else
				  {
					 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, c.item_description, cu.remarks, cu.drs_code, cu.last_updated_by, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in(select max(id) as 'id' from consignment_updates where date(created_at) = '$current_date' and consignment_id in(select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered') and date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and date(created_at) = '$current_date' and location = '$branch')) group by consignment_id) and cu.consignment_id = c.id and c.customer_name = '$client_name' order by cu.drs_code");
					 
				  }   
		 }
		}
        else if($branch == 'all')
		{
		  if(empty($client_name))
		  {
		  if(!empty($from_date_flag) && !empty($to_date_flag))
		  {
			
			 
			 $shipment_recieved = \DB::table('consignment_updates')->select(\DB::raw('count(*) as shipment_recieved'))
				->where(\DB::raw('date(created_at)'), '>=', $date_from)
				->where(\DB::raw('date(created_at)'), '<=', $date_to)
				->where('current_status', 'Shipment Received At BRANCH')->get();
			
				 
				 $ofd = \DB::table('consignment_updates')->select(\DB::raw('count(distinct consignment_id) as ofd'))
														 ->where(\DB::raw('date(created_at)'), '>=', $date_from)
														 ->where(\DB::raw('date(created_at)'), '<=', $date_to)
														 ->where('current_status', 'Out for Delivery')
														 ->get();
			 
			 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, cu.drs_code, cu.last_updated_by, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, cu.remarks, cu.updated_at from  consignments as c, consignment_updates as cu where cu.id in(select max(id) as 'id' from consignment_updates where (date(created_at) between '$date_from' and '$date_to') and consignment_id in(select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out for Delivery') and (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and (date(created_at) between '$date_from' and '$date_to'))) group by consignment_id) and cu.consignment_id=c.id order by cu.drs_code");
			 
			
			 
		  }
		  else
		  {
			 $shipment_recieved = \DB::table('consignment_updates')->select(\DB::raw('count(*) as shipment_recieved'))->where('created_at', 'LIKE', "%$current_date%")->where('current_status', 'Shipment Received At BRANCH')->get();
				 
				 
				 $ofd = \DB::table('consignment_updates')->select(\DB::raw('count(distinct consignment_id) as ofd'))->where('created_at', 'LIKE', "%$current_date%")->where('current_status', 'Out for Delivery')->get();
				 
			 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, c.item_description, delivery_boy, dr.mobile, cu.drs_code, cu.last_updated_by, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in(select max(id) as 'id' from consignment_updates where date(created_at) = '$current_date' and consignment_id in(select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out for Delivery') and date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and date(created_at) = '$current_date')) group by consignment_id) and cu.consignment_id = c.id order by cu.drs_code");
			 
		  }
		 }
		 else
		 {
			if(!empty($from_date_flag) && !empty($to_date_flag))
				  {
					 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, cu.drs_code, cu.last_updated_by, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, cu.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in(select max(id) as 'id' from consignment_updates where (date(created_at) between '$date_from' and '$date_to') and consignment_id in(select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered' or current_status = 'Out for Delivery') and (date(created_at) between '$date_from' and '$date_to') and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and (date(created_at) between '$date_from' and '$date_to'))) group by consignment_id) and cu.consignment_id = c.id and c.customer_name = '$client_name' order by cu.drs_code");
					 
				  }
				  else
				  {
					 $last_status = DB::select("select c.updated_by_platform, cu.consignment_id, cu.current_status, cu.location, c.awb, c.customer_name, c.payment_mode, c.collectable_value, c.consignee, c.consignee_address, c.created_at, c.item_description, cu.drs_code, cu.last_updated_by, c.remarks, cu.updated_at from consignment_updates as cu, consignments as c where cu.id in(select max(id) as 'id' from consignment_updates where date(created_at) = '$current_date' and consignment_id in(select distinct consignment_id from consignment_updates where (current_status = 'Delivered' or current_status = 'Undelivered' or current_status = 'Process to be RTO' or current_status = 'RTO Initiated' or current_status = 'RTO - Pending To Transit' or current_status = 'RTO - Transit to HUB' or current_status = 'RTO - Received By HUB' or current_status = 'RTO OFD' or current_status = 'RTO Delivered') and date(created_at) = '$current_date' and consignment_id in (select distinct consignment_id from consignment_updates where current_status = 'Out for Delivery' and date(created_at) = '$current_date')) group by consignment_id) and cu.consignment_id = c.id and c.customer_name = '$client_name' order by cu.drs_code");
					 
				  } 
		 }
		}			
		
			//DB::enableQueryLog();
            
			 if(!empty($ofd))
			  {
				foreach($ofd as $row1)
				{
					$ofd =  $row1->ofd;
				}						
			  }
			  else
			  {
				$ofd = 0; 
			  }
			  
			  
			  if(!empty($shipment_recieved))
			  {
				foreach($shipment_recieved as $row1)
				{
					$shipment_recieved =  $row1->shipment_recieved;
				}						
			  }
			  else
			  {
				$shipment_recieved = 0; 
			  }
			
			
			return Excel::create('consignments_data', function($excel)use ($last_status, $shipment_recieved, $ofd) {
            																			
            $excel->sheet('datas', function($sheet) use($last_status, $shipment_recieved, $ofd)
            {
				
				 $drs_array = array();
		         $current_drs = "";
		         $before_drs = "";
		   
				   foreach($last_status as $row)
				   {
					  $current_drs = $row->drs_code; 
					  {
						  if(!empty($current_drs))
						  {
							  if($current_drs != $before_drs)
							  $results = \DB::table('drs')->select('delivery_boy', 'mobile')	
											   ->where('drs_code', $row->drs_code)	
											   ->get();	
							  foreach($results as $result)
							  {
								$drs_array[$row->drs_code]['delivery_boy'] = $result->delivery_boy;
								$drs_array[$row->drs_code]['mobile'] = $result->mobile;
							  }
					     }
					  }
					  $before_drs = $current_drs;			  
				   }
				
				$arr =array();
				$i = 0;
				foreach($last_status as $row)
				{
					if(empty($client_name))
					{
						if($i == 0)
						{
						   if(!empty($row->drs_code))
						   {
							array_push($arr, [$i, $shipment_recieved, $ofd, $row->location, $row->awb, $row->customer_name, $row->drs_code, $drs_array[$row->drs_code]['delivery_boy'], $drs_array[$row->drs_code]['mobile'], $row->current_status, $row->remarks, $row->consignee, $row->consignee_address, $row->payment_mode, $row->collectable_value, $row->created_at, $row->updated_at, $row->updated_by_platform]);   
						   }
						   else
						   {
							   array_push($arr, [$i, $shipment_recieved, $ofd, $row->location, $row->awb, $row->customer_name, '-', '-', '-', $row->current_status, $row->remarks, $row->consignee, $row->consignee_address, $row->payment_mode, $row->collectable_value, $row->created_at, $row->updated_at, $row->updated_by_platform]);
						   }
						   
						}
						else
						{
							if(!empty($row->drs_code))
							{
							  array_push($arr, [$i, '-', '-', $row->location, $row->awb, $row->customer_name, $row->drs_code, $drs_array[$row->drs_code]['delivery_boy'], $drs_array[$row->drs_code]['mobile'], $row->current_status, $row->remarks, $row->consignee, $row->consignee_address, $row->payment_mode, $row->collectable_value, $row->created_at, $row->updated_at, $row->updated_by_platform]);
							}
							else
							{
							  array_push($arr, [$i, '-', '-', $row->location, $row->awb, $row->customer_name, '-', '-', '-', $row->current_status, $row->remarks, $row->consignee, $row->consignee_address, $row->payment_mode, $row->collectable_value, $row->created_at, $row->updated_at, $row->updated_by_platform]);	
							}
						}
					}
					else
					{
						
							if(!empty($row->drs_code))
							{
							  array_push($arr, [$i, $row->location, $row->awb, $row->customer_name, $row->drs_code, $drs_array[$row->drs_code]['delivery_boy'], $drs_array[$row->drs_code]['mobile'], $row->current_status, $row->remarks, $row->consignee, $row->consignee_address, $row->payment_mode, $row->collectable_value, $row->created_at, $row->updated_at, $row->updated_by_platform]);
							}
							else
							{
								array_push($arr, [$i, $row->location, $row->awb, $row->customer_name, '-', '-', '-', $row->current_status, $row->remarks, $row->consignee, $row->consignee_address, $row->payment_mode, $row->collectable_value, $row->created_at, $row->updated_at, $row->updated_by_platform]);
							}
					}
					$i++;
				}
				
                $sheet->fromArray($arr, null, 'A1', false, false);
                // Add before first row

				if(empty($client_name))
				{					
					$sheet->prependRow(1, array(
                    'S.No', 
					'Shipment Receieved',
					'OFD',
                    'Branch',
					'AWB',
					'Customer Name', 
					'DRS Code',
					'Delivery Boy',
					'Mobile',
					'Current Status',
					'Remarks',
					'Consignee Name',
					'Consignee Address',
					'Payment Mode',
					'Collectable Value',
					'Created At',
					'Updated At',
					'Updated By Platform'
                  ));
				}
				else
				{
					$sheet->prependRow(1, array(
                    'S.No', 
                    'Branch',
					'AWB',
					'Customer Name', 
					'DRS Code',
					'Delivery Boy',
					'Mobile',
					'Current Status',
					'Remarks',
					'Consignee Name',
					'Consignee Address',
					'Payment Mode',
					'Collectable Value',
					'Created At',
					'Updated At',
					'Updated By Platform'
                  ));
				}
                

				$sheet->cells('A1:AH1', function($cells) {
				
					// manipulate the range of cells
						   
				// Set font
				$cells->setFont(array(
					'family'     => 'Calibri',
					'size'       => '11',
					'bold'       =>  true,
					'background' => 'blue'
				));    
				
				// Set all borders (top, right, bottom, left)
				$cells->setBorder('solid', 'none', 'none', 'solid');

});
            });
        })->download('xls');
	 }
    }
	
	
	
	

}
