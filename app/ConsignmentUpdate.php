<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConsignmentUpdate extends Model
{
    //
   public $fillable = ['pod_code','consignment_id','last_updated_on','last_updated_by','location','current_status','remarks','bag_code','drs_code'];    


}
