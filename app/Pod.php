<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pod extends Model
{
    //
   public $fillable = ['pod_code','delivery_boy','delivery_date', 'created_by','route_no', 'vehicle_number', 'driver_name', 'mobile', 'transporter_name', 'vehicle_type', 'facility_name'];    

   protected $table = 'pod';

    public function pod_consignments()
    {
        return $this->hasMany('App\Consignment');
    }

}
