<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutsideConsignment extends Model
{
    //
   public $fillable = ['undelivered_reason','delivered_date_time','received_by','re_attempt','rto_awb','rto_reason','awb','current_status','remarks','prev_status','prev_sub_status','no_of_attempts','last_updated_on','last_updated_by','branch','delivery_boy'];    

    /**
     * Get the consignment updates for the consignment.
     */
    public function outside_consignment_updates()
    {
        return $this->hasMany('App\OutsideConsignmentUpdate');
    }

    /**
     * Get the bag that this consignment belongs to.
     */
}
