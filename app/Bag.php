<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bag extends Model
{
    //
   public $fillable = ['bag_code','seal_number','to_branch','in_transit','bag_id', 'is_verified'];    

    public function bag_consignments()
    {
        return $this->hasMany('App\Consignment');
    }

}
