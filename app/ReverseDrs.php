<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class ReverseDrs extends Model

{

    //

   //public $fillable = ['drs_code','delivery_boy','delivery_date', 'reverse_drs_id'];    
	protected $guarded = [];


   protected $table = 'reverse_drs';



    public function drs_reverse_consignments()

    {

        return $this->hasMany('App\ReverseConsignment');

    }



}

