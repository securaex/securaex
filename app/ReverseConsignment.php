<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ReverseConsignment extends Model
{
    //
   //public $fillable = ['received_by','pickup_date_time','pickup_fail_reason','pcs','re_attempt','rev_pod_code','rev_pod_id','data_received_date','awb','order_number ','customer_name','collectable_value','item_description','consignee','consignee_add1','consignee_add2','consignee_add3','destination_city','pincode','state','mobile','current_status','remarks','prev_status','no_of_attempts','last_updated_on','last_updated_by','rev_bag_code','rev_drs_code','rev_bag_id', 'rev_drs_id','actual_weight','volumetric_weight','declared_value','branch','position','pickedup_from'];    

    /**
     * Get the consignment updates for the consignment.
     */
	 
	 protected $guarded = [];
    public function reverse_consignment_updates()
    {
        return $this->hasMany('App\ReverseConsignmentUpdate');
    }

    /**
     * Get the bag that this consignment belongs to.
     */
    public function reverse_bag()
    {
        return $this->belongsTo('App\ReverseBag');
    }

    public function reverse_drs()
    {
        return $this->belongsTo('App\ReverseDrs');
    }



}