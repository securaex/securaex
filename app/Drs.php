<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drs extends Model
{
    //
   public $fillable = ['drs_code','delivery_boy','delivery_date', 'drs_id'];    

   protected $table = 'drs';

    public function drs_consignments()
    {
        return $this->hasMany('App\Consignment');
    }

}
