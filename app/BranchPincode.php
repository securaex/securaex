<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchPincode extends Model
{
	public $fillable = ['pincode','state','district','branch','branch_code']; 
	public $timestamps = false;

}
