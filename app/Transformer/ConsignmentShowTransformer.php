<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class ConsignmentShowTransformer extends TransformerAbstract {
 
    public function transform($consignment) {
		$status = array();$i=0;
		foreach($consignment->consignment_updates->sortByDesc('created_at') as $update)
		{
			$status[$i]['status']=$update->current_status;
			$status[$i]['remarks']=$update->remarks;
            $status[$i]['location']=$update->location;
			$status[$i]['date']=$update->created_at->format('Y-m-d g:i A');
			$i++;
		}
		
        return [
        'awb' => $consignment->awb,
        'order_id' => $consignment->order_id,
        'customer_name' => $consignment->customer_name,
        'payment_mode' => $consignment->payment_mode,
		'collectable_value' => $consignment->collectable_value,
        'item_description' => $consignment->item_description,
        'consignee' => $consignment->consignee,
        'consignee_address' => $consignment->consignee_address,
        'destination_city' => $consignment->destination_city,
        'pincode' => $consignment->pincode,
        'state' => $consignment->state,
        'mobile' => $consignment->mobile,
        'current_status' => $consignment->current_status,
        'remarks' => $consignment->remarks,
        'no_of_attempts' => $consignment->no_of_attempts,
        'last_updated_at' => $consignment->updated_at->toDateTimeString(),
        'last_updated_by' => $consignment->last_updated_by,
        'price' => $consignment->price,
        'branch' => $consignment->branch,
        'delivered_date_time' => $consignment->delivered_date_time,
		'history' => $status,
        ];
    }
 }
