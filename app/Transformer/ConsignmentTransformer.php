<?php 
namespace App\Transformer;
 
use League\Fractal\TransformerAbstract;
 
class ConsignmentTransformer extends TransformerAbstract {
 
    public function transform($consignment) {
        return [
		'message' => "Data Successfully Inserted",		
        'awb' => $consignment->awb,
        'order_id' => $consignment->order_id,
        'customer_name' => $consignment->customer_name,
        'payment_mode' => $consignment->payment_mode,
		'collectable_value' => $consignment->collectable_value,
        'item_description' => $consignment->item_description,
        'consignee' => $consignment->consignee,
        'consignee_address' => $consignment->consignee_address,
        'destination_city' => $consignment->destination_city,
        'pincode' => $consignment->pincode,
        'state' => $consignment->state,
        'mobile' => $consignment->mobile,
        'current_status' => $consignment->current_status,
        'remarks' => $consignment->remarks,
        'no_of_attempts' => $consignment->no_of_attempts,
        'last_updated_at' => $consignment->updated_at->toDateTimeString(),
        'last_updated_by' => $consignment->last_updated_by,
        'price' => $consignment->price,
        'branch' => $consignment->branch,
        'delivered_date_time' => $consignment->delivered_date_time,
        ];
    }
 }
