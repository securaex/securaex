<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    //
	public $fillable = ['driver_name','branch','vehicle_number','mobile','password','status','created_by'];
}
