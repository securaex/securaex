<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Consignment extends Model
{
    //
   public $fillable = ['received_by','delivered_date_time','undelivered_reason','rto_reason','pcs','payment_mode','re_attempt','pod_code','pod_id','data_received_date','awb','order_id','customer_name','collectable_value','item_description','consignee','consignee_address','destination_city','pincode','state','mobile','current_status','remarks','prev_status','prev_sub_status','no_of_attempts','last_updated_on','last_updated_by','bag_code','drs_code','bag_id', 'drs_id', 'status', 'weight','price','branch','position','caller_remarks','reviewed','caller_name','call_date','is_locked'];  
   
   

    /**
     * Get the consignment updates for the consignment.
     */
    public function consignment_updates()
    {
        return $this->hasMany('App\ConsignmentUpdate');
    }

    /**
     * Get the bag that this consignment belongs to.
     */
    public function bag()
    {
        return $this->belongsTo('App\Bag');
    }

    public function drs()
    {
        return $this->belongsTo('App\Drs');
    }

	public function scopeWithoutTimestamps()
    {
        $this->timestamps = false;
        return $this;
    }
	public function updateUntouched(array $attributes)
	{
		try {
			$timestamps = $this->timestamps;
			$this->timestamps = false;
	 
			return tap($this)->update($attributes);
		} finally {
			$this->timestamps = $timestamps;
		}
	}
	
	public function forceDelete()
    {
        // delete all related photos 
        $this->consignment_updates()->forceDelete();
        // as suggested by Dirk in comment,
        // it's an uglier alternative, but faster
        // Photo::where("user_id", $this->id)->delete()

        // delete the user
        return parent::forceDelete();
    }	
}