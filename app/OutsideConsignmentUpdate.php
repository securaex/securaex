<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutsideConsignmentUpdate extends Model
{
    //
   public $fillable = ['outside_consignment_id','last_updated_on','last_updated_by','location','current_status','remarks'];    


}
