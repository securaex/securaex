@extends('layouts.app')

@section('content')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <link href="/css/home.css" rel="stylesheet">
    
    



<div class="container">
<div class="row" style="margin:20px 0">

</div>
      <div class="row">  
      <form method="GET" action="/home" >

<div class="row">
        <div class="col-md-3">

         <div class="form-group">
            <label for="datepicker_from" class="col-md-3 control-label">From:</label>
            <div class="col-md-9">
               <input class="form-control" type="text" name="date_from" id="datepicker_from" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        
        <div class="col-md-3"> 
         <div class="form-group">
            <label for="datepicker_to" class="col-md-2 control-label">To:</label>
            <div class="col-md-9">
               <input class="form-control" type="text" name="date_to" id="datepicker_to" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        <div class="col-md-3">
         
         <div class="form-group">
            <label for="to_branch" class="col-md-3 control-label">Branch:</label>
            <div class="col-md-8">
               <select id="to_branch" class="form-control" name="branch"   autofocus>
                  <option value="">None</option>
                  @foreach($branches as $branch)
                  <option value="{{ ucfirst($branch->username) }}" @if(isset($_GET['branch']) &&  ($_GET['branch'] == ucfirst($branch->username))) selected  @endif>{{ ucfirst($branch->username) }}</option>
                  @endforeach
               </select>
            </div>
         </div>
        </div>
        
        <div class="col-md-3">
         <div class="form-group">
            <label for="customer_name" class="col-md-4 control-label">Customer:</label>
            <div class="col-md-8">
               <select id="customer_name" class="form-control" name="customer_name"   autofocus>
                  <option value="">None</option>
                  @foreach($customers as $customer)
                  <option value="{{ $customer->name }}" @if(isset($_GET['customer_name']) &&  ($_GET['customer_name'] == $customer->name)) selected  @endif >{{$customer->name}}</option>
                  @endforeach
               </select>
            </div>
         </div>
        </div>
         
        
</div>
<div class="row" style="margin-top:30px;">
<div class="col-md-2 col-md-offset-11">
         <div class="form-group">
          <div class="col-md-1">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
          </div>
         </div>
        </div>
</div>

      </form>
      </div>      



<div class="row" style="margin-top:50px">
<script type="text/javascript">

      // Load the Visualization API and the piechart package.
	  google.charts.load('current', {'packages':['corechart']});


      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(ofdReport);
	  google.setOnLoadCallback(deliveryAttempt);
	  google.setOnLoadCallback(codReports);
      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function ofdReport() {

// Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Delivered', <?php echo $delivered ?>],
          ['Undelivered', <?php echo $undelivered ?>],
		  ['Un-Attempted', <?php echo $ofd ?>],
        ]);

        // Set chart options
        var options = {'title':'OFD Report ( Total OFD - <?php echo $delivered + $undelivered + $ofd ?> )',
                       'width':370,
                       'height':300,
					   'colors':['#29AF31','#F5522B','#3366CC'],
					   	pieHole: 0.4,
						pieSliceTextStyle: {color: 'white', bold: true},
						chartArea:{left:10,right:10,top:50,width:'100%',height:'70%'},
						legend:{position: 'bottom'},
					   'pieSliceText':'value'};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div_ofdReport'));
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);

        var label1 = data.pieSliceText('label');

        // The selection handler.
        // Loop through all items in the selection and concatenate
        // a single message from all of them.
   function selectHandler() {
    var selection = chart.getSelection();
    if (selection.length) {
        var pieSliceLabel = data.getValue(selection[0].row, 0);
        window.location.href = '/home/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&branch=<?php if(isset($_GET['branch'])) { echo  $_GET['branch']; } ?>&customer_name=<?php if(isset($_GET['customer_name'])) { echo  $_GET['customer_name']; } ?>&type=' + pieSliceLabel;
    }
}

      }


      function deliveryAttempt() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['First Attempt', <?php echo $firstAttempt ?>],
          ['Re-Attempt', <?php echo $moreAttempt ?>],
        ]);

        var options = {'title':'Delivery Attempt ( Total Delivered - <?php echo $firstAttempt + $moreAttempt ?> )',
                       'width':370,
                       'height':300,
					   'colors':['#4C7FE5','#F5522B'],
					    pieHole: 0.4,
						pieSliceTextStyle: {color: 'white', bold: true},
						chartArea:{left:10,right:10,top:50,width:'100%',height:'70%'},
						legend:{position: 'bottom'},
					   'pieSliceText':'value'};

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_deliveryAttempt'));
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);

        var label1 = data.pieSliceText('label');

   function selectHandler() {
    var selection = chart.getSelection();
    if (selection.length) {
        var pieSliceLabel = data.getValue(selection[0].row, 0);
        //window.location.href = 'http://www.google.com/search?q=' + pieSliceLabel;
    }
		
}

      }
	  
	  
      function codReports() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Collected', <?php echo $codCollected ?>],
          ['Pending', <?php echo $codPending ?>],
        ]);

        var options = {'title':'COD Reports ( Total COD - Rs. <?php echo $codCollected + $codPending ?> )',
                       'width':370,
                       'height':300,
					   'colors':['#29AF31','#F5522B'],
					    pieHole: 0.4,
						pieSliceTextStyle: {color: 'white', bold: true},
						chartArea:{left:10,right:10,top:50,width:'100%',height:'70%'},
						legend:{position: 'bottom'},
					   'pieSliceText':'value'};

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_codReports'));
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);

        var label1 = data.pieSliceText('label');

   function selectHandler() {
    var selection = chart.getSelection();
    if (selection.length) {
        var pieSliceLabel = data.getValue(selection[0].row, 0);
        window.location.href = '/home/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&branch=<?php if(isset($_GET['branch'])) { echo  $_GET['branch']; } ?>&customer_name=<?php if(isset($_GET['customer_name'])) { echo  $_GET['customer_name']; } ?>&type=' + pieSliceLabel;
    }
		
}

      }
	  
    </script>
   <!-- /.row -->
         <div class="col-lg-4 col-md-4" style="border-right:1px dotted #333">
            <div id="chart_div_ofdReport"></div>
         </div>
         
         <div class="col-lg-4 col-md-4" style="border-right:1px dotted #333">
            <div id="chart_div_deliveryAttempt"></div>
         </div>
         
		<div class="col-lg-4 col-md-4">
            <div id="chart_div_codReports"></div>
         </div>
   <!-- /.col-lg-4 -->
</div>

<div class="row">
<div class="col-sm-12" style="height:10px; border-bottom:1px dotted #333"></div>
</div>
<div class="row" style="margin-top:10px; margin-bottom:10px">

<script type="text/javascript">
	  google.charts.load('current', {'packages':['corechart']});


      // Set a callback to run when the Google Visualization API is loaded.

	  google.setOnLoadCallback(undeliveredAnalysis);
	  google.setOnLoadCallback(deliveryTatDelhiNcr);
	  google.setOnLoadCallback(deliveryTatOustsideDelhi);
	  
      function undeliveredAnalysis() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
		<?php	
		foreach($undeliveredRemarks as $key => $value)
         echo "['".$key."', ".$value['count']." ],";
		?>  
        ]);

        var options = {'title':'Undelivered Analysis ( Total Undelivered - <?php echo $undelivered ?> )',
                       'width':370,
                       'height':300,
					   'is3D':true,
					    pieHole: 0.4,
						pieSliceTextStyle: {color: 'white', bold: true},
						chartArea:{left:10,right:10,top:50,width:'100%',height:'70%'},
						legend:{position: 'bottom'},
					   'pieSliceText':'value'};

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_undeliveredAnalysis'));
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);

        var label1 = data.pieSliceText('label');

function selectHandler() {
    var selection = chart.getSelection();
    if (selection.length) {
        var pieSliceLabel = data.getValue(selection[0].row, 0);
		window.location.href = '/home/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&branch=<?php if(isset($_GET['branch'])) { echo  $_GET['branch']; } ?>&customer_name=<?php if(isset($_GET['customer_name'])) { echo  $_GET['customer_name']; } ?>&type=undeliveredAnalysis&undeliveredReason=' + pieSliceLabel;
    }
}

      }
	  
      function deliveryTatDelhiNcr() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['On Time', <?php echo $onTimeDelhiNcr; ?>],
          ['1-2 Days', <?php echo $oneToTwoDaysDelhiNcr; ?>],
		  ['More Than 2 Days', <?php echo $moreThanTwoDaysDelhiNcr; ?>]
        ]);

        var options = {'title':'Delivery TAT - Delhi & NCR',
                       'width':370,
                       'height':300,
					   'colors':['#29AF31','orange','#F5522B'],
					    pieHole: 0.4,
						pieSliceTextStyle: {color: 'white', bold: true},
						chartArea:{left:10,right:10,top:50,width:'100%',height:'70%'},
						legend:{position: 'bottom'},
					   'pieSliceText':'value'};

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_deliveryTatDelhiNcr'));
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);

        var label1 = data.pieSliceText('label');

function selectHandler() {
    var selection = chart.getSelection();
    if (selection.length) {
        var pieSliceLabel = data.getValue(selection[0].row, 0);
		window.location.href = '/home/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&type=tatDelhi&tatStatus=' + pieSliceLabel;
    }
}

      }
	  
      function deliveryTatOustsideDelhi() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['On Time', <?php echo $onTimeOustsideDelhi; ?>],
          ['2-3 Days', <?php echo $oneToTwoDaysOustsideDelhi; ?>],
		  ['More Than 3 Days', <?php echo $moreThanTwoDaysOustsideDelhi; ?>]
        ]);

        var options = {'title':'Delivery TAT - Outside Delhi',
                       'width':370,
                       'height':300,
					   'colors':['#29AF31','orange','#F5522B'],
					    pieHole: 0.4,
						pieSliceTextStyle: {color: 'white', bold: true},
						chartArea:{left:10,right:10,top:50,width:'100%',height:'70%'},
						legend:{position: 'bottom'},
					   'pieSliceText':'value'};

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_deliveryTatOustsideDelhi'));
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);

        var label1 = data.pieSliceText('label');

function selectHandler() {
    var selection = chart.getSelection();
    if (selection.length) {
        var pieSliceLabel = data.getValue(selection[0].row, 0);
		window.location.href = '/home/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&type=tatOutsideDelhi&tatStatus=' + pieSliceLabel;
    }
}

      }
    </script>
   <!-- /.row -->
         
         
         <div class="col-lg-4 col-md-4" style="border-right:1px dotted #333">
            <div id="chart_div_undeliveredAnalysis"></div>
         </div>
         
         <div class="col-lg-4 col-md-4" style="border-right:1px dotted #333">
            <div id="chart_div_deliveryTatDelhiNcr"></div>
         </div>
         
         <div class="col-lg-4 col-md-4">
            <div id="chart_div_deliveryTatOustsideDelhi"></div>
         </div>
   <!-- /.col-lg-4 -->
</div>

<div class="row">
<div class="col-sm-12" style="margin-bottom:70px"> </div>
</div>



</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


  <script>
$.noConflict();  //Not to conflict with other scripts
jQuery(document).ready(function($) {


      var getUrlParameter = function getUrlParameter(sParam) {
      var sPageURL = decodeURIComponent(window.location.search.substring(1)),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i;

      for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
              return sParameterName[1] === undefined ? true : sParameterName[1];
          }
      }
  };

if(getUrlParameter('date_from')){
var date_from = getUrlParameter('date_from');

}else{
var date_from = new Date();
}

if(getUrlParameter('date_to')){
var date_to = getUrlParameter('date_to');

}else{
var date_to = new Date();
}


    $( "#datepicker_from" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat : 'yy-mm-dd',
        defaultDate: new Date(),
    }).datepicker("setDate", date_from);

    $( "#datepicker_to" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat : 'yy-mm-dd',
        defaultDate: new Date(),
    }).datepicker("setDate", date_to);
	

  });
  </script>



@endsection
