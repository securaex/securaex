@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Bag</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/bags/{{ $bag->id }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group{{ $errors->has('bag_code') ? ' has-error' : '' }}">
                            <label for="bag_code" class="col-md-4 control-label">Bag Code</label>

                            <div class="col-md-6">
                                <input id="bag_code" type="text" class="form-control" name="bag_code" value="{{ $bag->bag_code }}" required autofocus>

                                @if ($errors->has('bag_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bag_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('to_branch') ? ' has-error' : '' }}">
                            <label for="to_branch" class="col-md-4 control-label">To Branch</label>

                            <div class="col-md-6">
                                <select id="to_branch" class="form-control" name="to_branch" value="{{ old('to_branch') }}" required autofocus>
                                    
                                    <option value="Ghaziabad" <?php if($bag->to_branch == 'Ghaziabad') echo 'selected'; ?> >Ghaziabad</option>
                                    <option value="Rangpuri" <?php if($bag->to_branch == 'Rangpuri') echo 'selected'; ?> >Rangpuri</option>
                                    <option value="Jasola" <?php if($bag->to_branch == 'Jasola') echo 'selected'; ?> >Jasola</option>
                                    <option value="ODA" <?php if($bag->to_branch == 'ODA') echo 'selected'; ?> >ODA</option>
                                    <option value="Other" <?php if($bag->to_branch == 'Other') echo 'selected'; ?> >Other</option>
                                </select>
                                @if ($errors->has('to_branch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('to_branch') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
