@extends('layouts.app')
@section('content')
<div class="container-fluid">

<div class="row"  id="search-results">
   <div class="col-md-11 col-md-offset-1">
      <div class="shipmentTracking"><h4>Total Records Found : {{ $consignments->total() }} </h4>
      <h4>Not In Scanned : {{ $notOutScanned->count() }} </h4> </div>
      <p>
      <div class="row">  
      <form method="GET" action="/shipmentTracking" >

<div class="row">
        
        
        <div class="col-md-4"> 
         <div class="form-group">
            <label for="datepicker_to" class="col-md-6 control-label">Select Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_to" id="datepicker_to" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        <div class="col-md-4">
         <div class="form-group">
          <div class="col-md-4">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
          </div>
         </div>
        </div>

</div>


      </form>
      </div>      
      </p>
      <br />
      <div class="panel panel-default panel-table">
         <div class="panel-body">
            <table class="table table-striped table-bordered table-list">
               <thead>
                  <tr>
                     <th>In Scanned</th>
                     <!--<th class="hidden-xs">ID</th>-->
                     <th>Data Received Date</th>
                     <th>AWB</th>
                     <th>Customer Name</th>
                     <th>Pincode</th>
                     <th>Last Updated BY</th>
                     <th>Last Updated AT</th>
                     <th>Current Status</th>
                     <th>Branch</th>
                     <th>Attempts</th>
                  </tr>
               </thead>
               @foreach($consignments as $consignment)
               		@if($consignment->current_status == "Soft Data Upload")
               <tbody>
                  <tr>
                     <td align="center"><em class="fa fa-times fa-2x text-danger" aria-hidden="true"></em></td>
                     <td>{{ $consignment->created_at->format('Y-m-d g:i A') }}</td>
                     <td>{{ $consignment->awb }}</td>
                     <td>{{ $consignment->customer_name }}</td>
                     <td>{{ $consignment->pincode }}</td>
                     <td>{{ $consignment->last_updated_by }}</td>
                     <td>{{ $consignment->updated_at->format('Y-m-d g:i A') }}</td>
                     <td>{{ $consignment->current_status }}</td>
                     <td>{{ $consignment->branch }}</td>
                     <td>{{ $consignment->no_of_attempts }}</td>
                  </tr>
               </tbody>
                    @endif
               @endforeach
               
               @forelse($consignments as $consignment)
               @if($consignment->current_status != "Soft Data Upload")
               <tbody>
                  <tr>
                     <td align="center"><em class="fa fa-check fa-2x text-success" aria-hidden="true"></em></td>
                     <td>{{ $consignment->data_received_date }}</td>
                     <td>{{ $consignment->awb }}</td>
                     <td>{{ $consignment->customer_name }}</td>
                     <td>{{ $consignment->pincode }}</td>
                     <td>{{ $consignment->last_updated_by }}</td>
                     <td>{{ $consignment->updated_at->format('Y-m-d g:i A') }}</td>
                     <td>{{ $consignment->current_status }}</td>
                     <td>{{ $consignment->branch }}</td>
                     <td>{{ $consignment->no_of_attempts }}</td>
                  </tr>
               </tbody>
               @endif
               @empty
               No Consignments.
               @endforelse
            </table>
            
         </div>
         <div class="panel-footer">
            <div class="row">
               <div class="col col-xs-4">Page {{ $consignments->currentPage() }} of {{ $consignments->lastPage()  }}
               </div>
               <div class="col col-xs-8 pull-right">
                  <ul class="pagination hidden-xs pull-right">
                     {{ $consignments->appends(request()->input())->links() }} 
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $.noConflict();  //Not to conflict with other scripts
   jQuery(document).ready(function($) {
   
   
         var getUrlParameter = function getUrlParameter(sParam) {
         var sPageURL = decodeURIComponent(window.location.search.substring(1)),
             sURLVariables = sPageURL.split('&'),
             sParameterName,
             i;
   
         for (i = 0; i < sURLVariables.length; i++) {
             sParameterName = sURLVariables[i].split('=');
   
             if (sParameterName[0] === sParam) {
                 return sParameterName[1] === undefined ? true : sParameterName[1];
             }
         }
     };
   
   if(getUrlParameter('date_from')){
   var date_from = getUrlParameter('date_from');
   
   }else{
   var date_from = new Date();
   }
   
   if(getUrlParameter('date_to')){
   var date_to = getUrlParameter('date_to');
   
   }else{
   var date_to = new Date();
   }
   
   
       $( "#datepicker_from" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_from);
   
       $( "#datepicker_to" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_to);
   
   
     } );
     
</script>
<script>
  /* jQuery(document).ready(function(e){
       $('.search-panel .dropdown-menu').find('a').click(function(e) {
       e.preventDefault();
       var param = $(this).attr("href").replace("#","");
       if(param){
         $('#search_box').attr('disabled', false); 
         $('#search_box').attr('placeholder', $(this).attr("data")); 
       }
       var concept = $(this).text();
       $('.search-panel span#search_concept').text(concept);
       $('.input-group #search_param').val(param);
     });
   });
   
   
   var timer;
   function up() {
       timer = setTimeout(function() { 
           var keywords = $('#search_box').val(); 
           var param = $('#search_param').val(); 
           if (keywords.length > 0) { 
             $("#loading").show();
             $.ajax({
                 url: 'ajax-search-consignments',
                 method: 'GET',
                 data:{keywords: keywords, param : param}
             }).done(function(response){
                 $("#loading").hide();
                 $('#search-results').html(response);          
             });
           }
       }, 500);
   }
   
   function down() {
       clearTimeout(timer);
   }*/
   
   
   
</script>

@endsection