@extends('layouts.app')
@section('content')
<div class="container">
   <div id="page-wrapper" class="page-wrapper-cls">
      <div id="page-inner">
         <div class="row">
            <div class="col-md-12">
               <div class="alert alert-warning">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Shipment Current Status 
                     </div>
                     <div class="panel-body">
                        <table class="table table-bordered" >
                           <tbody>
  
                              <tr>
                                 <td style="width:40%"><strong>Shipment Upload Date</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_lbluploaddate">{{ $consignment->created_at->format('Y-M-d g:i A') }} </span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"><strong>AirWaybillnumber</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_lblAirWaybillnumber">{{ $consignment->awb }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"><strong>Current Status</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label22">{{ $consignment->current_status }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"> <strong>Consignee Name</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label3">{{ $consignment->consignee }}</span></td>
                              </tr>
							  <tr>
                                 <td style="width:40%"> <strong>Consignee Mobile</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label3">{{ $consignment->mobile }}</span></td>
                              </tr>	
                              <tr>
                                 <td style="width:40%"> <strong>Consignee Address</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label3">{{ $consignment->consignee_address }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"> <strong>Product Description</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label3">{{ $consignment->item_description }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"> <strong>Payment Mode</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label3">{{ $consignment->payment_mode }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"> <strong>COD Amount</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label3">{{ $consignment->collectable_value }}</span></td>
                              </tr>
                              @if($consignment->drs)
                              <tr>
                                 <td style="width:40%"> <strong>Delivery Boy</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label3">{{ $consignment->drs->delivery_boy }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"> <strong>Delivery Boy Mobile</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label3">{{ $consignment->drs->mobile }}</span></td>
                              </tr>
                              @else
                              <tr>
                                 <td style="width:40%"> <strong>Delivery Boy</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label3">No delivery boy</span></td>
                              </tr>
                              @endif
                              <tr>
                                 <td style="width:40%"><strong>Last Update Date</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label24">{{ $consignment->updated_at }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"><strong>Branch</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label19">{{ $consignment->branch }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"><strong>Remarks</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label23">{{ $consignment->remarks }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"><strong>Caller Remarks</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label23">{{ $consignment->caller_remarks }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"><strong>Caller Name</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label23">{{ $consignment->caller_name }}</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"><strong>Weight</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label1">@if($consignment->weight && $consignment->weight < 1000) {{ $consignment->weight }} kg @else {{ $consignment->weight / 1000 }} kg @endif</span></td>
                              </tr>
                              <tr>
                                 <td style="width:40%"><strong>Price</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label1">@if($consignment->price) {{ $consignment->price }} Rs /- @endif</span></td>

                              <tr>
                                 <td style="width:40%"><strong>SMS Response - OFD</strong></td>
                                 <td style="width:60%">  <span id="ctl00_ContentPlaceHolder1_repshipment_ctl01_Label1">@if($consignment->message_ofd_response) {{ $consignment->message_ofd_response}}  @endif</span></td>

                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
                  <!--
                     <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Message Log
                                    </div>
                                    <div class="panel-body">
                                   
                                      <div class="form-group">
                                          <span id="ctl00_ContentPlaceHolder1_lblhistory" style="color:Red;"></span>
                                      </div>
                                    </div>
                     </div>
                     -->
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Shipment History 
                     </div>
                     <div class="panel-body">
                        <div id="ctl00_ContentPlaceHolder1_pnlhistory">
                           <div>
                              <table class="table table-bordered" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_GridView2" style="width:100%;border-collapse:collapse;">
                                 <tbody>
                                    <tr>
                                       <th scope="col">S No</th>
                                       <th scope="col">Updated On</th>
                                       <th scope="col">Location</th>
                                       <th scope="col">Updated By</th>
                                       <th scope="col">Status</th>
                                       <th scope="col">Remarks</th>
                                       <th scope="col">Bag Code</th>
                                       <th scope="col">DRSCode</th>
                                       <th scope="col">PODCode</th>
                                    </tr>
                                    <?php  $count=$consignment->consignment_updates->count(); ?>
                                    @foreach($consignment->consignment_updates->sortByDesc('created_at') as $update) 
                                    <tr>
                                       <td style="width:30px;">
                                          {{ $count }}
                                       </td>
                                       <td style="width:0px;">
                                          <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblcause4">{{ $update->created_at->format('Y-m-d g:i A') }}</span>
                                       </td>
                                       <td style="width:0px;">
                                          <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblshipmentdate">{{ $update->location }}</span>
                                       </td>
                                       <td style="width:0px;">
                                          <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblupdatedby">{{ $update->last_updated_by }}</span>
                                       </td>
                                       <td style="width:0px;">
                                          <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblbagcode">{{ $update->current_status }}</span>
                                       </td>
                                       <td style="width:0px;">
                                          <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblbagcode">{{ $update->remarks }}</span>
                                       </td>
                                       <td style="width:0px;">
                                          <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblbagcode">{{ $update->bag_code }}</span>
                                       </td>
                                       <td style="width:0px;">
                                          <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblshipmentdate">{{ $update->drs_code }}</span>
                                       </td>
                                       <td style="width:0px;">
                                          <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblshipmentdate">{{ $update->pod_code }}</span>
                                       </td>
                                    </tr>
                                    <?php $count-- ?>
                                    @endforeach 
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection