@extends('layouts.app')
@section('content')
<div class="container-fluid">
<div class="row"  id="search-results">
   <div class="col-md-11 col-md-offset-1">
<div class="row" style="margin-top:60px">

<form method="GET" action="/api/downloadExcelType" >
        <div class="col-md-3">

         <div class="form-group">
            <label for="datepicker_from" class="col-md-4 control-label">From Date:</label>
            <div class="col-md-8">
               <input class="form-control" type="text" name="date_from" id="datepicker_from" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        
        <div class="col-md-3"> 
         <div class="form-group">
            <label for="datepicker_to" class="col-md-4 control-label">To Date:</label>
            <div class="col-md-8">
               <input class="form-control" type="text" name="date_to" id="datepicker_to" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        <div class="col-md-3">
         <div class="form-group">
            <label for="customer_name" class="col-md-4 control-label">Customer:</label>
            <div class="col-md-8">
               <select id="customer_name" class="form-control" name="customer_name"   autofocus>
                  <option value="all">All</option>
                  @foreach($customers as $customer)
                  <option value="{{ $customer->name }}" @if(isset($_GET['customer_name']) &&  ($_GET['customer_name'] == $customer->name)) selected  @endif >{{$customer->name}}</option>
                  @endforeach
               </select>
            </div>
         </div>
        </div>
        <div class="col-md-3">
         <div class="form-group">
          <div class="col-md-4">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Download Excel</button>
          </div>
         </div>
        </div>
      </form>
</div>   </div>
</div>
<div class="row" style="margin-top:20px">
	<div class="col-md-10 col-md-offset-1">
				@if ($message = Session::get('error'))
					<div class="alert alert-danger alert-dismissible" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
						{{ Session::get('error') }}
					</div>
				@endif
	</div>                
</div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $.noConflict();  //Not to conflict with other scripts
   jQuery(document).ready(function($) {
   
   
         var getUrlParameter = function getUrlParameter(sParam) {
         var sPageURL = decodeURIComponent(window.location.search.substring(1)),
             sURLVariables = sPageURL.split('&'),
             sParameterName,
             i;
   
         for (i = 0; i < sURLVariables.length; i++) {
             sParameterName = sURLVariables[i].split('=');
   
             if (sParameterName[0] === sParam) {
                 return sParameterName[1] === undefined ? true : sParameterName[1];
             }
         }
     };
   
   if(getUrlParameter('date_from')){
   var date_from = getUrlParameter('date_from');
   
   }else{
   var date_from = new Date();
   }
   
   if(getUrlParameter('date_to')){
   var date_to = getUrlParameter('date_to');
   
   }else{
   var date_to = new Date();
   }
   
   
       $( "#datepicker_from" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_from);
   
       $( "#datepicker_to" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_to);
   
   
     } );
     
</script>
<script>
   jQuery(document).ready(function(e){
       $('.search-panel .dropdown-menu').find('a').click(function(e) {
       e.preventDefault();
       var param = $(this).attr("href").replace("#","");
       if(param){
         $('#search_box').attr('disabled', false); 
         $('#search_box').attr('placeholder', $(this).attr("data")); 
       }
       var concept = $(this).text();
       $('.search-panel span#search_concept').text(concept);
       $('.input-group #search_param').val(param);
     });
   });
   
   
   var timer;
   function up() {
       timer = setTimeout(function() { 
           var keywords = $('#search_box').val(); 
           var param = $('#search_param').val(); 
           if (keywords.length > 0) { 
             $("#loading").show();
             $.ajax({
                 url: 'ajax-search-consignments',
                 method: 'GET',
                 data:{keywords: keywords, param : param}
             }).done(function(response){
                 $("#loading").hide();
                 $('#search-results').html(response);          
             });
           }
       }, 500);
   }
   
   function down() {
       clearTimeout(timer);
   }
   
   
   
</script>
@endsection