@extends('layouts.app')
@section('content')
<div class="container-fluid">
<div class="row">
   <div class="col-xs-8 col-xs-offset-2">
      <div class="input-group">
         <div class="input-group-btn search-panel">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <span id="search_concept">Filter by</span> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
               <li><a href="#awb" data="e.g. CPTP392612">Air waybill (AWB)</a></li>
               <li><a href="#drd" data="e.g. YYYY-mm-dd">Data Received Date</a></li>
               <li class="divider"></li>
            </ul>
         </div>
         <input type="hidden" name="search_param" value="all" id="search_param">         
         <input type="text" class="form-control" name="x" id="search_box" placeholder="Search term..." disabled="disabled" onkeyup="up()" onkeydown="down()">
         <span class="input-group-btn">
         <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="true"></i>
         </button>
         </span>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-xs-8 col-xs-offset-4">
      <img id="loading" src="{{URL::asset('/image/loading-dash.gif')}}" style="display:none" alt="Loading..." height="200" width="250">
   </div>
</div>
<div class="row"  id="search-results">
   <div class="col-md-11 col-md-offset-1">
      <h4>Total Records Found : {{ $consignments->total() }} </h4>
      <p>
      <div class="row">  
      <form method="GET" action="/consignments" >

<div class="row">
        <div class="col-md-4">

         <div class="form-group">
            <label for="datepicker_from" class="col-md-6 control-label">Updated From Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_from" id="datepicker_from" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        
        <div class="col-md-4"> 
         <div class="form-group">
            <label for="datepicker_to" class="col-md-6 control-label">Updated To Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_to" id="datepicker_to" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        <div class="col-md-4">
         
         <div class="form-group">
            <label for="to_branch" class="col-md-6 control-label">Sort By Branch:</label>
            <div class="col-md-6">
               <select id="to_branch" class="form-control" name="branch"   autofocus>
                  <option value="">None</option>
                  @foreach($branches as $branch)
                  <option value="{{ ucfirst($branch->username) }}" @if(isset($_GET['branch']) &&  ($_GET['branch'] == ucfirst($branch->username))) selected  @endif>{{ ucfirst($branch->username) }}</option>
                  @endforeach
               </select>
            </div>
         </div>
        </div>

</div>
<div class="row" style="margin-top:10px;">

        <div class="col-md-4">
         <div class="form-group">
            <label for="customer_name" class="col-md-6 control-label">Sort By Client:</label>
            <div class="col-md-6">
               <select id="customer_name" class="form-control" name="customer_name"   autofocus>
                  <option value="">None</option>
                  @foreach($customers as $customer)
                  <option value="{{ $customer->name }}" @if(isset($_GET['customer_name']) &&  ($_GET['customer_name'] == $customer->name)) selected  @endif >{{$customer->name}}</option>
                  @endforeach
               </select>
            </div>
         </div>
        </div>

        <div class="col-md-4">
         <div class="form-group">
            <label for="customer_name" class="col-md-6 control-label">Sort By Current Status:</label>
            <div class="col-md-6">
               <select id="current_status" class="form-control" name="current_status"   autofocus>
                  <option value="">None</option>
                 <?php $status = \Config::get('constants'); unset($status['deliveryStatus2'], $status['deliveryStatus4'], $status['notCollected'], $status['collected'], $status['inScanhubRev'], $status['outScanToClient'], $status['reverseFinalstatus']);  foreach ($status as $key => $value) {  ?>
                  <option value="{{ $value }}" @if(isset($_GET['current_status']) &&  ($_GET['current_status'] == $value)) selected  @endif  >{{$value}}</option>

                 <?php } ?>
				<option value="Lost" @if(isset($_GET['current_status']) &&  ($_GET['current_status'] == "Lost")) selected  @endif  >Lost</option>
              <option value="RTO Undelivered" @if(isset($_GET['current_status']) &&  ($_GET['current_status'] == "RTO Undelivered")) selected  @endif  >RTO Undelivered</option>
               </select>
            </div>
         </div>
        </div>
         
        <div class="col-md-4">
         <div class="form-group">
          <div class="col-md-4">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
          </div>
         </div>
        </div>
</div>

      </form>
      </div>      
      </p>
      <br />
      <div class="panel panel-default panel-table">
         <div class="panel-heading">
            <div class="row">
               <div class="col col-xs-6">
                  <h3 class="panel-title">Consignments</h3>
               </div>
               <div class="col col-xs-6 text-right">
                  <a href="/consignments/create"><button type="button" class="btn btn-sm btn-primary btn-create">Add New</button></a>
                  <a href="/consignments/exportExcel?date_from=<?php if(isset($_GET['date_from'])) { echo  $_GET['date_from']; } ?>&date_to=<?php if(isset($_GET['date_to'])) { echo  $_GET['date_to']; } ?>&branch=<?php if(isset($_GET['branch'])) { echo  $_GET['branch']; } ?>&customer_name=<?php if(isset($_GET['customer_name'])) { echo  $_GET['customer_name']; } ?>&current_status=<?php if(isset($_GET['current_status'])) { echo  $_GET['current_status']; } ?>"><button type="button" class="btn btn-sm btn-primary btn-create">Export</button></a>
               </div>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-list">
               <thead>
                  <tr>
                     <th><em class="fa fa-cog"></em></th>
                     <!--<th class="hidden-xs">ID</th>-->
                     <th>Data Received Date</th>
                     <th>AWB</th>
                     <th>Client Name</th>
                     <th>Payment Type</th>
                     <th>Amount</th>
                     <th>Delivery Boy</th>
                     <th>Pincode</th>
                     <th>Last Updated BY</th>
                     <th>Last Updated AT</th>
                     <th>Current Status</th>
                     <th>Branch</th>
                     <th>Attempts</th>
                  </tr>
               </thead>
               <!--<?php $action = false; $rto_count = 0; //$i=0;?>-->
               @forelse($consignments as $consignment)
               <!--
                  <?php if(empty($consignment->rto_reason)) $action=true; else {$rto_count = $rto_count+1 ;} ?>
                  -->
               <tbody>
                  <tr>
                     <td align="center">
                        <a target="_blank" class="btn btn-default" href="/consignments/{{ $consignment->id }}" ><em class="fa fa-eye"></em></a>                                  
                        <a class="btn btn-default" href="/consignments/{{ $consignment->id }}/edit" ><em class="fa fa-pencil"></em></a>
                      @if(Auth::user()->username === 'aman-hub' or Auth::user()->username === 'pankaj-hub' or Auth::user()->username === 'ved_prakash' or Auth::user()->username === 'Sunil_KR')
                        <?php if($consignment->is_locked == 1){ ?>
                        <form action="/consignments/unlock/{{ $consignment->id }}" method="POST" class="pull-right" onsubmit="return confirm('Are you sure you want to unlock this consignment for OFD?')" >{{ csrf_field() }} <button type="submit" class="btn btn-danger"><em class="fa fa-lock" ></em></button></form>
                        <?php } ?>
                      @endif
@if(Auth::user()->username === 'hub-master' or Auth::user()->username === 'Sunil_KR')
                        <form action="/consignments/{{ $consignment->id }}" method="POST" class="pull-right" onsubmit="return confirm('Are you sure you want to delete?')" >{{ csrf_field() }} {{ method_field('DELETE') }}<button type="submit" class="btn btn-danger"><em class="fa fa-trash" ></em></button></form>
                     
@endif

</td>
                     <!--<td class="hidden-xs">{{ $consignment->id }}</td>-->
                     <td>{{ $consignment->created_at->format('Y-m-d g:i A') }}</td>
                     <td>{{ $consignment->awb }}</td>
                     <td>{{ $consignment->customer_name }}</td>
                     <td>{{ $consignment->payment_mode }}</td>
                     
                     <td>
                         @if(strtoupper($consignment->payment_mode)=="COD")
                            {{ $consignment->collectable_value }}
                         @else
                            {{ 0 }}
                         @endif      
                     </td>
                     <td>
                         @if($consignment->drs)
                            {{ $consignment->drs->delivery_boy }}
                         @endif   
                     </td>
                     <td>{{ $consignment->pincode }}</td>
                     <td>{{ $consignment->last_updated_by }}</td>
                     <td>{{ $consignment->updated_at->format('Y-m-d g:i A') }}</td>
                     <td>{{ $consignment->current_status }}</td>
                     <td>{{ $consignment->branch }}</td>
                     <td>{{ $consignment->no_of_attempts }}</td>
                  </tr>
               </tbody>
               <?php //$i = $i+1; ?>
               @empty
               No Consignments.
               @endforelse
            </table>
            <!--<?php echo "RTO Found : " . $rto_count; ?>-->
         </div>
         <div class="panel-footer">
            <div class="row">
               <div class="col col-xs-4">Page {{ $consignments->currentPage() }} of {{ $consignments->lastPage()  }}
               </div>
               <div class="col col-xs-8 pull-right">
                  <ul class="pagination hidden-xs pull-right">
                     {{ $consignments->appends(request()->input())->links() }} 
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $.noConflict();  //Not to conflict with other scripts
   jQuery(document).ready(function($) {
   
   
         var getUrlParameter = function getUrlParameter(sParam) {
         var sPageURL = decodeURIComponent(window.location.search.substring(1)),
             sURLVariables = sPageURL.split('&'),
             sParameterName,
             i;
   
         for (i = 0; i < sURLVariables.length; i++) {
             sParameterName = sURLVariables[i].split('=');
   
             if (sParameterName[0] === sParam) {
                 return sParameterName[1] === undefined ? true : sParameterName[1];
             }
         }
     };
   
   if(getUrlParameter('date_from')){
   var date_from = getUrlParameter('date_from');
   
   }else{
   var date_from = new Date();
   }
   
   if(getUrlParameter('date_to')){
   var date_to = getUrlParameter('date_to');
   
   }else{
   var date_to = new Date();
   }
   
   
       $( "#datepicker_from" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_from);
   
       $( "#datepicker_to" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_to);
   
   
     } );
     
</script>
<script>
   jQuery(document).ready(function(e){
       $('.search-panel .dropdown-menu').find('a').click(function(e) {
       e.preventDefault();
       var param = $(this).attr("href").replace("#","");
       if(param){
         $('#search_box').attr('disabled', false); 
         $('#search_box').attr('placeholder', $(this).attr("data")); 
       }
       var concept = $(this).text();
       $('.search-panel span#search_concept').text(concept);
       $('.input-group #search_param').val(param);
     });
   });
   
   
   var timer;
   function up() {
       timer = setTimeout(function() { 
           var keywords = $('#search_box').val(); 
           var param = $('#search_param').val(); 
           if (keywords.length > 0) { 
             $("#loading").show();
             $.ajax({
                 url: 'ajax-search-consignments',
                 method: 'GET',
                 data:{keywords: keywords, param : param}
             }).done(function(response){
                 $("#loading").hide();
                 $('#search-results').html(response);          
             });
           }
       }, 500);
   }
   
   function down() {
       clearTimeout(timer);
   }
   
   
   
</script>
@endsection