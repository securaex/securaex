@extends('layouts.app')



@section('content')
<div class="container-fluid">

<div class="row"  id="search-results">

  <div class="col-md-11 col-md-offset-1">
    <h4></h4>
    <div class="panel panel-default panel-table">
      <div class="panel-heading">
	  
	    <div class="row">
		   <div class="col col-md-12 col-xs-6">
		   
		   <p style="color:red">Date Must Be Selected in both fields<p><br>
		   
		   <form method="GET" action="" onsubmit="rider_report(event, this)">

<div class="row">
        <div class="col-md-3">

         <div class="form-group">
            <label for="datepicker_from" class="col-md-6 control-label">From Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="from_date" id="datepicker_from" placeholder="yyyy-mm-dd" value="<?php echo (!empty($from_date) ? $from_date : ''); ?>">
            </div>
         </div>
        </div>
        
        <div class="col-md-3"> 
         <div class="form-group">
            <label for="datepicker_to" class="col-md-6 control-label">To Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="to_date" id="datepicker_to" placeholder="yyyy-mm-dd" value="<?php echo (!empty($to_date) ? $to_date : '') ?>">
            </div>
         </div>
        </div>
		
		<div class="col-md-3"> 
         <div class="form-group">
		   <label for="to_branch" class="col-md-6 control-label">Branch:</label>
            <div class="col-md-6">
               <select id="branch" class="form-control" name="branch"   autofocus>
                  <option value="" selected>Select Branch</option>
				  <option value="all">All</option>
				  <?php
				    if(!empty($users_results))
				    {
				  ?>
                  @foreach($users_results as $branches)
                  <option value="{{ ucfirst($branches->username) }}" @if(isset($_GET['branch']) &&  ($_GET['branch'] == ucfirst($branches->username))) selected  @endif>{{ ucfirst($branches->username) }}</option>
                  @endforeach
				  <?php
				    }
					else
					{
						?>
						  <option value="">Branches not Found</option>
						<?php
					}
				  ?>
               </select>
            </div>
         </div>
        </div>
		
		<div class="col-md-3">
         <div class="form-group">
          <div class="col-md-4">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
          </div>
         </div>
        </div>
       

</div>


      </form>
		   
		   
		   
		   
		      
		   </div>
		</div>
	  <br>
        <div class="row">
          <div class="col col-xs-6">
            <h3 class="panel-title">RIDER PERFORMANCE REPORT</h3>
			<br>
			<?php
			  if(empty($from_date) || empty($to_date))
			  {
				echo "Results showing for todays' date";  
			  }
			  else if(!empty($from_date) && !empty($to_date))
			  {
				  echo "Results showing for ".$from_date;
			  }
			  else
			  {
				  echo "Results showing for todays' date";
			  }
			  if(!empty($branch))
			  {
				  echo " &nbsp;&nbsp;| &nbsp;&nbsp; Branch - ".$branch;
			  }
			?>
			
			<?php
			    
				if(!empty($from_date) && !empty($to_date) && !empty($branch))
				{
					echo "<a style='float:right' href='branch_sdd_performance/export?from_date=$from_date&to_date=$to_date&branch=$branch'>Export</a>";
				}
				if(!empty($from_date) && !empty($to_date) && empty($branch))
				{
					echo "<a style='float:right' href='branch_sdd_performance/export?from_date=$from_date&to_date=$to_date&branch='>Export</a>";
				}
				if(empty($from_date) && empty($to_date) && !empty($branch))
				{
					echo "<a style='float:right' href='branch_sdd_performance/export?from_date=&to_date=&branch=$branch'>Export</a>";
				}
				if(empty($from_date) && empty($to_date) && empty($branch))
				{
					echo "<a style='float:right' href='branch_sdd_performance/export?from_date=&to_date=&branch='>Export</a>";
				}
				
			  ?>
			
          </div>
          <div class="col col-xs-6 text-right"> <a href="/drivers/create">
            <!-- <button type="button" class="btn btn-sm btn-primary btn-create">Add New Driver</button> -->
            </a> </div>
        </div>
      </div>
      <div class="panel-body">
	   
		<br><br>
        <table style="color:black" class="table table-striped table-bordered table-list">
          <thead>
            <tr>
              <th>Branch</th>
              <th>No Of shipments Received	</th>
              <th>OFD</th>
              <th>Delivered</th>
			  <th>Fresh</th>
			  <th>OLD</th>
			  <th>SDD Performance</th>
			  <th>Target</th>
            </tr>
          </thead>
          <tbody style="font-weight:bold">
	       
	     </tbody>
        </table>
      </div>
      <div class="panel-footer">
        <div class="row">
          <div class="col col-xs-4"></div>
          <div class="col col-xs-8 pull-right">
            <ul class="pagination hidden-xs pull-right">
              
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

$.noConflict();  //Not to conflict with other scripts

jQuery(document).ready(function($) {





      var getUrlParameter = function getUrlParameter(sParam) {

      var sPageURL = decodeURIComponent(window.location.search.substring(1)),

          sURLVariables = sPageURL.split('&'),

          sParameterName,

          i;



      for (i = 0; i < sURLVariables.length; i++) {

          sParameterName = sURLVariables[i].split('=');



          if (sParameterName[0] === sParam) {

              return sParameterName[1] === undefined ? true : sParameterName[1];

          }

      }

  };



if(getUrlParameter('date')){

var date = getUrlParameter('date');



}else{

var date = new Date();

}



    
   $( "#datepicker_from" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           //defaultDate: new Date(),
       });
   
       $( "#datepicker_to" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           //defaultDate: new Date(),
       });
  
  
}

 );
  
function rider_report(e, element)
{
	alert();
	e.preventDefault();
	var from_date = $('#datepicker_from').val();
	var to_date = $('#datepicker_to').val();
	var branch = $('#branch').val();
	var diffDays;
	var counter;
	if(from_date == '' && to_date == '' && branch != '')
	{
		if(branch == 'all')
		{
			  
		}
		else
		{
			  
		}
	}
	else if(from_date != '' && to_date != '' && branch != '')
	{
		var date1 = new Date(from_date);
        var date2 = new Date(to_date);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        if(date2.getTime() >= date1.getTime())
		{
			alert('positive');
		    diffDays = Math.ceil(timeDiff / (1000*3600*24));
			counter = diffDays;
			
			var i=0;
			while(counter >= 0)
			{
				alert("counter = "+counter);
				$rider_performace_url = "rider_performace?from_date="+from_date+"&branch="+branch+"flag="+i;
				alert($rider_performace_url);
				counter--;
				i++;
				alert(date1);
			}
			
		}		
		else
		{
            alert('To-Date cant be smaller than From-Date');			
        }
	}
	
}


  </script>
@endsection 