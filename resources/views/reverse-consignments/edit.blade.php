@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Reverse Consignment
                    <span class="pull-right" >
                        WARNING !! Reset Consginment >>>

                        <form class="pull-right" action="/reverse-consignments/reset/{{ $consignment->id }}" method="POST" onsubmit="return confirm('Are you sure you want to reset this consignment to orignial state? WARNING !! All updates done to this consignment by Hub & Branch will go away, Soft Data Update will remain as it is !')" >{{ csrf_field() }} {{ method_field('DELETE') }}<button type="submit" class="btn btn-danger"><em class="fa fa-refresh" ></em></button></form>
                    
                    </span>


                    <span class="pull-right" >
                        WARNING !! Reset Branch >>>

                        <form class="pull-right" action="/reverse-consignments/resetbranch/{{ $consignment->id }}" method="POST" onsubmit="return confirm('Are you sure you want to reset this consignment branch? WARNING !!')" >{{ csrf_field() }} {{ method_field('POST') }}<input type="hidden" name="branch" value="{{ $consignment->branch }}" /><input type="hidden" name="pincode" value="{{ $consignment->pincode }}" /><button type="submit" class="btn btn-danger"><em class="fa fa-refresh" ></em></button></form>
                    
                    </span>


                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/reverse-consignments/{{ $consignment->id }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}


                        <div class="form-group{{ $errors->has('branch') ? ' has-error' : '' }}">
                            <label for="branch" class="col-md-4 control-label">Branch Assigned</label>

                            <div class="col-md-6">


                                <!--<input readonly id="branch" type="text" class="form-control" name="branch" value="{{ $consignment->branch }}"  autofocus>-->
                                
                                <select id="to_branch" class="form-control" name="branch" autofocus>
                  <option value="">None</option>
                  @foreach($branches as $branch)
                  <option value="{{ ucfirst($branch->username) }}" @if(isset($consignment) &&  ($consignment->branch == ucfirst($branch->username))) selected  @endif>{{ ucfirst($branch->username) }}</option>
                  @endforeach
               </select>
                                @if ($errors->has('branch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('branch') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('data_received_date') ? ' has-error' : '' }}">
                            <label for="data_received_date" class="col-md-4 control-label">Data Received Date</label>

                            <div class="col-md-6">
                                <input id="data_received_date" type="text" class="form-control" name="data_received_date" value="{{ $consignment->data_received_date }}"  autofocus>

                                @if ($errors->has('data_received_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('data_received_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('awb') ? ' has-error' : '' }}">
                            <label for="awb" class="col-md-4 control-label">AWB</label>

                            <div class="col-md-6">
                                <input id="awb" type="text" class="form-control" name="awb" value="{{ $consignment->awb }}"  autofocus>

                                @if ($errors->has('awb'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('awb') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('order_id') ? ' has-error' : '' }}">
                            <label for="order_number" class="col-md-4 control-label">Order Number</label>

                            <div class="col-md-6">
                                <input id="order_number" type="text" class="form-control" name="order_number" value="{{ $consignment->order_number }}"  autofocus>

                                @if ($errors->has('order_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('order_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('customer_name') ? ' has-error' : '' }}">
                            <label for="customer_name" class="col-md-4 control-label">Customer Name</label>

                            <div class="col-md-6">
                                <input id="customer_name" type="text" class="form-control" name="customer_name" value="{{ $consignment->customer_name }}"  autofocus>

                                @if ($errors->has('customer_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('customer_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('collectable_value') ? ' has-error' : '' }}">
                            <label for="collectable_value" class="col-md-4 control-label">Collectable Value</label>

                            <div class="col-md-6">
                                <input id="collectable_value" type="number" class="form-control" name="collectable_value" value="{{ $consignment->collectable_value }}"  autofocus>

                                @if ($errors->has('collectable_value'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('collectable_value') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('item_description') ? ' has-error' : '' }}">
                            <label for="item_description" class="col-md-4 control-label">Item Description</label>

                            <div class="col-md-6">
                                <textarea class="form-control" name="item_description"   autofocus>{{ $consignment->item_description }}</textarea>

                                @if ($errors->has('item_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('item_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('consignee') ? ' has-error' : '' }}">
                            <label for="consignee" class="col-md-4 control-label">Consignee</label>

                            <div class="col-md-6">
                                <textarea class="form-control" name="item_description"   autofocus>{{ $consignment->consignee }}</textarea>

                                @if ($errors->has('consignee'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('consignee') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('consignee_address') ? ' has-error' : '' }}">
                            <label for="consignee_address" class="col-md-4 control-label">Consignee Address</label>

                            <div class="col-md-6">
                                <textarea class="form-control" name="consignee_address"   autofocus>{{ $consignment->consignee_address }}</textarea>

                                @if ($errors->has('consignee_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('consignee_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('destination_city') ? ' has-error' : '' }}">
                            <label for="destination_city" class="col-md-4 control-label">Destination City</label>

                            <div class="col-md-6">
                                <input id="destination_city" type="text" class="form-control" name="destination_city" value="{{ $consignment->destination_city }}"  autofocus>

                                @if ($errors->has('destination_city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('destination_city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('pincode') ? ' has-error' : '' }}">
                            <label for="pincode" class="col-md-4 control-label">Pincode</label>

                            <div class="col-md-6">
                                <input id="pincode" type="text" class="form-control" name="pincode" value="{{ $consignment->pincode }}"  autofocus>

                                @if ($errors->has('pincode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pincode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            <label for="state" class="col-md-4 control-label">State</label>

                            <div class="col-md-6">
                                <input id="state" type="text" class="form-control" name="state" value="{{ $consignment->state }}"  autofocus>

                                @if ($errors->has('state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ $consignment->mobile }}"  autofocus>

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!--
                        <div class="form-group{{ $errors->has('current_status') ? ' has-error' : '' }}">
                            <label for="current_status" class="col-md-4 control-label">Current Status</label>

                            <div class="col-md-6">
                                <input id="current_status" type="text" class="form-control" name="current_status" value="{{ $consignment->current_status }}"  autofocus>

                                @if ($errors->has('current_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('current_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        -->
                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            <label for="remarks" class="col-md-4 control-label">Remarks</label>

                            <div class="col-md-6">
                                <input id="remarks" type="text" class="form-control" name="remarks" value="{{ $consignment->remarks }}"  autofocus>

                                @if ($errors->has('remarks'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('remarks') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!--
                        <div class="form-group{{ $errors->has('prev_status') ? ' has-error' : '' }}">
                            <label for="prev_status" class="col-md-4 control-label">Previous Status</label>

                            <div class="col-md-6">
                                <input id="prev_status" type="text" class="form-control" name="prev_status" value="{{ $consignment->prev_status }}"  autofocus>

                                @if ($errors->has('prev_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('prev_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        -->

                        <!--<div class="form-group{{ $errors->has('prev_sub_status') ? ' has-error' : '' }}">
                            <label for="prev_sub_status" class="col-md-4 control-label">Previous Sub Status</label>

                            <div class="col-md-6">
                                <input id="prev_sub_status" type="text" class="form-control" name="prev_sub_status" value="{{ $consignment->prev_sub_status }}"  autofocus>

                                @if ($errors->has('prev_sub_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('prev_sub_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>-->

                        <div class="form-group{{ $errors->has('no_of_attempts') ? ' has-error' : '' }}">
                            <label for="no_of_attempts" class="col-md-4 control-label">No of Attempts</label>

                            <div class="col-md-6">
                                <input id="no_of_attempts" type="number" class="form-control" name="no_of_attempts" value="{{ $consignment->no_of_attempts }}"  autofocus>

                                @if ($errors->has('no_of_attempts'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_of_attempts') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('last_updated_on') ? ' has-error' : '' }}">
                            <label for="last_updated_on" class="col-md-4 control-label">Last Updated_On</label>

                            <div class="col-md-6">
                                <input id="last_updated_on" type="text" class="form-control" name="last_updated_on" value="{{ $consignment->last_updated_on }}"  autofocus>

                                @if ($errors->has('last_updated_on'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_updated_on') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('last_updated_by') ? ' has-error' : '' }}">
                            <label for="last_updated_by" class="col-md-4 control-label">Last Updated_By</label>

                            <div class="col-md-6">
                                <input id="last_updated_by" type="text" class="form-control" name="last_updated_by" value="{{ $consignment->last_updated_by }}"  autofocus>

                                @if ($errors->has('last_updated_by'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_updated_by') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('reverse_bag_code') ? ' has-error' : '' }}">
                            <label for="reverse_bag_code" class="col-md-4 control-label">Bag Code</label>

                            <div class="col-md-6">
                                <input id="reverse_bag_code" type="text" class="form-control" name="reverse_bag_code" value="{{ $consignment->reverse_bag_code }}"  autofocus>

                                @if ($errors->has('reverse_bag_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reverse_bag_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('reverse_drs_code') ? ' has-error' : '' }}">
                            <label for="reverse_drs_code" class="col-md-4 control-label">Drs Code</label>

                            <div class="col-md-6">
                                <input id="reverse_drs_code" type="text" class="form-control" name="reverse_drs_code" value="{{ $consignment->reverse_drs_code }}"  autofocus>

                                @if ($errors->has('reverse_drs_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('reverse_drs_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('actual_weight') ? ' has-error' : '' }}">
                            <label for="actual_weight" class="col-md-4 control-label">Weight (in grams)</label>

                            <div class="col-md-6">
                                <input id="actual_weight" type="text" class="form-control" name="actual_weight" value="{{ $consignment->actual_weight }}"  autofocus>

                                @if ($errors->has('actual_weight'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('actual_weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('pcs') ? ' has-error' : '' }}">
                            <label for="pcs" class="col-md-4 control-label">Quantity</label>

                            <div class="col-md-6">
                                <input id="pcs" type="text" class="form-control" name="pcs" value="{{ $consignment->pcs }}"  autofocus>

                                @if ($errors->has('pcs'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pcs') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('message-ofd-response') ? ' has-error' : '' }}">
                            <label for="message-ofd-response" class="col-md-4 control-label">SMS Response - OFD</label>

                            <div class="col-md-6">


                                <textarea class="form-control" name="message_ofd_response"   autofocus>{{ $consignment->message_ofd_response }}</textarea>

                                @if ($errors->has('message-ofd-response'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message-ofd-response') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
