@extends('layouts.app')
@section('content')
<div class="container">

<div class="row"  id="search-results">
   <div class="col-md-12">
      <h4></h4>
      <p>
      <div class="row">  
      <form method="GET" >

<div class="row">
        <div class="col-md-4">

         <div class="form-group">
            <label for="datepicker_from" class="col-md-6 control-label">Updated From Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_from" id="datepicker_from" placeholder="yyyy-mm-dd" value="<?php echo (!empty($date_from) ? $date_from : '') ?>">
            </div>
         </div>
        </div>
        
        <div class="col-md-4"> 
         <div class="form-group">
            <label for="datepicker_to" class="col-md-6 control-label">Updated To Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_to" id="datepicker_to" placeholder="yyyy-mm-dd" value="<?php echo (!empty($date_to) ? $date_to : '') ?>">
            </div>
         </div>
        </div>
		
        <div class="col-md-4">
         <div class="form-group">
		 <?php
		   
		 ?>
            <label for="to_branch" class="col-md-6 control-label">Sort By Branch:</label>
            <div class="col-md-6">
               <select id="to_branch" class="form-control" name="branch"   autofocus>
                  <!-- <option value="">None</option> -->
				  <option value="all" selected>All</option>
                  @foreach($branches as $branch)
                  <option value="{{ ucfirst($branch->username) }}" @if(isset($_GET['branch']) &&  ($_GET['branch'] == ucfirst($branch->username))) selected  @endif>{{ ucfirst($branch->username) }}</option>
                  @endforeach
               </select>
            </div>
         </div>
        </div>
		
		
		<!-- <div class="col-md-4">
         
         <div class="form-group">
		 <?php
		   
		 ?>
            <!-- <label for="to_branch" class="col-md-6 control-label">Sort By Client:</label>
            <div class="col-md-6">
               <select id="to_branch" class="form-control" name="client_name"   autofocus>
                  <option value="">None</option>
                  @foreach($customers as $customer)
                  <option value="{{ ucfirst($customer->customer_name) }}" @if(isset($_GET['client_name']) &&  ($_GET['client_name'] == ucfirst($customer->customer_name))) selected  @endif>{{ ucfirst($customer->customer_name) }}</option>
                  @endforeach
               </select>
            </div 
         </div>
        </div> -->

</div>


        
         
        <div class="col-md-4">
         <div class="form-group">
          <div class="col-md-4">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
          </div>
         </div>
        </div>
</div>

      </form>
      </div>      
      </p>
      <br />
      <div class="panel panel-default panel-table">
         <div class="panel-heading">
            <div class="row">
               <div class="col-md-12">
                  <h3 class="panel-title">Consignments</h3>
				  
				  <span>
				    <?php
					 if(empty($_GET['client_name']))
					 {
					 if(!empty($shipment_recieved))
					  {
						foreach($shipment_recieved as $row)
						{
							echo "Shipment Recieved - ". $row->shipment_recieved;
							$sr = $row->shipment_recieved;
						}						
					  }
					  else
					  {
						  ?>
						    shipment Recieved - 0
						  <?php
					  }
					 }
					?>
				  </span>
				   
				    &nbsp;&nbsp;&nbsp;&nbsp;
				  
				  <span>
				    <?php
					if(empty($_GET['client_name']))
					{
					  if(!empty($ofd))
					  {
						foreach($ofd as $row)
						{
							echo "OFD - ". $row->ofd;
							$of = $row->ofd;
						}						
					  }
					  else
					  {
						  ?>
						    OFD - 0
						  <?php
					  }
					}
					?>
				  </span>
				  
				  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  
				 <!--  <span>
				    <?php
					  if(!empty($last_status))
					  {
						echo 'Updated - '.count($last_status);						
					  }
					  else
					  {
						  ?>
						    OFD - 0
						  <?php
					  }
					?>
				  </span> -->
				   <div style="float:right">
			  <?php
			  
			    if(empty($_GET['client_name']))
				{
					if(!empty($date_from) && !empty($date_to) && !empty($mybranch))
					{
						?>
						  <a href="<?php echo 'new_consignments/exportxls?date_from='.$date_from.'&date_to='.$date_to.'&branch='.$mybranch ?>">Export</a>
						<?php
					}
					 else if(empty($date_from) && empty($date_to) && !empty($mybranch))
					{
						?>
						  <a href="<?php echo 'new_consignments/exportxls?branch='.$mybranch ?>">Export</a>
						<?php
					}
					else if(empty($date_from) && empty($date_to) && empty($mybranch))
					{
						?>
						  <a href="<?php echo 'new_consignments/exportxls' ?>">Export</a>
						<?php
					}
				}
				else
				{
					if(!empty($date_from) && !empty($date_to) && !empty($mybranch))
					{
						?>
						  <a href="<?php echo 'new_consignments/exportxls?date_from='.$date_from.'&date_to='.$date_to.'&branch='.$mybranch.'&client_name='.$_GET['client_name'] ?>">Export</a>
						<?php
					}
					 else if(empty($date_from) && empty($date_to) && !empty($mybranch))
					{
						?>
						  <a href="<?php echo 'new_consignments/exportxls?branch='.$mybranch.'&client_name='.$_GET['client_name'] ?>">Export</a>
						<?php
					}
					else if(empty($date_from) && empty($date_to) && empty($mybranch))
					{
						?>
						  <a href="<?php echo 'new_consignments/exportxls'.'&client_name='.$_GET['client_name'] ?>">Export</a>
						<?php
					}
				}
				
			    
			  ?>
			</div>
               </div>
               
            </div>
         </div>
		 
         <div class="panel-body">
		   
            <table style="font-size:0.9em; color:black" class="table table-striped table-bordered table-list col-md-12">
               <thead>
                  <tr>
                     <!-- <th><em class="fa fa-cog"></em></th>-->
                     <!--<th class="hidden-xs">ID</th>-->
					 
					 <th>S. No</th>
					 <?php
					 if(empty($_GET['client_name']))
					 {
						?>
						<th>Shipment Recieved</th>
						<?php
					 }
					 ?>
					 <th>OFD</th>
                     <th>Branch</th>
					 <th>AWB</th>
					 <th>Customer Name</th>
                     <th>DRS Code</th>
                     <th>Delivery Boy</th>
					 <th>Mobile</th>
                     
                     <th>Day End Status </th>
					 <th>Remarks</th>
					 <th>Consignee Name</th>
					 <th>Consignee Address</th>
					 <th>Payment mode</th>
                     <th>Collectable Value</th>
					 <th>Created At</th>
					 <th>Updated At</th>
					 <th>Updated By Platform</th>
					 
                  </tr>
               </thead>
               <!--<?php $action = false; $rto_count = 0; //$i=0;?>-->
			
                <?php if(!empty($last_status))
				{
					 $i = 0;
					 $current_drs = '';
					 $before_drs = '';
					?>
					@foreach($last_status as $row)
                    <tr>
					
					 <td><?php echo $i++; ?></td>
					 
					 <?php
					   if(empty($_GET['client_name']))
					   {
					   ?>
					     <td>
						 <?php
						   if($i == 1)
						   {
							  if(!empty($sr))
							  {
							     echo $sr;						
							  }
							  else
							  {
								  
									 
								
						      }
						   }
						   else
						   {
						     echo '-';
						   }
						 ?>
						 </td>
					   <?php
					   }  
				     ?>
					
					
					 
					 <td id="client_ofd">
					   <?php
					     if($i == 1)
						 {
							  if(!empty($of))
							  {
							
									echo $of;
												
							  }
							  else
							  {
								  ?>
									 0
								  <?php
							  } 
						 }
						 else
						 {
						   echo '-';
						 }
					   ?>
					 </td>
				     <td> <?php  echo(!empty($row->location) ? $row->location : '-') ?>  </td>
                     <td> <?php echo (!empty($row->awb) ? $row->awb : '-') ?>  </td>
					 <td> <?php echo (!empty($row->customer_name) ? $row->customer_name : '-') ?>  </td>
					 <td> 
					   <?php 
					      echo (!empty($row->drs_code) ? $row->drs_code : '-') ;
						?>  
					 </td>
					 <td> 
					    <?php echo (!empty($drs_array[$row->drs_code]) ? $drs_array[$row->drs_code]['delivery_boy'] : '-') ?>  </td>
					 <td> 
					    <?php echo (!empty($drs_array[$row->drs_code]) ? $drs_array[$row->drs_code]['mobile'] : '-') ?> 
						</td>
					 <td> <?php echo (!empty($row->current_status) ? $row->current_status : '-') ?>  </td>
					 <td> <?php echo (!empty($row->remarks) ? $row->remarks : '-') ?>  </td>
					 
					 <td> <?php echo (!empty($row->consignee) ? $row->consignee : '-') ?>  </td>
					 <td> <?php echo (!empty($row->consignee_address) ? $row->consignee_address : '-') ?>  </td>
					 <td> <?php echo (!empty($row->payment_mode) ? $row->payment_mode : '-') ?>  </td>
					 <td> <?php echo (!empty($row->collectable_value) ? $row->collectable_value : '-') ?>  </td>
					 <td> <?php echo (!empty($row->created_at) ? $row->created_at : '-') ?>  </td>
					 <td> <?php echo (!empty($row->updated_at) ? $row->updated_at : '-') ?>  </td>
					 <td> <?php echo (!empty($row->updated_by_platform) ? $row->updated_by_platform : 'web') ?>  </td>
					 
					 
					 
                   </tr>
				  @endforeach 
				  <?php
				}
				  ?>
               </tbody>
               
            </table>
            <!--<?php echo "RTO Found : " . $rto_count; ?>-->
         </div>
         <div class="panel-footer">
            <div class="row">
               <div class="col col-xs-4 col-md-12">
               </div>
               <div class="col col-xs-8 pull-right col-md-12">
                  <ul class="pagination hidden-xs pull-right">
                     
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $.noConflict();  //Not to conflict with other scripts
   jQuery(document).ready(function($) {
   
   
         var getUrlParameter = function getUrlParameter(sParam) {
         var sPageURL = decodeURIComponent(window.location.search.substring(1)),
             sURLVariables = sPageURL.split('&'),
             sParameterName,
             i;
   
         for (i = 0; i < sURLVariables.length; i++) {
             sParameterName = sURLVariables[i].split('=');
   
             if (sParameterName[0] === sParam) {
                 return sParameterName[1] === undefined ? true : sParameterName[1];
             }
         }
     };
   
   if(getUrlParameter('date_from')){
   var date_from = getUrlParameter('date_from');
   
   }else{
   var date_from = new Date();
   }
   
   if(getUrlParameter('date_to')){
   var date_to = getUrlParameter('date_to');
   
   }else{
   var date_to = new Date();
   }
   
   
       $( "#datepicker_from" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_from);
   
       $( "#datepicker_to" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_to);
   
   
     } );
     
</script>
<script>
   jQuery(document).ready(function(e){
       $('.search-panel .dropdown-menu').find('a').click(function(e) {
       e.preventDefault();
       var param = $(this).attr("href").replace("#","");
       if(param){
         $('#search_box').attr('disabled', false); 
         $('#search_box').attr('placeholder', $(this).attr("data")); 
       }
       var concept = $(this).text();
       $('.search-panel span#search_concept').text(concept);
       $('.input-group #search_param').val(param);
     });
   });
   
   
   var timer;
   function up() {
       timer = setTimeout(function() { 
           var keywords = $('#search_box').val(); 
           var param = $('#search_param').val(); 
           if (keywords.length > 0) { 
             $("#loading").show();
             $.ajax({
                 url: 'ajax-search-consignments',
                 method: 'GET',
                 data:{keywords: keywords, param : param}
             }).done(function(response){
                 $("#loading").hide();
                 $('#search-results').html(response);          
             });
           }
       }, 500);
   }
   
   function down() {
       clearTimeout(timer);
   }
   
   
   <?php
     if(!empty($_GET['client_name']))
	 {
		 ?>
		document.getElementById('client_ofd').innerHTML = <?php echo $i; ?>; 
		<?php
	 }
   ?>
   
   
</script>
@endsection