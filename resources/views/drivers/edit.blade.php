@extends('layouts.app')



@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Edit Driver</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="/drivers/{{ $driver->id }}">
            {{ csrf_field() }}
            
            {{ method_field('PUT') }}
            <div class="form-group{{ $errors->has('driver_name') ? ' has-error' : '' }}">
              <label for="driver_name" class="col-md-4 control-label">Driver Name</label>
              <div class="col-md-6">
                <input id="driver_name" type="text" class="form-control" name="driver_name" value="{{ $driver->driver_name }}" required autofocus>
                @if ($errors->has('driver_name')) <span class="help-block"> <strong>{{ $errors->first('driver_name') }}</strong> </span> @endif </div>
            </div>
            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
              <label for="mobile" class="col-md-4 control-label">Mobile Number</label>
              <div class="col-md-6">
                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ $driver->mobile }}" required autofocus>
                @if ($errors->has('mobile')) <span class="help-block"> <strong>{{ $errors->first('mobile') }}</strong> </span> @endif </div>
            </div>
            <div class="form-group{{ $errors->has('vehicle_number') ? ' has-error' : '' }}">
              <label for="vehicle_number" class="col-md-4 control-label">Vehicle Number</label>
              <div class="col-md-6">
                <input id="vehicle_number" type="text" class="form-control" name="vehicle_number" value="{{ $driver->vehicle_number }}" required autofocus>
                @if ($errors->has('vehicle_number')) <span class="help-block"> <strong>{{ $errors->first('vehicle_number') }}</strong> </span> @endif </div>
            </div>
            <div class="form-group">
            <label for="branch" class="col-md-4 control-label">Branch:</label>
            <div class="col-md-6">
               <select id="branch" class="form-control" name="branch" required autofocus>
                  <option value="">None</option>
                  @foreach($branches as $branch)
                  <option value="{{ ucfirst($branch->username) }}" @if(isset($driver) &&  ($driver->branch == ucfirst($branch->username))) selected  @endif>{{ ucfirst($branch->username) }}</option>
                  @endforeach
               </select>
            </div>
         </div>
            <input type="hidden" name="created_by" value="{{ Auth::user()->username }}" />
            <br />
            <br />
            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary"> Update </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection 