@extends('layouts.app')



@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Add New Driver</div>
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="/drivers">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('driver_name') ? ' has-error' : '' }}">
              <label for="driver_name" class="col-md-4 control-label">Driver's Name</label>
              <div class="col-md-6">
                <input id="drs_code" type="text" class="form-control" name="driver_name" value="{{old('driver_name')}}" autofocus >
                @if ($errors->has('driver_name')) <span class="help-block"> <strong>{{ $errors->first('driver_name') }}</strong> </span> @endif </div>
            </div>
            <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
              <label for="mobile" class="col-md-4 control-label">Mobile Number</label>
              <div class="col-md-6">
                <input type="text" class="form-control" name="mobile" value="{{old('mobile')}}" autofocus >
                @if ($errors->has('mobile')) <span class="help-block"> <strong>{{ $errors->first('mobile') }}</strong> </span> @endif </div>
            </div>
            <div class="form-group{{ $errors->has('vehicle_number') ? ' has-error' : '' }}">
              <label for="vehicle_number" class="col-md-4 control-label">Vehicle Number</label>
              <div class="col-md-6">
                <input type="text" class="form-control" name="vehicle_number" value="{{old('vehicle_number')}}" autofocus >
                @if ($errors->has('vehicle_number')) <span class="help-block"> <strong>{{ $errors->first('vehicle_number') }}</strong> </span> @endif </div>
            </div>
            <div class="form-group{{ $errors->has('branch') ? ' has-error' : '' }}">
            <label for="branch" class="col-md-4 control-label">Select Branch:</label>
            <div class="col-md-6">
               <select id="branch" class="form-control" name="branch" required autofocus>
                  <option value="">None</option>
                  @foreach($branches as $branch)
                  <option value="{{ ucfirst($branch->username) }}" @if(isset($_GET['branch']) &&  ($_GET['branch'] == ucfirst($branch->username))) selected  @endif>{{ ucfirst($branch->username) }}</option>
                  @endforeach
               </select>@if ($errors->has('branch')) <span class="help-block"> <strong>{{ $errors->first('branch') }}</strong> </span> @endif
            </div>
         </div>
            <input type="hidden" name="created_by" value="{{ Auth::user()->username }}" />
            <input type="hidden" name="password" value="pass@123" />
            <input type="hidden" name="status" value="1" />
            <br />
            <div class="form-group">
              <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary"> Add Driver </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@endsection 