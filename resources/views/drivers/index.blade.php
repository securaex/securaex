@extends('layouts.app')



@section('content')
<div class="container-fluid">
<!--<div class="row">
<div class="col-md-11 col-md-offset-1">
<div class="col-md-4">
         <form method="GET" action="drivers" >
         <div class="form-group">
            <label for="to_branch" class="col-md-6 control-label">Sort By Branch:</label>
            <div class="col-md-6">
               <select id="to_branch" class="form-control" name="branch"   autofocus>
                  <option value="">None</option>
                  @foreach($branches as $branch)
                  <option value="{{ ucfirst($branch->username) }}" @if(isset($_GET['branch']) &&  ($_GET['branch'] == ucfirst($branch->username))) selected  @endif>{{ ucfirst($branch->username) }}</option>
                  @endforeach
               </select>
            </div>
         </div>
        </div>
<div class="col-md-5">
         <div class="form-group">
          
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
         
         </div>
        </div>

</div>
</div></form>-->
<div class="row"  id="search-results">

  <div class="col-md-11 col-md-offset-1">
    <h4>Toatal Records Found : {{ $drivers->total() }} </h4>
    <div class="panel panel-default panel-table">
      <div class="panel-heading">
        <div class="row">
          <div class="col col-xs-6">
            <h3 class="panel-title">Drivers</h3>
          </div>
          <div class="col col-xs-6 text-right"> <a href="/drivers/create">
            <button type="button" class="btn btn-sm btn-primary btn-create">Add New Driver</button>
            </a> </div>
        </div>
      </div>
      <div class="panel-body">
        <table class="table table-striped table-bordered table-list">
          <thead>
            <tr>
              <th width="110"><em class="fa fa-cog"></em></th>
              <!--<th class="hidden-xs">ID</th>-->
              <th>Driver Name</th>
              <th>Branch</th>
              <th>Created By</th>
              <th>Created Date</th>
              <th>Updated Date</th>
            </tr>
          </thead>
          @forelse($drivers as $driver)
          <tbody>
            <tr>
              <td align="center"><a class="btn btn-default" href="/drivers/{{ $driver->id }}/edit" ><em class="fa fa-pencil"></em></a>
                <form action="/drivers/{{ $driver->id }}" method="POST" class="pull-right" onsubmit="return confirm('Are you sure you want to delete?')" >
                  {{ csrf_field() }} {{ method_field('DELETE') }}
                  <button type="submit" class="btn btn-danger"><em class="fa fa-trash" ></em></button>
                </form></td>
              <td>{{ $driver->driver_name }}</td>
              <td>{{ $driver->branch }}</td>
              <td>{{ $driver->created_by }}</td>
              <td>{{ $driver->created_at->format('Y-m-d g:i A') }}</td>
              <td>{{ $driver->updated_at->format('Y-m-d g:i A') }}</td>
            </tr>
          </tbody>
          @empty
          
          No Drivers.
          
          
          
          @endforelse
        </table>
      </div>
      <div class="panel-footer">
        <div class="row">
          <div class="col col-xs-4">Page {{ $drivers->currentPage() }} of {{ $drivers->lastPage()  }} </div>
          <div class="col col-xs-8 pull-right">
            <ul class="pagination hidden-xs pull-right">
              {{ $drivers->appends(request()->input())->links() }}
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

$.noConflict();  //Not to conflict with other scripts

jQuery(document).ready(function($) {





      var getUrlParameter = function getUrlParameter(sParam) {

      var sPageURL = decodeURIComponent(window.location.search.substring(1)),

          sURLVariables = sPageURL.split('&'),

          sParameterName,

          i;



      for (i = 0; i < sURLVariables.length; i++) {

          sParameterName = sURLVariables[i].split('=');



          if (sParameterName[0] === sParam) {

              return sParameterName[1] === undefined ? true : sParameterName[1];

          }

      }

  };



if(getUrlParameter('date')){

var date = getUrlParameter('date');



}else{

var date = new Date();

}





    $( "#datepicker" ).datepicker({

        changeMonth: true,

        changeYear: true,

        dateFormat : 'yy-mm-dd',

        defaultDate: new Date(),

    }).datepicker("setDate", date);

  } );

  </script>
@endsection 