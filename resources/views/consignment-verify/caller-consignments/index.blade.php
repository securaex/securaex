@extends('layouts.app')
@section('content')
<div class="container-fluid">

<div class="row"  id="search-results">
   <div class="col-md-10 col-md-offset-1">
      <h4>Total Records Found : {{ $consignments->total() }} </h4>
      <p>
      <div class="row">  
      <form method="GET" action="/caller-consignments" >

<div class="row">
        <div class="col-md-4">

         <div class="form-group">
            <label for="datepicker_from" class="col-md-6 control-label">Updated From Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_from" id="datepicker_from" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        
        <div class="col-md-4"> 
         <div class="form-group">
            <label for="datepicker_to" class="col-md-6 control-label">Updated To Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_to" id="datepicker_to" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        <div class="col-md-4">
         
         <div class="form-group">
            <label for="to_branch" class="col-md-6 control-label">Sort By Branch:</label>
            <div class="col-md-6">
               <select id="to_branch" class="form-control" name="branch"   autofocus>
                  <option value="">None</option>
                  @foreach($branches as $branch)
                  <option value="{{ ucfirst($branch->username) }}" @if(isset($_GET['branch']) &&  ($_GET['branch'] == ucfirst($branch->username))) selected  @endif>{{ ucfirst($branch->username) }}</option>
                  @endforeach
               </select>
            </div>
         </div>
        </div>

</div>
<div class="row" style="margin-top:10px;">

        <div class="col-md-4 col-md-offset-8">
         <div class="form-group pull-right">
          <div class="col-md-4">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
          </div>
         </div>
        </div>
</div>

      </form>
      </div>      
      </p>
      <br />
      <div class="panel panel-default panel-table">
         
         <div class="panel-body">
            <table class="table table-striped table-bordered table-list">
               <thead>
                  <tr>
                     <th>AWB</th>
                     <th>Customer Name</th>
                     <th>Mobile</th>
                     <th>Product Desc.</th>
                     <th>Remarks</th>
                     <th>Caller Remarks</th>
                     <th>Attempt Date</th>
                     <th>No. Of Attempts</th>
                     <th><em class="fa fa-cog"></em></th>
                  </tr>
               </thead>
               <!--<?php $action = false; $rto_count = 0; //$i=0;?>-->
               @forelse($consignments as $consignment)
               <!--
                  <?php if(empty($consignment->rto_reason)) $action=true; else {$rto_count = $rto_count+1 ;} ?>
                  -->
               <tbody>
                  <tr>
                     <td>{{ $consignment->awb }}</td>
                     <td>{{ $consignment->consignee }}</td>
                     <td>{{ $consignment->mobile }}</td>
                     <td>{{ $consignment->item_description }}</td>
                     <td>{{ $consignment->remarks }}</td>
                     <td>{{ $consignment->caller_remarks }}</td>
                     <td>{{ $consignment->updated_at->format('Y-M-d') }}</td>
                     <td>{{ $consignment->no_of_attempts }}</td>
                     <td align="center"><a class="btn btn-default" href="/caller-consignments/{{ $consignment->id }}/edit?date_from=2018-08-01&date_to=2018-08-01&branch=" target="_blank" ><em class="fa fa-pencil"></em></a></td>
                  </tr>
               </tbody>
               <?php //$i = $i+1; ?>
               @empty
               No Consignments.
               @endforelse
            </table>
            <!--<?php echo "RTO Found : " . $rto_count; ?>-->
         </div>
         <div class="panel-footer">
            <div class="row">
               <div class="col col-xs-4">Page {{ $consignments->currentPage() }} of {{ $consignments->lastPage()  }}
               </div>
               <div class="col col-xs-8 pull-right">
                  <ul class="pagination hidden-xs pull-right">
                     {{ $consignments->appends(request()->input())->links() }} 
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $.noConflict();  //Not to conflict with other scripts
   jQuery(document).ready(function($) {
   
   
         var getUrlParameter = function getUrlParameter(sParam) {
         var sPageURL = decodeURIComponent(window.location.search.substring(1)),
             sURLVariables = sPageURL.split('&'),
             sParameterName,
             i;
   
         for (i = 0; i < sURLVariables.length; i++) {
             sParameterName = sURLVariables[i].split('=');
   
             if (sParameterName[0] === sParam) {
                 return sParameterName[1] === undefined ? true : sParameterName[1];
             }
         }
     };
   
   if(getUrlParameter('date_from')){
   var date_from = getUrlParameter('date_from');
   
   }else{
   var date_from = new Date();
   }
   
   if(getUrlParameter('date_to')){
   var date_to = getUrlParameter('date_to');
   
   }else{
   var date_to = new Date();
   }
   
   
       $( "#datepicker_from" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_from);
   
       $( "#datepicker_to" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_to);
   
   
     } );
     
</script>

@endsection