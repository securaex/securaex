@extends('layouts.app')

@section('content')
<script>
	
function getComboA(selectObject) {
    var value = selectObject.value;
	if(value == 'Same As Above Remarks'){
		document.getElementById('caller_remarks').value = document.getElementById('remarks').value;
	}
	else {
		document.getElementById('caller_remarks').value = value;
	}	
}
	
</script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                   Caller Feedback
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/caller-consignments/{{ $consignment->id }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group{{ $errors->has('awb') ? ' has-error' : '' }}">
                            <label for="awb" class="col-md-4 control-label">AWB</label>

                            <div class="col-md-6">
                                <input id="awb" type="text" class="form-control" name="awb" value="{{ $consignment->awb }}" readonly disabled >

                                @if ($errors->has('awb'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('awb') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        <div class="form-group{{ $errors->has('branch') ? ' has-error' : '' }}">
                            <label for="branch" class="col-md-4 control-label">Branch</label>

                            <div class="col-md-6">


                                <input readonly id="branch" type="text" class="form-control" name="branch" value="{{ $consignment->branch }}" disabled >
                                @if ($errors->has('branch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('branch') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('consignee') ? ' has-error' : '' }}">
                            <label for="consignee" class="col-md-4 control-label">Consignee</label>

                            <div class="col-md-6">
                                
								<input readonly id="consignee" type="text" class="form-control" name="consignee" value="{{ $consignment->consignee }}" disabled >
                                @if ($errors->has('consignee'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('consignee') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('item_description') ? ' has-error' : '' }}">
                            <label for="item_description" class="col-md-4 control-label">Product Description</label>

                            <div class="col-md-6">
                                
								<input readonly id="item_description" type="text" class="form-control" name="item_description" value="{{ $consignment->item_description }}" disabled >
                                @if ($errors->has('item_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('item_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('consignee_address') ? ' has-error' : '' }}">
                            <label for="consignee_address" class="col-md-4 control-label">Consignee Address</label>

                            <div class="col-md-6">
                                <textarea class="form-control" rows="4" name="consignee_address" readonly disabled >{{ $consignment->consignee_address }}</textarea>

                                @if ($errors->has('consignee_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('consignee_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        

                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ $consignment->mobile }}" readonly disabled >

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
						<div class="form-group">
                            <label class="col-md-4 control-label">Attempt Date</label>

                            <div class="col-md-6">
                                <label for="mobile" class="control-label">{{ $consignment->updated_at->format('Y-M-d g:i A') }}</label>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">No. Of Attempts</label>

                            <div class="col-md-6">
                                <label for="mobile" class="control-label">{{ $consignment->no_of_attempts }}</label>
                            </div>
                        </div>
                        
                        @if($consignment->payment_mode == "COD")
							<div class="form-group">
								<label class="col-md-4 control-label">Price</label>

								<div class="col-md-6">
									<label for="mobile" class="control-label">Rs. {{ $consignment->collectable_value }}</label>
								</div>
							</div>                        
                        @endif
                        
                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            <label for="remarks" class="col-md-4 control-label">Remarks</label>

                            <div class="col-md-6">
                                <textarea id="remarks" class="form-control" rows="4" name="remarks" readonly disabled >{{ $consignment->remarks }}</textarea>

                                @if ($errors->has('remarks'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('remarks') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        

                        <div class="form-group{{ $errors->has('caller_remarks') ? ' has-error' : '' }}">
                            <label for="caller_remarks" class="col-md-4 control-label">Caller Remarks</label>
							<div class="col-md-6">

                                   <select id="remarksList" onchange="getComboA(this)" class="form-control" name="remarksList" value="{{ old('remarksList') }}" autofocus>
                                    
                                       <option value="" > --Select Remarks-- </option>
                                       <option value="Phone Switched Off" title="" >Phone Switched Off</option>
                                       <option value="Call Not Picked" title="" >Call Not Picked</option>
                                       <option value="Out Of Network Area" title="" >Out Of Network Area</option>
                                       <option value="Call Disconnected" title="" >Call Disconnected</option>
                                       <option value="Same As Above Remarks" title="" >Same As Above Remarks</option>
                                       <!--<option value="Others" title="" >Others</option>-->
                                   </select>
                                   <br>
                                   <textarea id="caller_remarks" class="form-control" rows="5" name="caller_remarks" autofocus >{{ $consignment->caller_remarks }}</textarea>
                              </div>
                           
								
                                
                            
                        </div>
							<input type="hidden" name="reviewed" value="1" >
                       		<input type="hidden" name="caller_name" value="{{ Auth::user()->username }}" >
                       		<input type="hidden" name="call_date" value="{{ \Carbon\Carbon::today()->toDateString() }}" >
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
