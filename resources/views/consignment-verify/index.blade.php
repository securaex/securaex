@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div id="page-wrapper" class="page-wrapper-cls">
    <div id="page-inner">
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-warning">
            <div class="panel panel-default">
              <div class="panel-heading"> Step 1 : HUB : In Scan Consignment with Instant Search
                <!--<div class="pull-right"> <span><a href="/consignment-verification"><strong>HUB : In Scan with Old Search</strong></a></span> </div>-->
              </div>
              <div class="panel-body"> @if(Session::has('success_message'))
                <div class="alert alert-success"><em> {!! session('success_message') !!}</em></div>
                @endif
                <form class="form-horizontal" role="form" method="POST" action="/consignment-verification">
                  {{ csrf_field() }}
                  <input type="hidden" name="action"     value="show" />
                  <div class="form-group{{ $errors->has('txtwaybill') ? ' has-error' : '' }}">
                    <label for="txtwaybill" class="col-md-4 control-label">Waybill</label>
                    <div class="col-md-4">
                      <textarea id="search_box" type="text" rows="3" cols="20"  class="form-control" name="txtwaybill" placeholder="Please Enter/Scan Waybill Numbers" required  autofocus  onkeydown="down()" onfocus="clearContents(this);"></textarea>
                      @if ($errors->has('txtwaybill')) <span class="help-block"> <strong>{{ $errors->first('txtwaybill') }}</strong> </span> @endif </div>
                    <div class="col-md-4">
                      <button type="button" class="btn btn-success">Total Count <span class="badge" id="counter">0</span></button>
                    </div>
                  </div>
                  <!--

                           <div class="form-group">

                              <div class="col-md-6 col-md-offset-4">

                                 <button type="submit" class="btn btn-primary">

                                 Submit

                                 </button>

                              </div>

                           </div>

                          -->
                </form>
              </div>
            </div>
            <div class="panel panel-default">
            @if(!isset($consignments))
              <div class="panel-heading"> Message Log </div>
            @else
              <div class="panel-heading"> In Scanned Consignments Details: </div>
            @endif  
              <div class="row">
                <div class="col-xs-8 col-xs-offset-4"> <img id="loading" src="{{URL::asset('/image/loading-dash.gif')}}" style="display:none" alt="Loading..." height="200" width="250"> </div>
              </div>
              <div class="panel-body">
              	<div id="notFound" class="alert alert-danger" style="display:none; padding:20px">Consignments Not Found: </div>
                <div id="alreadyScanned" class="alert alert-warning" style="display:none; padding:20px">Consignments Already Scanned: </div>
              
                <form role="form" method="POST" action="/consignment-verify">
                  <table  id="tbUser" class="table table-striped table-bordered table-list">
                    <thead>
                      <tr>
                        <th>AWB</th>
                        <th>Data Received Date</th>
                        <th>Customer Name</th>
                        <th>Pincode</th>
                        <th>Branch</th>
                        <th>Payment Mode</th>
                        @if(!isset($consignments))
                        <th><em class="fa fa-cog"></em></th>
                        @endif
                      </tr>
                    </thead>
                    @if(isset($consignments))
                      <tbody>
                      @foreach($consignments as $consignment)
                       <tr>
                         <td>{{$consignment->awb}}</td>
                         <td>{{$consignment->data_received_date}}</td>
                         <td>{{$consignment->customer_name}}</td>
                         <td>{{$consignment->pincode}}</td>
                         <td>{{$consignment->branch}}</td>
                         <td>{{$consignment->payment_mode}}</td>
                       </tr>
                       @endforeach  
                      </tbody>
					@endif		
                    <tbody id="search-results">
                    </tbody>
                  </table>
                  {{ csrf_field() }}
                  <div id="Btnsubmit" class="row" style="display:none">
                    <div class="col-md-11 "></div>
                    <div class="col-md-1 ">
                      <button type="submit" class="btn btn-primary" > Submit </button>
                    </div>
                  </div>
                </form>
 </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>



    var timer;
$(document).ready(function(){
    $('#search_box').on('keyup', function(e){
    var code = (e.keyCode || e.which);

    // do nothing if it's an arrow key
    if(code == 37 || code == 38 || code == 39 || code == 40 || code == 17 || code == 18 || code == 9 || code == 8 || code == 46 || code == 27) {
        return;
    }
        timer = setTimeout(function() { 

            var keywords = $('#search_box').val(); 

            if (keywords.length > 0) { 

              $("#loading").show();

              $.ajax({

                  url: 'ajax-verify-consignment',

                  method: 'GET',

                  data:{keywords: keywords}

              }).done(function(response){

                  $("#loading").hide();


                  if(response.status == 'success'){
					  //alert(response.founds);
					  $.each(response.founds, function(index, element) {
						//alert(element.awb);
						
						$('#search-results').prepend("<tr id='"+ element.id +"' > \ <td><a href='/consignments/"+ element.id +"' target='_blank' ><strong>"+ element.awb +"</strong></a></td> \ <td>"+ element.created_at +"</td> \ <td>"+ element.customer_name +"</td> \ <td>"+ element.pincode +"</td> \ <td>"+ element.branch +"</td> \ <td>"+ element.payment_mode +"</td> \ <td align='center'><button class='btnDelete'>Remove</button></td> \ <input type='hidden' value='"+ element.awb +"' name='awbs[]' /> \</tr>");
						
							$('#counter').html(function(i, val) { return +val+1 });
                            $('#Btnsubmit').show();
                            $('textarea#search_box').val("");
					});
					  
					  
					  $.each(response.notFounds, function( i, val ) {
						  //alert(val);
						  $('#notFound').show();
						  $('#notFound').append(val + ", ");
						  $('textarea#search_box').val("");
						});
					  
					  
					  $.each(response.alreadyScanneds, function( i, val ) {
						  //alert(val);
						  $('#alreadyScanned').show();
						  $('#alreadyScanned').append(val + ", ");
						  $('textarea#search_box').val("");
						});
					  
					  
					  }

              });

            }

        }, 500);

        return false;

    });

 });

    function down() {

        clearTimeout(timer);

    }



    function clearContents(element) {

      //element.value = '';

    }



    $('fa-trash').on('click', function() {

        $(this).closest('tr').remove();

    });





    $(document).ready(function(){



     $("#tbUser").on('click','.btnDelete',function(){

           $(this).closest('tr').remove();

           $('#counter').html(function(i, val) { return +val-1 });

           if($('#counter').html() < 1)                            

            $('#Btnsubmit').hide();



         });



    });

</script>
@endsection