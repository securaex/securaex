@extends('layouts.app')
@section('content')
<div class="container-fluid">

              

<div class="row"  id="search-results">
   <div class="col-md-11 col-md-offset-1">
            
      </p>
      <br />
      <div class="panel panel-default panel-table">
         <div class="panel-heading">
            <div class="row">
               <div class="col col-xs-6">
                  <h2 class="panel-title">Branch - <strong>{{ucwords(strtolower($branch))}}</strong></h2>
               </div>
               <div class="col col-xs-6 text-right">
                  <h2 class="panel-title">Total Number Of Drivers - <strong> {{$totalDrivers}}</strong></h2>
               </div>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-list">
               <thead>
                  <tr>
                     <th>Driver's Name</th>
                     <th>DRS Generated</th>
                     <th>Consignments</th>
                     <th>OFD</th>
                     <th>Delivered</th>
                     <th>Undelivered</th>
                     <th>Cancelled</th>
                  </tr>
               </thead>
               @foreach($driver_names as $key => $value)
               <tbody>
                  <tr>
                  	 <td>{{ucwords(strtolower($key))}}</td>
                     <td>{{$value['totalDrs']}}</td>
                     <td>{{$value['totalConsignments']}}</td>
                     <td>{{$value['totalOfd']}}</td>
                     <td>{{$value['totalDelivered']}}</td>
                     <td>{{$value['totalUndelivered']}}</td>
                     <td>{{$value['totalCancelled']}}</td>
                  </tr>
               </tbody>
               @endforeach
            </table>
            <div class="col-md-5 col-md-offset-7 text-right">
                  <a href="{{URL::previous()}}" title="Go To Previous Page">Back To Report Page</a>
               </div>

         </div>
         <div class="panel-footer">

            
         </div>
      </div>
   </div>
</div>
</div>

@endsection