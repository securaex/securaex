@extends('layouts.app')
@section('content')
<div class="container-fluid">

              

<div class="row"  id="search-results">
   <div class="col-md-11 col-md-offset-1">
      <div class="row">  
      <form method="GET" action="/branchwise-report" >

<div class="row">
        <div class="col-md-4">

         <div class="form-group">
            <label for="datepicker_from" class="col-md-6 control-label">Updated From Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_from" id="datepicker_from" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        
        <div class="col-md-4"> 
         <div class="form-group">
            <label for="datepicker_to" class="col-md-6 control-label">Updated To Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="date_to" id="datepicker_to" placeholder="yyyy-mm-dd">
            </div>
         </div>
        </div>
        
        <div class="col-md-4">
         <div class="form-group">
          <div class="col-md-4">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
          </div>
         </div>
        </div>

</div>
      </form>
      </div>      
      </p>
      <br />
      <div class="panel panel-default panel-table">
         <div class="panel-heading">
            <div class="row">
               <div class="col col-xs-6">
                  <h2 class="panel-title">Branchwise Report</h2>
               </div>
               <div class="col col-xs-6 text-right">
                  <h2 class="panel-title">Total Number Of Shipments Recieved - <strong>{{$totalShpmtRcvd + $osTotalShpmtRcvd}}</strong></h2>
               </div>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-list">
               <thead>
                  <tr>
                     <th>Branch</th>
                     <th>Shipment Received/OS Shipment Received</th>
                     <th>OFD/OS-OFD/Total OFD</th>
                     <th>Delivered/OS-Delivered/Total Delivered</th>
                     <th>Undelivered/OS-Undelivered/Total Undelivered</th>
                     <th>Cancelled/OS-Cancelled/Total Cancelled</th>
                     <th>Reattempt/OS-Reattempt</th>
                     <th>RTO/OS-RTO</th>
                     <th><em class="fa fa-cog"></em></th>
                  </tr>
               </thead>
               @foreach($branches as $branch)
               <tbody>
                  <tr>
                     <td>{{ ucfirst($branch->username) }}</td>
                      @foreach($branchStatus as $key => $value)
                          @if($branch->username == $key)
                             <td>{{$value['Shipment Received']}}/{{$value['OS Shipment Received']}}</td>
                             <td>{{$value['OFD']}}/{{ $value['OS OFD']}}/{{ $value['Total OFD']}}</td>
                             <td>{{$value['Delivered']}}/{{$value['OS Delivered']}}/{{$value['Total Delivered']}}</td>
                             <td>{{$value['Undelivered']}}/{{$value['OS Undelivered']}}/{{$value['Total Undelivered']}}</td>
                             <td>{{$value['Cancelled']}}/{{$value['OS Cancelled']}}/{{$value['Total Cancelled']}}</td>
                             <td>{{$value['Reattempt']}}/{{$value['OS Reattempt']}}</td>
                             <td>{{$value['Rto']}}/{{$value['OS Rto']}}</td>
                             <td><a href="/branchwise-report-details?branch={{$branch->username}}&date_from={{$date_from}}&date_to={{$date_to}}">more details..</a></td>
                          @endif
                     @endforeach
                  </tr>
               </tbody>
               @endforeach
            </table>
         </div>
         <div class="panel-footer">
            
         </div>
      </div>
   </div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $.noConflict();  //Not to conflict with other scripts
   jQuery(document).ready(function($) {
   
   
         var getUrlParameter = function getUrlParameter(sParam) {
         var sPageURL = decodeURIComponent(window.location.search.substring(1)),
             sURLVariables = sPageURL.split('&'),
             sParameterName,
             i;
   
         for (i = 0; i < sURLVariables.length; i++) {
             sParameterName = sURLVariables[i].split('=');
   
             if (sParameterName[0] === sParam) {
                 return sParameterName[1] === undefined ? true : sParameterName[1];
             }
         }
     };
   
   if(getUrlParameter('date_from')){
   var date_from = getUrlParameter('date_from');
   
   }else{
   var date_from = new Date();
   }
   
   if(getUrlParameter('date_to')){
   var date_to = getUrlParameter('date_to');
   
   }else{
   var date_to = new Date();
   }
   
   
       $( "#datepicker_from" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_from);
   
       $( "#datepicker_to" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           defaultDate: new Date(),
       }).datepicker("setDate", date_to);
   
   
     } );
     
</script>

@endsection