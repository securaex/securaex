@extends('layouts.app')



@section('content')
<div class="container-fluid">
  <div id="page-wrapper" class="page-wrapper-cls">
    <div id="page-inner">
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-warning">
            <div class="panel panel-default">
              <div class="panel-heading"> Step 2 : HUB : Create Bagging </div>
              <div class="panel-body">
              @if(Session::has('success_message'))
              <div class="alert alert-success"><em> {!! session('success_message') !!}</em></div>
              @endif
              @if(Session::has('error_message'))
              <div class="alert alert-danger"><em> {!! session('error_message') !!}</em></div>
              @endif
              <form class="form-horizontal" role="form" method="POST" action="/bagging">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('to_branch') ? ' has-error' : '' }}">
                            <label for="to_branch" class="col-md-4 control-label">To Branch</label>

                            <div class="col-md-5">
                                <select id="to_branch" class="form-control" name="to_branch" value="{{ old('to_branch') }}" required autofocus >
                                    <option value="">None</option>

                                    @foreach($branches as $branch)
                                    <option value="{{ ucwords($branch->username) }}">{{ ucwords($branch->username) }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('to_branch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('to_branch') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div id="newData"></div>
                        
                        <div class="form-group{{ $errors->has('seal_number') ? ' has-error' : '' }}">
              <label for="seal_number" class="col-md-4 control-label">Seal Number</label>
              <div class="col-md-5">
              
                <input id="seal_number" type="text" class="form-control" name="seal_number" placeholder="Please Enter Seal Number" required >
               
                @if ($errors->has('seal_number')) <span class="help-block"> <strong>{{ $errors->first('seal_number') }}</strong> </span> @endif </div>
</div>
                        
                <div class="form-group">
                  <label class="col-md-4 control-label" for="">Enter Air WayBill Numbers</label>
                  <div class="col-md-5">
                  	<textarea id="search_box" name="awbs" rows="2" cols="20" autofocus  onkeydown="down()" onfocus="clearContents(this);"  class="form-control" style="height:100px;" placeholder="<?php if(Auth::user()->username === 'Sunil_KR' || Auth::user()->username === 'aman-hub' || Auth::user()->username === 'pankaj-hub' || Auth::user()->username === 'ved_prakash' || Auth::user()->username === 'mahender-hub') echo 'You can enter Multiple AWB no one after other on each line'; else echo 'Enter an Air WayBill Number'; ?>" ></textarea>
                  </div>
                  <div class="col-md-3">
                    <button type="button" class="btn btn-success">Total Count <span class="badge" id="counter">0</span></button>
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-md-4 control-label" for="exampleInputEmail1"></label>
                  <div class="col-md-6">
                    <label class="checkbox-inline">
                      <input id="override" type="checkbox" name="override" value="1" onchange=" getMessage(this) ">
                      Override Branch & Bag Mismatch</label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-4"></div>
                  <div class="col-md-6">
                    <input type="submit" name="submit" value="Submit" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnsbmit&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btnsbmit" class="btn btn-info">
                    <input type="hidden" id="awb" name="awbsSingle" value="">
                  </div>
                </div>
                </div>
              </form>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div style="margin-left:20px;float:left;"> </div>
              </div>
            </div>
            <div class="panel panel-default"> @if(!isset($consignments))
              <div class="panel-heading"> Message Log </div>
              @else
              <div class="panel-heading"> In Scanned Consignments Details: </div>
              @endif
              <div class="row">
                <div class="col-xs-8 col-xs-offset-4"> <img id="loading" src="{{URL::asset('/image/loading-dash.gif')}}" style="display:none" alt="Loading..." height="200" width="250"> </div>
              </div>
              <div class="panel-body">
                <!--<form role="form" method="POST" action="/consignment-verify">-->
                  <table  id="tbUser" class="table table-striped table-bordered table-list">
                    <thead>
                      <tr>
                        <th>AWB</th>
                        <th>Data Received Date</th>
                        <th>Customer Name</th>
                        <th>Pincode</th>
                        <th>Branch</th>
                        <th>Payment Mode</th>
                      </tr>
                    </thead>
                    <tbody id="search-results">
                    </tbody>
                  </table>
                  {{ csrf_field() }}
                <!--</form>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
	var timer;
	$(document).ready(function(){
		var awbs = [];
		$('#search_box').on('keyup', function(e){
		var code = (e.keyCode || e.which);
		// do nothing if it's an arrow key
		if(code == 37 || code == 38 || code == 39 || code == 40 || code == 17 || code == 18 || code == 9 || code == 8 || code == 46 || code == 27) {
			return;
		}
		
				<?php if(Auth::user()->username === 'Sunil_KR' || Auth::user()->username === 'aman-hub' || Auth::user()->username === 'pankaj-hub' || Auth::user()->username === 'ved_prakash' || Auth::user()->username === 'mahender-hub') { ?>
			timer = setTimeout(function() { 
				var keywords = $('#search_box').val().split(/\n/);
				
				for (var i=0; i < keywords.length; i++) {
				if (keywords[i].length > 0) { 
					var awbz = $.trim(keywords[i]);
				  $("#loading").show();
				  $.ajax({
					  url: '/ajax-show-bagging',
					  method: 'GET',
					  data:{keywords: awbz}
				  }).done(function(response){
					  $("#loading").hide();
	//alert(response.status + i);
					  if(response.status == 'success'){
						if($("#" + response.data.id).length == 0) {
							//it doesn't exist
							$('#search-results').prepend("<tr id='"+ response.data.id +"' > \ <td><a href='/consignments/"+ response.data.id +"' target='_blank' ><strong>"+ response.data.awb +"</strong></a></td> \ <td>"+ response.data.data_received_date +"</td> \ <td>"+ response.data.customer_name +"</td> \ <td>"+ response.data.pincode +"</td> \ <td>"+ response.data.branch +"</td> \ <td>"+ response.data.payment_mode +"</td> \ </tr>");
								$('#counter').html(function(i, val) { return +val+1 });
						}else{
						  alert('This AWB Already added in the list !')
						}
					  }else if(response.status == 'error'){
						alert(response.message);
					  }
				  });
				}
			}
			}, 500);
				<?php }
					else
					{
				?>
			timer = setTimeout(function() { 
				var keywords = $('#search_box').val();
				
				if (keywords.length > 0) { 
					var awbz = $.trim(keywords);
				  $("#loading").show();
				  $.ajax({
					  url: '/ajax-show-bagging',
					  method: 'GET',
					  data:{keywords: awbz}
				  }).done(function(response){
					  $("#loading").hide();
	//alert(response.status + i);
					  if(response.status == 'success'){
						if($("#" + response.data.id).length == 0) {
							//it doesn't exist
							$('#search-results').prepend("<tr id='"+ response.data.id +"' > \ <td><a href='/consignments/"+ response.data.id +"' target='_blank' ><strong>"+ response.data.awb +"</strong></a></td> \ <td>"+ response.data.data_received_date +"</td> \ <td>"+ response.data.customer_name +"</td> \ <td>"+ response.data.pincode +"</td> \ <td>"+ response.data.branch +"</td> \ <td>"+ response.data.payment_mode +"</td> \ </tr>");
								$('#counter').html(function(i, val) { return +val+1 });
								$('#search_box').val('');
								awbs.push(response.data.awb);
								$('#awb').val(JSON.stringify(awbs));
								
						}else{
						  alert('This AWB Already added in the list !');
						  $('#search_box').val('');
						}
					  }else if(response.status == 'error'){
						alert(response.message);
						$('#search_box').val('');
					  }
				  });
				}
			}, 500);
			
			<?php
			
					}
			?>
			return false;
		}); 
	$('#to_branch').change(function(){
	var value = $('#to_branch').val();
		if(value != ''){
			$.ajax({
				url: '/ajax-get-bag',
				method: 'GET',
				data: {branch: value }
			}).done(function(response){
						  $('#newData').html(response);          
			});
	}else{
		$('#bag_code' ).prop( "disabled", true );
		$('#bagCodeDiv').hide();
	}
		  });		
	 });
	
	function down() {
	  clearTimeout(timer);
	}
	
	function clearContents(element) {
		  //element.value = '';
	}
	
	function getMessage(selectObject) {
	   if(selectObject.checked)
	   {
		   var retVal = confirm("WARNING !! This will override consignment & bag branch mismatch by changing the default consignment branch to this bag's branch.");
			 if (retVal == true)
			   {
				  return true;
			   } 
			 else
			   {
				  document.getElementById("override").checked = false;
				  return false;
			   }
	   }
	}
	

</script>
@endsection 