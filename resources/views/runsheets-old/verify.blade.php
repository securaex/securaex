@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                   
                    Verify Bagging 

                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            Bag Code : {{ $bag->bag_code }}
                        </div>

                        <div class="col-md-8 col-md-offset-2">
                            For Branch  : {{ $bag->to_branch }}
                        </div>
                    </div>
                    Consignments
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                        @foreach($bag->bag_consignments as $consignment)                        
                                {{ $consignment->awb }}
                        @endforeach
                        </div>
                    </div>
<hr>
                     <form class="form-horizontal"  method="POST" action="/bagging/updateBagVerification/{{ $bag->id }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}


                        <div class="form-group{{ $errors->has('verify') ? ' has-error' : '' }}">
                            <label for="verify" class="col-md-4 control-label">Bag Verification Status</label>

                            <div class="col-md-4">
                                <select id="verify" class="form-control" name="verify"  required autofocus>
                                    <option value="">Choose Bag Verification</option>
                                    <option value="0" @if(!$bag->is_verified) selected  @endif  >Unverified</option>
                                    <option value="1" @if($bag->is_verified) selected  @endif  >Verified</option>
                                </select>
                                @if ($errors->has('verify'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('verify') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>

                    </form>




                </div>



            </div>
        </div>
    </div>
</div>
@endsection
