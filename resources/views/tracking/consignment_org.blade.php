@extends('layouts.app')

@section('content')
<div class="container-fluid">
<div id="page-wrapper" class="page-wrapper-cls">
    <div id="page-inner">

        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Shipment Tracking
                        </div>
                        <div class="panel-body">

                     @if(Session::has('success_message'))
                         <div class="alert alert-success"><em> {!! session('success_message') !!}</em></div>
                     @endif

                     @if(Session::has('error_message'))
                         <div class="alert alert-danger"><em> {!! session('error_message') !!}</em></div>
                     @endif

                           <form class="form-horizontal" role="form" method="POST" >
                           {{ csrf_field() }}

                            <div class="form-group">
                              <!--
                                <label for="exampleInputEmail1" style="color:red;">
                                    <h4>Note:-</h4> It will display 50 records only. Please Click On Download Button for downloading complete data.
                                    <br> [For Downloading Large Data, You can direct click on Download Button After Selecting Search Options ]

                                </label>
                              -->
                                <br>
                                <br>
                                <label class="col-md-4 control-label" for="exampleInputEmail1">Select Tracking Option
                                    <br>
                                </label>
                                <div class="col-md-6">
                                  <select name="trackingtype" id="ctl00_ContentPlaceHolder1_ddltype" class="form-control" style="width:300px;">
                                      <option @if(isset($_POST['trackingtype']) && $_POST['trackingtype'] == '0') {{ 'selected' }} @endif value="0" style="color: #000000;">Air Waybill No</option>
                                      <option @if(isset($_POST['trackingtype']) && $_POST['trackingtype'] == '1') {{ 'selected' }} @endif  value="1" style="color: #000000;">Order No</option>
                                      <option @if(isset($_POST['trackingtype']) && $_POST['trackingtype'] == '2') {{ 'selected' }} @endif value="2" style="color: #000000;">Mobile No</option>

                                  </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="exampleInputEmail1">Enter Selected Tracking Option </label>
                                <div class="col-md-6">
                                <textarea name="trackingdata" rows="2" cols="20" id="ctl00_ContentPlaceHolder1_txtwaybillororderno" class="form-control" style="height:100px;width:300px;">@if(isset($_POST['trackingdata'])) {{ trim($_POST['trackingdata']) }} @endif</textarea>
                                </div>
                                <br>
                                <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator4" style="color:Red;visibility:hidden;">Enter Selected Tracking Option</span>
                            </div>
                            <div class="form-group">
                              <div class="col-md-6 col-md-offset-4">
                                <input type="submit" formaction="consignment" name="" value="Search Shipment"  id="" class="btn btn-info"> &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="submit" formaction="exportExcel" name="" value="Download Shipment" id="" class="btn btn-info">
                              </div>
                            </div>
                         </form>
                        </div>
                    </div>
  
                    <div>




@if(isset($consignments))
                        <table class="table table-bordered" cellspacing="0" border="0" id="ctl00_ContentPlaceHolder1_GridView2" style="width:98%;border-collapse:collapse;">
                            <tbody>
                                <tr>
                                    <th scope="col">S No</th>
                                    <th scope="col">AWB</th>


                                    <th scope="col">Upload Date</th>
                                    <th scope="col">Customer Name</th>
                                    <th scope="col">Consignee Name</th>
                                    <th scope="col">City</th>
                                    <th scope="col">Updated By</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Last Updated On</th>
                                </tr>


  <?php $count = 1; ?>
   @forelse($consignments as $consignment)

                                   <tr>
                                       <td style="width:30px;">
                                           {{ $count++ }}
                                       </td>
                                       <td style="width:0px;">
                                           <a id="ctl00_ContentPlaceHolder1_GridView2_ctl02_hpwaybill" target="_blank" href="/consignments/{{ $consignment->id }}" style="color:Black;"><strong>{{ $consignment->awb }}</strong></a>
                                       </td>

                                       <td style="width:0px;">
                                           <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblcause4">{{ $consignment->created_at }}</span>
                                       </td>

                                       <td style="width:0px;">
                                           <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblcust">{{ $consignment->customer_name }}</span>
                                       </td>

                                       <td style="width:0px;">
                                           <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblvname">{{ $consignment->consignee }}</span>
                                       </td>
                                       <td style="width:0px;">
                                           <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblshipmentdate">{{ $consignment->destination_city }}</span>
                                       </td>
                                       <td style="width:0px;">
                                           <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblupdatedby">{{ $consignment->last_updated_by }}</span>
                                       </td>

                                       <td style="width:0px;">
                                           <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblshipmentdate">{{ $consignment->current_status }}</span>
                                       </td>
                                       <td style="width:0px;">
                                           <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_lblupdatedate">{{ $consignment->updated_at }}</span>
                                       </td>
                                   </tr>

   @empty

   No records

   @endforelse




                            </tbody>
                        </table>
@endif                        
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
</div>

@endsection
