@extends('layouts.app')

@section('content')
<div class="container-fluid">
   <div id="page-wrapper" class="page-wrapper-cls">
      <div id="page-inner">
         <div class="row">
            <div class="col-md-12">
               <div class="alert alert-warning">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Step 1 : Branch : Final Consignment Status
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/status">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="show" />
                          
                           <div class="form-group">
                              <label for="exampleInputEmail1" class="col-md-4 control-label">Status</label> 
                            <div class="col-md-6">

                                   <select id="status" onchange='document.getElementById("bagIdValue").value = this.options[this.selectedIndex].title;'  class="form-control" name="status" value="{{ old('status') }}" required autofocus>
                                    
                                    <option value="" >Please Select Consignment Status</option>

                                       <option value="Delivered" title="" >Delivered</option>
                                       <option value="Cancelled" title="" >Cancelled</option>
                                       <option value="Undelivered" title="" >Undelivered</option>
                                       <option value="Other Reason" title="" >Other Reason</option>
                                   
                                   </select>
                                   <input id="bagIdValue" type="hidden" name="drs_id" />
                              </div>
                           </div>

                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            <label for="remarks" class="col-md-4 control-label">Remarks</label>

                            <div class="col-md-6">
                                <textarea id="remarks" class="form-control" name="remarks" placeholder="optional"  autofocus>{{ old('remarks') }}</textarea>

                                @if ($errors->has('remarks'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('remarks') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                           <div class="form-group">
                              <label for="exampleInputEmail1" class="col-md-4 control-label">Waybill</label> 
                              <div class="col-md-6">
                              <textarea name="awbs" rows="2" cols="20" id="ctl00_ContentPlaceHolder1_txtwaybill" class="form-control" placeholder="Scan waybill and press submit button." style="height:150px;" required></textarea><br>
                              <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator1" style="color:Red;visibility:hidden;">Enter Waybill</span>
                              </div>
                           </div>




                           <div class="form-group">
                            <div class="col-md-6 pull-right">
                              <input type="submit" name="ctl00$ContentPlaceHolder1$btnsbmit" value="Submit" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnsbmit&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btnsbmit" class="btn btn-info">
                            </div>
                           </div>

                        </form>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Message Log
                     </div>
                     <div class="panel-body">
                        @if(isset($consignment)) 
                        Consignment Found for Waybill no: {{ $consignment->awb }} <br />
                        @foreach($consignment->consignment_updates as $update)
                        <?php
                           if($update->current_status == \Config::get('constants.inScanbranch'))
                             $found = 1;
                           ?>
                        @endforeach
                        @if(!isset($found)) 
                        <form class="form-horizontal" role="form" method="POST" action="/in-scan">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="verify" />
                           <input type="hidden" name="txtwaybill" value="{{ $consignment->awb }}" />
                           <div class="col col-xs-12 text-left">
                              <button type="submit" class="btn btn-sm btn-success btn-create">Verify In Scan</button>
                           </div>
                        </form>
                        @else
                        Already Verified !
                        @endif
                        @elseif(isset($awb))
                        Consignment was not found for Waybill no : {{ $awb }} 
                        @endif
                        <div class="form-group">
                           <span id="ctl00_ContentPlaceHolder1_lblmsg" style="color:Red;"></span>
                        </div>
                     </div>
                  </div>
                  <!--
                     <div class="panel panel-default">
                                        <div class="panel-heading">
                                          RTO  Consignments Verified Today
                                        </div>
                                        <div class="panel-body">
                                       
                     <div class="form-group">
                      <div>
                     
                     </div>
                     </div>
                     
                     -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>

@endsection
