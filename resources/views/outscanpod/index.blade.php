@extends('layouts.app')

@section('content')
<div class="container-fluid">
   <div id="page-wrapper" class="page-wrapper-cls">
      <div id="page-inner">
         <div class="row">
            <div class="col-md-12">
               <div class="alert alert-warning">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Step 1 : RTO : Out Scan /  Back to Customer
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/out-scan-pod">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="show" />
                          
                           <div class="form-group">
                              <label for="exampleInputEmail1" class="col-md-4 control-label">Pod Code</label> 
                                  <div class="col-md-6">
                                   <select id="pod_code" onchange='document.getElementById("bagIdValue").value = this.options[this.selectedIndex].title;'  class="form-control" name="pod_code" value="{{ old('pod_code') }}" required autofocus>
                                    
                                    <option value="" >Please Select Pod code</option>

                                    @foreach($pods as $pod):
                                       
                                       <option value="{{ $pod->pod_code }}" title="{{ $pod->id }}" >{{ $pod->pod_code }} - {{ $pod->delivery_boy }}- {{ $pod->delivery_date }}</option>
                                   @endforeach
                                   
                                   </select>
                                 </div>
                                   <input id="bagIdValue" type="hidden" name="pod_id" />

                           </div>


                           <div class="form-group">
                              <label for="exampleInputEmail1" class="col-md-4 control-label">Waybill</label> 
                              <div class="col-md-6">
                              <textarea name="awbs" rows="2" cols="20" id="ctl00_ContentPlaceHolder1_txtwaybill" class="form-control" placeholder="You can enter Multiple AWB numbers one after other on each line" style="height:150px;"></textarea><br>
                              </div>
                              <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator1" style="color:Red;visibility:hidden;">Enter Waybill</span>
                           </div>




                           <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-6">
                              <input type="submit" name="ctl00$ContentPlaceHolder1$btnsbmit" value="Submit" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnsbmit&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btnsbmit" class="btn btn-info">
                            </div>
                           </div>
                        </form>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Message Log
                     </div>
                     <div class="panel-body">

@if(Session::has('success_message'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Heads Up!</strong> {!!Session::get('success_message')!!}
        </div>
    @endif

@if(Session::has('error_message'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Heads Up!</strong> {!!Session::get('error_message')!!}
        </div>
    @endif
    

                        @if(isset($consignment)) 
                        Consignment Found for Waybill no: {{ $consignment->awb }} <br />
                        @foreach($consignment->consignment_updates as $update)
                        <?php
                           if($update->current_status == \Config::get('constants.inScanbranch'))
                             $found = 1;
                           ?>
                        @endforeach
                        @if(!isset($found)) 
                        <form class="form-horizontal" role="form" method="POST" action="/in-scan">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="verify" />
                           <input type="hidden" name="txtwaybill" value="{{ $consignment->awb }}" />
                           <div class="col col-xs-12 text-left">
                              <button type="submit" class="btn btn-sm btn-success btn-create">Verify In Scan</button>
                           </div>
                        </form>
                        @else
                        Already Verified !
                        @endif
                        @elseif(isset($awb))
                        Consignment was not found for Waybill no : {{ $awb }} 
                        @endif
                        <div class="form-group">
                           <span id="ctl00_ContentPlaceHolder1_lblmsg" style="color:Red;"></span>
                        </div>
                     </div>
                  </div>
                  <!--
                     <div class="panel panel-default">
                                        <div class="panel-heading">
                                          RTO  Consignments Verified Today
                                        </div>
                                        <div class="panel-body">
                                       
                     <div class="form-group">
                      <div>
                     
                     </div>
                     </div>
                     
                     -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>

@endsection
