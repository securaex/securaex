
        <div class="col-md-11 col-md-offset-1">

            <h4>Toatal Records Found : {{ $consignments->total() }} </h4>

            <div class="panel panel-default panel-table">
              <div class="panel-heading">
                <div class="row">
                  <div class="col col-xs-6">
                    <h3 class="panel-title">Consignments</h3>
                  </div>
                  <div class="col col-xs-6 text-right">
                    <a href="/consignments/create"><button type="button" class="btn btn-sm btn-primary btn-create">Add New</button></a>
                  </div>
                </div>
              </div>
              <div class="panel-body">
                

                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">ID</th>
                            <th>Data Received Date</th>
                            <th>AWB</th>
                            <th>Order ID</th>
                            <th>Customer Name</th>  
                            <th>Last Updated BY</th>     
                            <th>Last Updated AT</th>
                            <th>Current Status</th>                                                                        
                        </tr> 
                      </thead>

                @forelse($consignments as $consignment)

                      <tbody>  
                              <tr>
                                <td align="center">
                                  <a class="btn btn-default" href="/consignments/{{ $consignment->id }}" ><em class="fa fa-eye"></em></a>                                  
                                  <a class="btn btn-default" href="/consignments/{{ $consignment->id }}/edit" ><em class="fa fa-pencil"></em></a>
                                  <form action="/consignments/{{ $consignment->id }}" method="POST" class="pull-right" onsubmit="return confirm('Are you sure you want to delete?')" >{{ csrf_field() }} {{ method_field('DELETE') }}<button type="submit" class="btn btn-danger"><em class="fa fa-trash" ></em></button></form>
                                </td>
                                <td class="hidden-xs">{{ $consignment->id }}</td>
                                <td>{{ $consignment->data_received_date }}</td>
                                <td>{{ $consignment->awb }}</td>
                                <td>{{ $consignment->order_id }}</td>
                                <td>{{ $consignment->customer_name }}</td>  
                                <td>{{ $consignment->last_updated_by }}</td>                            
                                <td>{{ $consignment->updated_at }}</td>                            
                                <td>{{ $consignment->current_status }}</td>                            
                              </tr>

                      </tbody>
                @empty
                    No Consignments.

                @endforelse

                    </table>

              
              </div>
              <div class="panel-footer">

                <div class="row">
                  
                  <div class="col col-xs-4">Page {{ $consignments->currentPage() }} of {{ $consignments->lastPage()  }}
                  </div>
                  <div class="col col-xs-8 pull-right">
                    <ul class="pagination hidden-xs pull-right">
                        {{ $consignments->links() }}
                    </ul>

                  </div>
                </div>
              </div>
            </div>