<div id="bagCodeDiv" class="form-group{{ $errors->has('vehicle_number') ? ' has-error' : '' }}">
              <label for="bag_code" class="col-md-4 control-label">Bag Code</label>
              <div class="col-md-5">
              
                <input id="bag_code" type="text" class="form-control" name="bag_code" value="{{ $bag_code }}" readonly >
               
                @if ($errors->has('bag_code')) <span class="help-block"> <strong>{{ $errors->first('bag_code') }}</strong> </span> @endif </div>
</div>
            
