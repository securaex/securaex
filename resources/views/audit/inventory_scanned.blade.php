@extends('layouts.app')



@section('content')
<div class="container-fluid">

<div class="row"  id="search-results">

  <div class="col-md-11 col-md-offset-1">
    <h4></h4>
    <div class="panel panel-default panel-table">
      <div class="panel-heading">
        <div class="row">
          <div class="col col-xs-6">
            <h3 class="panel-title">Inventory Scanned</h3>
          </div>
          <div class="col col-xs-6 text-right"> <a href="/drivers/create">
            <!-- <button type="button" class="btn btn-sm btn-primary btn-create">Add New Driver</button> -->
            </a> </div>
        </div>
      </div>
      <div class="panel-body">
	    
		<br><br>
		<?php 
          if(!empty($from_date) && !empty($to_date))
		  {
			 echo "Result Showing for between $from_date and $to_date"; 
		  }
		  else
		  {
		    echo "Records showing for today";
		  }
		?>
        <table style="color:black" class="table table-striped table-bordered table-list">
          <thead>
            <tr>
			  <th>S.No</th>
              <th>AWB</th>
			  <th>Customer Name</th>
              <th>Branch</th>
              <th>Last Updated By</th>
			  <th>Current Status</th>
			  <th>Remarks</th>
              <th>Payment Mode</th>
			  <th>Collectable Value</th>
			  <th>Updated At</th>
			  <th>Last Scanned Time</th>
			  <th>Scanning Status</th>
			  
            </tr>
          </thead>
          <tbody>
	      <?php
		    if(!empty($scanned))
			{
				$i=0;
				foreach($scanned as $row)
				{
					?>
					   <tr>
					     <td><?php echo $i++; ?></td>
						 <td>{{ $row->awb }}</td>
						 <td>{{ $row->customer_name }}</td>
						 
						 <td>{{ $row->branch }}</td>
						 <td>{{ $row->last_updated_by }}</td>
						 <td>{{ $row->current_status }} </td>
						 <td>{{ $row->remarks }}</td>
						 
						 <td>{{ $row->payment_mode }} </td>
						 <td>{{ $row->collectable_value }} </td>
						 <td>{{ $row->last_updated_at }} </td>
						 <td>{{ $row->created_at }} </td>
						 <td>{{ $row->scanning_status }}</td>
						 
					   </tr>
					<?php
				}
			}
		  ?>  
	     </tbody>
        </table>
      </div>
      <div class="panel-footer">
        <div class="row">
          <div class="col col-xs-4"></div>
          <div class="col col-xs-8 pull-right">
            <ul class="pagination hidden-xs pull-right">
              
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

$.noConflict();  //Not to conflict with other scripts

jQuery(document).ready(function($) {





      var getUrlParameter = function getUrlParameter(sParam) {

      var sPageURL = decodeURIComponent(window.location.search.substring(1)),

          sURLVariables = sPageURL.split('&'),

          sParameterName,

          i;



      for (i = 0; i < sURLVariables.length; i++) {

          sParameterName = sURLVariables[i].split('=');



          if (sParameterName[0] === sParam) {

              return sParameterName[1] === undefined ? true : sParameterName[1];

          }

      }

  };



if(getUrlParameter('date')){

var date = getUrlParameter('date');



}else{

var date = new Date();

}





    $( "#datepicker" ).datepicker({

        changeMonth: true,

        changeYear: true,

        dateFormat : 'yy-mm-dd',

        defaultDate: new Date(),

    }).datepicker("setDate", date);

  } );

  </script>
@endsection 