@extends('layouts.app')



@section('content')
<div class="container-fluid">

<div class="row"  id="search-results">

  <div class="col-md-11 col-md-offset-1">
    <h4></h4>
    <div class="panel panel-default panel-table">
      <div class="panel-heading">
	  
	    <div class="row">
		   <div class="col col-md-12 col-xs-6">
		   
		   <form method="GET" action="" >

<div class="row">
        <div class="col-md-4">

         <div class="form-group">
            <label for="datepicker_from" class="col-md-6 control-label">From Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="from_date" id="datepicker_from" placeholder="yyyy-mm-dd" value="<?php echo (!empty($from_date) ? $from_date : ''); ?>">
            </div>
         </div>
        </div>
        
        <div class="col-md-4"> 
         <div class="form-group">
            <label for="datepicker_to" class="col-md-6 control-label">To Date:</label>
            <div class="col-md-6">
               <input class="form-control" type="text" name="to_date" id="datepicker_to" placeholder="yyyy-mm-dd" value="<?php echo (!empty($to_date) ? $to_date : '') ?>">
            </div>
         </div>
        </div>
		
		<div class="col-md-4">
         <div class="form-group">
          <div class="col-md-4">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
          </div>
         </div>
        </div>
       

</div>


      </form>
		   
		   
		   
		   
		      
		   </div>
		</div>
	  <br>
        <div class="row">
          <div class="col col-xs-6">
            <h3 class="panel-title">Audit Report</h3>
			<br>
			<h3 class="panel-title"><?php if(!empty($message)) echo $message; ?></h3>
          </div>
          <div class="col col-xs-6 text-right"> <a href="/drivers/create">
            <!-- <button type="button" class="btn btn-sm btn-primary btn-create">Add New Driver</button> -->
            </a> </div>
        </div>
      </div>
      <div class="panel-body">
	    AWBs to be scanned - {{ $counts['awb_to_be_scanned'] }}  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    Scanned - {{ $counts['awb_scanned'] }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    Left to be Scan - {{ $counts['left_to_be_scan']}}
		<br><br>
        <table style="color:black" class="table table-striped table-bordered table-list">
          <thead>
            <tr>
              <th>Branch</th>
              <th>AWBs to be scanned</th>
              <th>Scanned</th>
              <th>Not Scanned</th>
            </tr>
          </thead>
          <tbody>
	      <?php
		    foreach($data_array as $key => $value)
			{
				?>
				   <tr>
				     <td><?php echo $key; ?></td>
					 <td>{{ $value['awb_to_be_scanned'] }}</td>
					 <td>
					    <?php
						
						  if($value['awb_scanned'] == 0)
						  {
							echo "0";  
						  }
						  else if($value['awb_scanned'] < 0)
						  {
							echo $value['awb_scanned'];  
						  }
						  else
						  {
						  ?>
						    <a style="color:blue" href="<?php echo 'inventory_scanned?branch='.$key .((!empty($from_date) && !empty($to_date)) ? '&from_date='.$from_date.'&to_date='.$to_date : ''); ?>">{{ $value['awb_scanned'] }}</a>
						  <?php
						  }
						
						?> 
					 
					 </td>
					 
					 <td>
					 	
						<?php
						
						  if($value['awb_to_be_scanned'] - $value['awb_scanned'] == 0 )
						  {
							echo "0";  
						  }
						  else if($value['awb_to_be_scanned'] - $value['awb_scanned'] < 0)
						  {
							echo $value['awb_to_be_scanned'] - $value['awb_scanned'];  
						  }
						  else
						  {
						  ?>
						    <a style="color:blue" href="<?php echo 'inventory_not_scanned?branch='.$key.((!empty($from_date) && !empty($to_date)) ? '&from_date='.$from_date.'&to_date='.$to_date : ''); ?>">{{ $value['awb_to_be_scanned'] - $value['awb_scanned'] }}</a>
						  <?php
						  }
						
						?>
					</td>
					 
				   </tr>
				<?php
			}
		  ?>  
	     </tbody>
        </table>
      </div>
      <div class="panel-footer">
        <div class="row">
          <div class="col col-xs-4"></div>
          <div class="col col-xs-8 pull-right">
            <ul class="pagination hidden-xs pull-right">
              
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>

$.noConflict();  //Not to conflict with other scripts

jQuery(document).ready(function($) {





      var getUrlParameter = function getUrlParameter(sParam) {

      var sPageURL = decodeURIComponent(window.location.search.substring(1)),

          sURLVariables = sPageURL.split('&'),

          sParameterName,

          i;



      for (i = 0; i < sURLVariables.length; i++) {

          sParameterName = sURLVariables[i].split('=');



          if (sParameterName[0] === sParam) {

              return sParameterName[1] === undefined ? true : sParameterName[1];

          }

      }

  };



if(getUrlParameter('date')){

var date = getUrlParameter('date');



}else{

var date = new Date();

}



    
   $( "#datepicker_from" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           //defaultDate: new Date(),
       });
   
       $( "#datepicker_to" ).datepicker({
           changeMonth: true,
           changeYear: true,
           dateFormat : 'yy-mm-dd',
           //defaultDate: new Date(),
       });
  
  
} );
  
  


  </script>
@endsection 