@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                   
                    Edit Bagging

                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            Bag Code : {{ $bag->bag_code }}
                        </div>

                        <div class="col-md-8 col-md-offset-2">
                            For Branch  : {{ $bag->to_branch }}
                        </div>
                    </div>
                    Consignments
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                        @foreach($bag->bag_consignments as $consignment)                        
                                {{ $consignment->awb }}
                        @endforeach
                        </div>
                    </div>
<hr>
                    <form class="form-horizontal" role="form" method="POST" action="/bagging/{{ $bag->id }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

 
                        <div class="form-group{{ $errors->has('action') ? ' has-error' : '' }}">
                            <label for="action" class="col-md-4 control-label">Action</label>

                            <div class="col-md-6">
                                <select id="action" class="form-control" name="action"  required autofocus>
                                    <option value="1"  >Add </option>
                                    <option value="0"  >Remove</option>
                                </select>
                                @if ($errors->has('action'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('action') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="bag_code" value="{{ $bag->bag_code }}" />
                        <input type="hidden" name="bag_id" value="{{ $bag->id }}" />

                        <div class="form-group{{ $errors->has('awb') ? ' has-error' : '' }}">
                            <label for="awb" class="col-md-4 control-label">AWB</label>

                            <div class="col-md-6">
                                <input id="awb" type="text" class="form-control" name="awb"  required autofocus>

                                @if ($errors->has('awb'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('awb') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>

                    </form>




                </div>



            </div>
        </div>
    </div>
</div>
@endsection
