@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Drs</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/drs">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('drs_code') ? ' has-error' : '' }}">
                            <label for="drs_code" class="col-md-4 control-label">Drs Code</label>

                            <div class="col-md-6">
                                <input id="drs_code" type="text" class="form-control" name="drs_code" value="{{ old('drs_code') }}" required autofocus>

                                @if ($errors->has('drs_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('drs_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('delivery_boy') ? ' has-error' : '' }}">
                            <label for="delivery_boy" class="col-md-4 control-label">Delivery Boy Name</label>

                            <div class="col-md-6">
                                <input id="delivery_boy" type="text" class="form-control" name="delivery_boy" value="{{ old('delivery_boy') }}" required autofocus>

                                @if ($errors->has('delivery_boy'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('delivery_boy') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('delivery_date') ? ' has-error' : '' }}">
                            <label for="datepicker" class="col-md-4 control-label">Date of Delivery</label>

                            <div class="col-md-6">
                                <input id="datepicker" type="text" placeholder="yyyy-mm-dd" class="form-control" name="delivery_date" value="{{ old('delivery_date') }}" required autofocus>

                                @if ($errors->has('delivery_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('delivery_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br /><br />
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
$.noConflict();  //Not to conflict with other scripts
jQuery(document).ready(function($) {
    $( "#datepicker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat : 'yy-mm-dd',
        defaultDate: new Date(),
    }).datepicker("setDate", date);
  } );
</script>


@endsection
