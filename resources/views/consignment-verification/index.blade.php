@extends('layouts.app')

@section('content')
<div class="container-fluid">
   <div id="page-wrapper" class="page-wrapper-cls">
      <div id="page-inner">
         <div class="row">
            <div class="col-md-12">
               <div class="alert alert-warning">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Step 1 : HUB : In Scan Consignment
                        <div class="pull-right">
                          <img src="{{URL::asset('/image/new_icon.gif')}}"  />
                          <span><a href="/consignment-verify"><strong>HUB : In Scan with Instant Search</strong></a></span>
                        </div>
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/consignment-verification">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="show" />
                           <!--
                           <div class="form-group">
                              <label for="exampleInputEmail1">Waybill</label> 
                              <div class="col-md-6">
                              <textarea name="txtwaybill" rows="2" cols="20" id="" class="form-control" placeholder="Scan waybill and press submit button." style="height:150px;"></textarea><br>
                              </div>
                           </div>
                           -->
                        <div class="form-group{{ $errors->has('txtwaybill') ? ' has-error' : '' }}">
                            <label for="txtwaybill" class="col-md-4 control-label">Waybill</label>

                            <div class="col-md-6">
                                <textarea id="txtwaybill" type="text" rows="6" cols="20"  class="form-control" name="txtwaybill" placeholder="Please Enter/Scan a Waybill Number" required  autofocus></textarea>

                                @if ($errors->has('txtwaybill'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('txtwaybill') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                        </form>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Message Log
                     </div>
                     <div class="panel-body">
                        @if(isset($consignment)) 
                        <div class="col-md-6 col-md-offset-4">
                        Consignment Found for Waybill no: <strong><a href="/consignments/{{ $consignment->id }}" target="_blank">{{ $consignment->awb }}</a></strong>  <br /><br />
                        </div>
                        @foreach($consignment->consignment_updates as $update)
                        <?php
                           if($update->current_status == \Config::get('constants.inScanhub'))
                             $found = 1;
                           ?>
                        @endforeach
                        @if(!isset($found)) 
                        <form class="form-horizontal" role="form" method="POST" action="/consignment-verification">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="verify" />
                           <input type="hidden" name="txtwaybill" value="{{ $consignment->awb }}" />

                        <!--
                        <div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
                            <label for="weight" class="col-md-4 control-label">Weight (in gram)</label>

                            <div class="col-md-6">
                                <input id="weight" type="text" class="form-control" name="weight" required  autofocus>

                                @if ($errors->has('weight'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-4 control-label">Price (in Rs)</label>

                            <div class="col-md-6">
                                <input id="price" type="text" class="form-control" name="price" required   autofocus>

                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>

                           
                        </div>
                            Note: In case you don't want to enter weight, just enter :  NA 
                            <br />
                        
                        -->
                           <div class="col-md-6 col-md-offset-4">
                            

                              <button type="submit" class="btn btn-sm btn-success btn-create">Verify In Scan</button>                           
                           </div>

                        </form>
                        @else
                        <br />
                        <div class="col-md-6 col-md-offset-4">
                        Already Verified !
                        </div>
                        @endif
                        @elseif(isset($awb))
                        <div class="col-md-6 col-md-offset-4">
                        Consignment was not found for Waybill no : <strong>{{ $awb }}</strong><br /><br />
                        </div>
                        @endif
                        <div class="form-group">
                           <span id="ctl00_ContentPlaceHolder1_lblmsg" style="color:Red;"></span>
                        </div>
                     </div>
                  </div>
                  <!--
                     <div class="panel panel-default">
                                        <div class="panel-heading">
                                          RTO  Consignments Verified Today
                                        </div>
                                        <div class="panel-body">
                                       
                     <div class="form-group">
                      <div>
                     
                     </div>
                     </div>
                     
                     -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>

@endsection
