@extends('layouts.app')



@section('content')

<div class="container-fluid">



    <div class="row">

      <div class="col-xs-12 col-xs-offset-1">

        <form method="GET" action="/reverse-por" >

           <div class="form-group">

              <label for="to_branch" class="col-md-1 control-label">Created Date:</label>

              <div class="col-md-2">

                 <input class="form-control" type="text" name="date" id="datepicker" placeholder="yyyy-mm-dd">

              </div>

           </div>

           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>

        </form>

      </div>

    </div>



    <div class="row">

      <div class="col-xs-8 col-xs-offset-4">

         <img id="loading" src="{{URL::asset('/image/loading-dash.gif')}}" style="display:none" alt="Loading..." height="200" width="250">

      </div>

    </div>



    <div class="row"  id="search-results">



        <div class="col-md-11 col-md-offset-1">



            <h4>Toatal Records Found : {{ $pods->total() }} </h4>





            <div class="panel panel-default panel-table">

              <div class="panel-heading">

                <div class="row">

                  <div class="col col-xs-6">

                    <h3 class="panel-title">DTO</h3>

                  </div>

                  <div class="col col-xs-6 text-right">

                    <a href="/reverse-por/create"><button type="button" class="btn btn-sm btn-primary btn-create">Add New</button></a>

                  </div>







                </div>

              </div>

              <div class="panel-body">

                



                    <table class="table table-striped table-bordered table-list">

                      <thead>

                        <tr>

                            <th><em class="fa fa-cog"></em></th>

                            <!--<th class="hidden-xs">ID</th>-->

                            <th>DTO Code</th>

                            <th>Delivery Boy</th>

                            <th>Delivery Date</th>                            

                            <th>Created Date</th>     

                            <th>Updated Date</th>                                                                        

                        </tr> 

                      </thead>



                @forelse($pods as $pod)



                      <tbody>  

                              <tr>

                                <td align="center">

                                  <a class="btn btn-default" href="/reverse-por/{{ $pod->id }}/edit" ><em class="fa fa-pencil"></em></a>

                                  <form action="/reverse-por/{{ $pod->id }}" method="POST" class="pull-right" onsubmit="return confirm('Are you sure you want to delete?')" >{{ csrf_field() }} {{ method_field('DELETE') }}<button type="submit" class="btn btn-danger"><em class="fa fa-trash" ></em></button></form>

                                </td>

                                <td>{{ $pod->por_code }}</td>

                                <td>{{ $pod->delivery_boy }}</td>

                                <td>{{ $pod->delivery_date }}</td>                                

                                <td>{{ $pod->created_at->format('Y-m-d g:i A') }}</td>                            

                                <td>{{ $pod->updated_at->format('Y-m-d g:i A') }}</td>                            

                              </tr>



                      </tbody>

                @empty

                    No bags.



                @endforelse



                    </table>



              

              </div>

              <div class="panel-footer">



                <div class="row">

                  

                  <div class="col col-xs-4">Page {{ $pods->currentPage() }} of {{ $pods->lastPage()  }}

                  </div>

                  <div class="col col-xs-8 pull-right">

                    <ul class="pagination hidden-xs pull-right">



                       {{ $pods->appends(request()->input())->links() }} 



                    </ul>



                  </div>

                </div>

              </div>

            </div>



    </div>

</div>





<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>





  <script>

$.noConflict();  //Not to conflict with other scripts

jQuery(document).ready(function($) {





      var getUrlParameter = function getUrlParameter(sParam) {

      var sPageURL = decodeURIComponent(window.location.search.substring(1)),

          sURLVariables = sPageURL.split('&'),

          sParameterName,

          i;



      for (i = 0; i < sURLVariables.length; i++) {

          sParameterName = sURLVariables[i].split('=');



          if (sParameterName[0] === sParam) {

              return sParameterName[1] === undefined ? true : sParameterName[1];

          }

      }

  };



if(getUrlParameter('date')){

var date = getUrlParameter('date');



}else{

var date = new Date();

}





    $( "#datepicker" ).datepicker({

        changeMonth: true,

        changeYear: true,

        dateFormat : 'yy-mm-dd',

        defaultDate: new Date(),

    }).datepicker("setDate", date);

  } );

  </script>

@endsection

