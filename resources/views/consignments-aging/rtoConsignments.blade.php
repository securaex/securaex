@extends('layouts.app')
@section('content')
<div class="container-fluid">


<div class="row"  id="search-results">
   <div class="col-md-11 col-md-offset-1">
      <h4>Total Records Found : {{ $consignments->total() }} </h4>
      <p>
      <div class="row">  
      <form method="GET" action="/rto-consignments-aging" >

<div class="row">
        <div class="col-md-4">

         <div class="form-group">
            <label for="to_branch" class="col-md-6 control-label">Sort By Branch:</label>
            <div class="col-md-6">
               <select id="to_branch" class="form-control" name="branch"   autofocus>
                  <option value="">None</option>
                  @foreach($branches as $branch)
                  <option value="{{ ucfirst($branch->username) }}" @if(isset($_GET['branch']) &&  ($_GET['branch'] == ucfirst($branch->username))) selected  @endif>{{ ucfirst($branch->username) }}</option>
                  @endforeach
               </select>
            </div>
         </div>
        </div>
        
        <div class="col-md-4"> 
         <div class="form-group">
            <label for="customer_name" class="col-md-6 control-label">Sort By Customer:</label>
            <div class="col-md-6">
               <select id="customer_name" class="form-control" name="customer_name"   autofocus>
                  <option value="">None</option>
                  @foreach($customers as $customer)
                  <option value="{{ $customer->name }}" @if(isset($_GET['customer_name']) &&  ($_GET['customer_name'] == $customer->name)) selected  @endif >{{$customer->name}}</option>
                  @endforeach
               </select>
            </div>
         </div>
        </div>
        <div class="col-md-4">
         <div class="form-group">
            <label for="customer_name" class="col-md-6 control-label">Sort By Current Status:</label>
            <div class="col-md-6">
               <select id="current_status" class="form-control" name="current_status"   autofocus>
                  <option value="">None</option>
                 <?php $status = \Config::get('constants'); unset($status['deliveryStatus1'], $status['deliveryStatus4'], $status['notCollected'], $status['collected'], $status['inScanhubRev'], $status['outScanToClient'], $status['reverseFinalstatus'], $status['rtoProcess'], $status['rtoInitiated'], $status['rtoInitiatedbyBranch'], $status['rtoIntransittoHub'], $status['rtoVerifiedbyHub'], $status['rtoCompletedbyHub'], $status['rtoFinalstatus']);  foreach ($status as $key => $value) {  ?>
                  <option value="{{ $value }}" @if(isset($_GET['current_status']) &&  ($_GET['current_status'] == $value)) selected  @endif  >{{$value}}</option>

                 <?php } ?>

               </select>
            </div>
         </div>
         
        </div>

</div>
<div class="row" style="margin-top:10px;">

        <div class="col-md-4">
        <div class="form-group">
            <label for="aging" class="col-md-6 control-label">Sort By Age:</label>
            <div class="col-md-6">
               <select id="aging" class="form-control" name="aging"   autofocus>
                  <option value="">None</option>
                  <option value="2 Days" @if(isset($_GET['aging']) &&  ($_GET['aging'] == "2 Days")) selected  @endif>2 Days</option>
                  <option value="More Than 2 Days" @if(isset($_GET['aging']) &&  ($_GET['aging'] == "More Than 2 Days")) selected  @endif>More Than 2 Days</option>
               </select>
            </div>
         </div>
         
        </div>

         
        <div class="col-md-4">
         <div class="form-group">
          <div class="col-md-4">
           <button type="submit" class="btn btn-sm btn-primary btn-create">Filter</button>
          </div>
         </div>
        </div>
</div>

      </form>
      </div>      
      </p>
      <br />
      <div class="panel panel-default panel-table">
         <div class="panel-heading">
            <div class="row">
               <div class="col col-xs-6">
                  <h3 class="panel-title">Aged RTO Consignments</h3>
               </div>
               <div class="col col-xs-6 text-right">
                  
                  <a href="/rto-consignments-aging/exportExcelRto?branch=<?php if(isset($_GET['branch'])) { echo  $_GET['branch']; } ?>&customer_name=<?php if(isset($_GET['customer_name'])) { echo  $_GET['customer_name']; } ?>&current_status=<?php if(isset($_GET['current_status'])) { echo  $_GET['current_status']; } ?>&aging=<?php if(isset($_GET['aging'])) { echo  $_GET['aging']; } ?>"><button type="button" class="btn btn-sm btn-primary btn-create">Export</button></a>
               </div>
            </div>
         </div>
         <div class="panel-body">
            <table class="table table-striped table-bordered table-list">
               <thead>
                  <tr>
                     <!--<th><em class="fa fa-cog"></em></th>
                     <th class="hidden-xs">ID</th>-->
                     <th>Age</th>
                     <th>Soft Data Upload Date</th>
                     <th>AWB</th>
                     <th>Customer Name</th>
                     <th>Payment Type</th>
                     <th>Amount</th>
                     <th>Pincode</th>
                     <th>Last Updated BY</th>
                     <th>Last Updated AT</th>
                     <th>Current Status</th>
                     <th>Branch</th>
                  </tr>
               </thead>
               @forelse($consignments as $consignment)
               <tbody>
                  <tr>
                     <td>
                         @if(isset($consignment->consignment_updates))
                         <?php
						 	$created_at="";
							 foreach($consignment->consignment_updates as $update)
							{
								 if($update->current_status == "Process to be RTO")
									 $created_at = $update->created_at;
							}
							echo $created_at->diffInDays()." Days";
						?>	
                         @endif
                     </td>
                     <td>
                         @if(isset($consignment->consignment_updates))
                         <?php
						 	$created_at="";
							 foreach($consignment->consignment_updates as $update)
							{
								 if($update->current_status == "Process to be RTO")
									 $created_at = $update->created_at;
							}
							echo $created_at->format('Y-m-d g:i A');
						?>	
                         @endif
                     </td>
                     <td>{{ $consignment->awb }}</td>
                     <td>{{ $consignment->customer_name }}</td>
                     <td>{{ $consignment->payment_mode }}</td>
                     <td>
                         @if(strtoupper($consignment->payment_mode)=="COD")
                            {{ $consignment->price }}
                         @else
                            {{ 0 }}
                         @endif      
                     </td>
                     <td>{{ $consignment->pincode }}</td>
                     <td>{{ $consignment->last_updated_by }}</td>
                     <td>{{ $consignment->updated_at->format('Y-m-d g:i A') }}</td>
                     <td>{{ $consignment->current_status }}</td>
                     <td>{{ $consignment->branch }}</td>
                  </tr>
               </tbody>
               @empty
               No Consignments.
               @endforelse
            </table>
         </div>
         <div class="panel-footer">
            <div class="row">
               <div class="col col-xs-4">Page {{ $consignments->currentPage() }} of {{ $consignments->lastPage()  }}
               </div>
               <div class="col col-xs-8 pull-right">
                  <ul class="pagination hidden-xs pull-right">
                     {{ $consignments->appends(request()->input())->links() }} 
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection