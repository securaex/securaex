@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                   Caller Feedbak
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/caller-consignments/{{ $consignment->id }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group{{ $errors->has('awb') ? ' has-error' : '' }}">
                            <label for="awb" class="col-md-4 control-label">AWB</label>

                            <div class="col-md-6">
                                <input id="awb" type="text" class="form-control" name="awb" value="{{ $consignment->awb }}" readonly disabled >

                                @if ($errors->has('awb'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('awb') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        <div class="form-group{{ $errors->has('branch') ? ' has-error' : '' }}">
                            <label for="branch" class="col-md-4 control-label">Branch</label>

                            <div class="col-md-6">


                                <input readonly id="branch" type="text" class="form-control" name="branch" value="{{ $consignment->branch }}" disabled >
                                @if ($errors->has('branch'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('branch') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('consignee') ? ' has-error' : '' }}">
                            <label for="consignee" class="col-md-4 control-label">Consignee</label>

                            <div class="col-md-6">
                                
								<input readonly id="consignee" type="text" class="form-control" name="consignee" value="{{ $consignment->consignee }}" disabled >
                                @if ($errors->has('consignee'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('consignee') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('consignee_address') ? ' has-error' : '' }}">
                            <label for="consignee_address" class="col-md-4 control-label">Consignee Address</label>

                            <div class="col-md-6">
                                <textarea class="form-control" rows="4" name="consignee_address" readonly disabled >{{ $consignment->consignee_address }}</textarea>

                                @if ($errors->has('consignee_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('consignee_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        

                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ $consignment->mobile }}" readonly disabled >

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            <label for="remarks" class="col-md-4 control-label">Remarks</label>

                            <div class="col-md-6">
                                <textarea class="form-control" rows="4" name="remarks" readonly disabled >{{ $consignment->remarks }}</textarea>

                                @if ($errors->has('remarks'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('remarks') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('caller_remarks') ? ' has-error' : '' }}">
                            <label for="caller_remarks" class="col-md-4 control-label">Caller Remarks</label>

                            <div class="col-md-6">
								<textarea autofocus class="form-control" rows="5" name="caller_remarks" >{{ $consignment->caller_remarks }}</textarea>
                                
                            </div>
                        </div>
							<input type="hidden" name="reviewed" value="1" >
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
