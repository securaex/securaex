@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div id="page-wrapper" class="page-wrapper-cls">
        <div id="page-inner">

            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-warning">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                RTO Bag Dispatch
                            </div>
                            <div class="panel-body">

                     @if(Session::has('success_message'))
                         <div class="alert alert-success"><em> {!! session('success_message') !!}</em></div>
                     @endif

                     @if(Session::has('error_message'))
                         <div class="alert alert-danger"><em> {!! session('error_message') !!}</em></div>
                     @endif

                              
                           <form class="form-horizontal" role="form" method="POST" action="dispatchbag">
                           {{ csrf_field() }}
                               
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Bag Code</label>

                                    <textarea name="bag_code" rows="2" cols="20" id="ctl00_ContentPlaceHolder1_txtbagcode" class="form-control"></textarea>
                                    <br>
                                    <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator1" style="color:Red;visibility:hidden;">Enter Bag code</span>
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="ctl00$ContentPlaceHolder1$btnsbmit" value="Submit" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnsbmit&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btnsbmit" class="btn btn-info">
                                </div>

                            </div>

                           </form>
                            <div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection
