@extends('layouts.app')



@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4"><div class="no-print"><button class="print_drs" onclick="window.print();return false;" ></button><h5 class="print_text">Print This Page</h5></div></div>
    <div class="col-md-4"></div>
    <div class="col-md-4"><img src="{{URL::asset('/image/Secura-Ex-logo-final-large.png')}}"  /></div>
  </div>
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <h3>SecuraEx Mover</h3>
      <h3>POR Sheet</h3>
    </div>
  </div>
  <br />
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
    <table class="table borderless">
  <tr>
    <td style="text-align:left">
    <div style="float:right"><strong>Dated: {{$dateToday}}</strong></div>
    <p>To,</p>
    <p>{{ $pod->to_client }},</p>
    <p>{{ $pod->client_address }}</p>
    <p>&nbsp;</p>
    <p><strong>Subject:</strong> Handover of reverse shipment.</p>
    <p>Dear Sir,</p>
<p>The following shipments are being handed over to you. </p>
<p>Please acknowledge the reciept of these shipments.</p>
<p>&nbsp;</p>
    <p>&nbsp;</p>
    </td>
    
  </tr>
  <tr>
    <td><div style="float:left; ">
    <table width="100%" class="table table-bordered">
    <tr>
    <th>#Sr No</th>
    <th>AWB Number</th>
    <th>Order Number</th>
    <th>Product Description</th>
    </tr>
    <?php $sn = 1; ?>
        @foreach($pod->reverse_por_consignments as $consignment)
        <tr>
          <th scope="row">{{ $sn++ }}</th>
          <td>{{ $consignment->awb }}</td>
          <td>{{ $consignment->order_number }}</td>
          <td>{{ $consignment->item_description }}</td>
        </tr>
        @endforeach
        
        <tr>
        <td>&nbsp;</td>
          <td> <strong>Total Consignment</strong></td>
          <td><strong>{{ $sn-1 }}</strong></td>
        </tr>
    </table>
    <!--<p>&nbsp;</p>
    <strong>Date: {{$dateToday}}</strong>-->
    </div><div style="float:right; padding-top:200px; text-align:left">
    
    <p><strong>Signature OF Receiver:</strong></p>
    <p><strong>Name OF Receiver:</strong></p>
    <p><strong>Phone Number:</strong></p>
    <p><strong>Email ID:</strong></p>
    <p><strong>Stamp:</strong></p>
    
    </div></td>
    
  </tr>
</table>

      
    </div>
  </div>
  
  
</div>
@endsection 