@extends('layouts.app')



@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4"><div class="no-print"><button class="print_drs" onclick="window.print();return false;" ></button><h5 class="print_text">Print This Page</h5></div></div>
    <div class="col-md-4"></div>
    <div class="col-md-4"><img src="{{URL::asset('/image/Secura-Ex-logo-final-large.png')}}"  /></div>
  </div>
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <h3>Secura Logistic</h3>
      <h3>POD Sheet</h3>
    </div>
  </div>
  <br />
  <div class="row">
    <div class="col-md-12 col-md-offset-0">
      <table class="table table-bordered">
        <tbody>
          <tr>
            <th scope="row">Employee Name:</th>
            <td>{{ $pod->delivery_boy }}</td>
            <th scope="row">Date:</th>
            <td>{{ $pod->delivery_date }}</td>
            <th scope="row">Pod Sheet No.:</th>
            <td>{{ $pod->pod_code }}</td>
          <tr>
            <th scope="row">Route No.:</th>
            <td>{{ $pod->route_no }}</td>
            <th scope="row">No. of Deliveries</th>
            <td>{{ count($pod->pod_consignments) }}</td>
            <th scope="row">Vehicle Number:</th>
            <td>{{ $pod->vehicle_number }}</td>
          </tr>
          <tr>
            <th scope="row">Driver Name:</th>
            <td>{{ $pod->driver_name }}</td>
            <th scope="row">Mobile:</th>
            <td>{{ $pod->mobile }}</td>
            <th scope="row">Transporter Name:</th>
            <td>{{ $pod->transporter_name }}</td>
          </tr>
          <tr>
            <th scope="row">Vehicle Type:</th>
            <td>{{ $pod->vehicle_type }}</td>
            <th scope="row">Facility Name:</th>
            <td>{{ $pod->facility_name }}</td>
            <th scope="row"></th>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-md-offset-0">
      <table class="table table-bordered">
        <thead class="thead-inverse">
          <tr>
            <th>#Sr No</th>
            <th>AWB Number</th>
            <th>Consignee Name / Contact Number</th>
            <th>Address</th>
            <!--<th>COD Amount</th>-->
            <th>Signature/Remarks</th>
          </tr>
        </thead>
        <tbody>
          <?php $cod_sum = 0; $sn = 1; ?>
        @foreach($pod->pod_consignments as $consignment)
        <tr>
          <th scope="row">{{ $sn++ }}</th>
          <td>{{ $consignment->awb }}</td>
          <td>{{ $consignment->consignee }}/{{ $consignment->mobile }}</td>
          <td>{{ $consignment->consignee_address }}</td>
          <!--<td>{{ $consignment->collectable_value }}</td>-->
          <td></td>
          <?php $cod_sum += $consignment->collectable_value; ?>
        </tr>
        @endforeach
        </tbody>
        
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-md-offset-0">
      <table class="table table-bordered">
        <tbody>
          <tr>
            <th scope="row">Total Shipment:</th>
            <td>{{ count($pod->pod_consignments) }}</td>
            <th scope="row"></th>
            <td></td>
            <!--

            <th scope="row">Total Amount:</th>

            <td>{{ $cod_sum }}</td>

          -->
          <tr>
            <th scope="row">Delivered Shipment:</th>
            <td></td>
            <th scope="row">Undelivered Shipment:</th>
            <td></td>
          </tr>
          <tr>
            <th scope="row">Start KM:</th>
            <td></td>
            <th scope="row">End KM:</th>
            <td></td>
          </tr>
          <tr>
            <th scope="row">Remarks:</th>
            <td></td>
            <th scope="row"></th>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection 