@extends('layouts.app')

@section('content')
<div class="container-fluid">
   <div id="page-wrapper" class="page-wrapper-cls">
      <div id="page-inner">
         <div class="row">
            <div class="col-md-12">
               <div class="alert alert-warning">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Step 2 : HUB : Create Bagging
                     </div>
                     <div class="panel-body">
                     @if(Session::has('success_message'))
                         <div class="alert alert-success"><em> {!! session('success_message') !!}</em></div>
                     @endif

                     @if(Session::has('error_message'))
                         <div class="alert alert-danger"><em> {!! session('error_message') !!}</em></div>
                     @endif

                     <form class="form-horizontal" role="form" method="POST" action="/bagging">
                        {{ csrf_field() }}


                        <div class="form-group">
                           <label for="exampleInputEmail1">Bag Code</label> 

                                <select id="bag_code" onchange='document.getElementById("bagIdValue").value = this.options[this.selectedIndex].title;'  class="form-control" name="bag_code" value="{{ old('bag_code') }}" required autofocus>
                                 
                                 <option value="" >Please Select the Bag</option>

                                 @foreach($bags as $bag):
                                    
                                    <option value="{{ $bag->bag_code }}" title="{{ $bag->id }}" >{{ $bag->bag_code }}</option>
                                @endforeach
                                
                                </select>
                                <input id="bagIdValue" type="hidden" name="bag_id" />

                        </div>
                        <div class="form-group">
                           <label for="exampleInputEmail1">Enter Air WayBill Numbers</label> 
                           <div id="ctl00_ContentPlaceHolder1_UpdatePanel1">
                              <textarea name="awbs" rows="2" cols="20" required onchange="javascript:setTimeout('__doPostBack(\'ctl00$ContentPlaceHolder1$txtwaybill\',\'\')', 0)" id="ctl00_ContentPlaceHolder1_txtwaybill" class="form-control" style="height:100px;" placeholder="You can enter Multiple AWB no one after other on each line"></textarea><br>
                              <span id="ctl00_ContentPlaceHolder1_rq1" style="color:Red;visibility:hidden;">Enter waybill no</span>
                           </div>
                           <br>
                           <div id="ctl00_ContentPlaceHolder1_lbl">
                              <span id="ctl00_ContentPlaceHolder1_lblwaybillcounter" style="color:Red;"></span>
                           </div>
                        </div>
                        <div class="form-group">
                           <input type="submit" name="submit" value="Submit" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnsbmit&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btnsbmit" class="btn btn-info">
                        </div>
                     </div>
                  </form>
                  </div>
                  <div class="panel-body">
                     <div class="form-group">
                        <div style="margin-left:20px;float:left;">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

@endsection
