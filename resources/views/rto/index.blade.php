@extends('layouts.app')

@section('content')
<div class="container-fluid">
   <div id="page-wrapper" class="page-wrapper-cls">
      <div id="page-inner">
         <div class="row">
            <div class="col-md-12">
               <div class="alert alert-warning">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        File RTO
                     </div>
                     <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/rto">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="show" />
                          
                           <div class="form-group">
                              <label for="exampleInputEmail1" class="col-md-4 control-label">Status</label> 
                            <div class="col-md-6">

                                   <select id="status" onchange='document.getElementById("bagIdValue").value = this.options[this.selectedIndex].title;'  class="form-control" name="rto_reason" value="{{ old('status') }}" required autofocus>
                                    
                                    <option value="" >Undelivered Reasons For RTO (Return to origin)</option>

                                       <option value="House /Office Locked" title="" >House /Office Locked</option>
                                       <option value="COD amount not ready" title="" >COD amount not ready</option>
                                       <option value="Customer out of station" title="" >Customer out of station</option>
                                       <option value="Customer Refused to accept" title="" >Customer Refused to accept </option>
                                       <option value="Address Incorrect" title="" >Address Incorrect</option>
                                       <option value="Customer asking for future delivery" title="" >Customer asking for future delivery</option>
                                       <option value="Customer contact no and address not traceable" title="" >Customer contact no and address not traceable</option>
                                       <option value="Fake order Refused by Customer" title="" >Fake order Refused by Customer</option>
                                       <option value="ODA location" title="" >ODA location</option>
                                       <option value="Address Shifted" title="" >Address Shifted</option>
                                       <option value="Cod amount miss match" title="" >Cod amount miss match</option>
                                       <option value="Customer snatched the shipment forceibly" title="" >Customer snatched the shipment forceibly</option>
                                       <option value="Other remarks" title="" >Other remarks</option>
                                       <option value="Shipment Returned as per Vendor" title="" >Shipment Returned as per Vendor</option>

                                   
                                   </select>
                              </div>
                           </div>

                        <div class="form-group{{ $errors->has('remarks') ? ' has-error' : '' }}">
                            <label for="remarks" class="col-md-4 control-label">Remarks</label>

                            <div class="col-md-6">
                                <textarea id="remarks" class="form-control" name="remarks" placeholder="optional"  autofocus>{{ old('remarks') }}</textarea>

                                @if ($errors->has('remarks'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('remarks') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                           <div class="form-group">
                              <label for="exampleInputEmail1" class="col-md-4 control-label">Waybill</label> 
                              <div class="col-md-6">
                              <textarea name="rto_awb" rows="2" cols="20" id="ctl00_ContentPlaceHolder1_txtwaybill" class="form-control" placeholder="Scan waybill and press submit button." style="height:150px;" required></textarea><br>
                              <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator1" style="color:Red;visibility:hidden;">Enter Waybill</span>
                              </div>
                           </div>




                           <div class="form-group">
                            <div class="col-md-6 pull-right">
                              <input type="submit" name="ctl00$ContentPlaceHolder1$btnsbmit" value="Submit" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btnsbmit&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btnsbmit" class="btn btn-info">
                            </div>
                           </div>

                        </form>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Message Log
                     </div>
                     <div class="panel-body">

@if(Session::has('success_message'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Heads Up!</strong> {!!Session::get('success_message')!!}
        </div>
    @endif

@if(Session::has('error_message'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Heads Up!</strong> {!!Session::get('error_message')!!}
        </div>
    @endif

                        @if(isset($consignment)) 
                        Consignment Found for Waybill no: {{ $consignment->awb }} <br />
                        @foreach($consignment->consignment_updates as $update)
                        <?php
                           if($update->current_status == \Config::get('constants.inScanbranch'))
                             $found = 1;
                           ?>
                        @endforeach
                        @if(!isset($found)) 
                        <form class="form-horizontal" role="form" method="POST" action="/in-scan">
                           {{ csrf_field() }}
                           <input type="hidden" name="action"     value="verify" />
                           <input type="hidden" name="txtwaybill" value="{{ $consignment->awb }}" />
                           <div class="col col-xs-12 text-left">
                              <button type="submit" class="btn btn-sm btn-success btn-create">Verify In Scan</button>
                           </div>
                        </form>
                        @else
                        Already Verified !
                        @endif
                        @elseif(isset($awb))
                        Consignment was not found for Waybill no : {{ $awb }} 
                        @endif
                        <div class="form-group">
                           <span id="ctl00_ContentPlaceHolder1_lblmsg" style="color:Red;"></span>
                        </div>
                     </div>
                  </div>
                  <!--
                     <div class="panel panel-default">
                                        <div class="panel-heading">
                                          RTO  Consignments Verified Today
                                        </div>
                                        <div class="panel-body">
                                       
                     <div class="form-group">
                      <div>
                     
                     </div>
                     </div>
                     
                     -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>

@endsection
