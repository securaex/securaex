@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Pod</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="/pod">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('pod_code') ? ' has-error' : '' }}">
                            <label for="pod_code" class="col-md-4 control-label">Pod Code</label>

                            <div class="col-md-6">
                                <input id="pod_code" type="text" class="form-control" name="pod_code" value="{{ old('pod_code') }}" required autofocus>

                                @if ($errors->has('pod_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pod_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('delivery_boy') ? ' has-error' : '' }}">
                            <label for="delivery_boy" class="col-md-4 control-label">Delivery Boy Name</label>

                            <div class="col-md-6">
                                <input id="delivery_boy" type="text" class="form-control" name="delivery_boy" value="{{ old('delivery_boy') }}" required autofocus>

                                @if ($errors->has('delivery_boy'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('delivery_boy') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="created_by" value="{{ Auth::user()->username }}" />

                        <div class="form-group{{ $errors->has('delivery_date') ? ' has-error' : '' }}">
                            <label for="datepicker" class="col-md-4 control-label">Date of Delivery</label>

                            <div class="col-md-6">
                                <input id="datepicker" type="text" placeholder="yyyy-mm-dd" class="form-control" name="delivery_date" value="{{ old('delivery_date') }}" required autofocus>

                                @if ($errors->has('delivery_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('delivery_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('route_no') ? ' has-error' : '' }}">
                            <label for="route_no" class="col-md-4 control-label">Route No</label>

                            <div class="col-md-6">
                                <input id="route_no" type="text" class="form-control" name="route_no" value="{{ old('route_no') }}" required autofocus>

                                @if ($errors->has('route_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('route_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('vehicle_number') ? ' has-error' : '' }}">
                            <label for="vehicle_number" class="col-md-4 control-label">Vehicle Number</label>

                            <div class="col-md-6">
                                <input id="vehicle_number" type="text" class="form-control" name="vehicle_number" value="{{ old('vehicle_number') }}" required autofocus>

                                @if ($errors->has('vehicle_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('vehicle_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('driver_name') ? ' has-error' : '' }}">
                            <label for="driver_name" class="col-md-4 control-label">Driver Name</label>

                            <div class="col-md-6">
                                <input id="driver_name" type="text" class="form-control" name="driver_name" value="{{ old('driver_name') }}" required autofocus>

                                @if ($errors->has('driver_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('driver_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for="mobile" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" required autofocus>

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('transporter_name') ? ' has-error' : '' }}">
                            <label for="transporter_name" class="col-md-4 control-label">Transporter Name</label>

                            <div class="col-md-6">
                                <input id="transporter_name" type="text" class="form-control" name="transporter_name" value="{{ old('transporter_name') }}" required autofocus>

                                @if ($errors->has('transporter_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('transporter_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('vehicle_type') ? ' has-error' : '' }}">
                            <label for="vehicle_type" class="col-md-4 control-label">Vehicle Type</label>

                            <div class="col-md-6">
                                <input id="vehicle_type" type="text" class="form-control" name="vehicle_type" value="{{ old('vehicle_type') }}" required autofocus>

                                @if ($errors->has('vehicle_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('vehicle_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('facility_name') ? ' has-error' : '' }}">
                            <label for="facility_name" class="col-md-4 control-label">Facility Name</label>

                            <div class="col-md-6">
                                <input id="facility_name" type="text" class="form-control" name="facility_name" value="{{ old('facility_name') }}" required autofocus>

                                @if ($errors->has('facility_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('facility_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <br /><br />
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
$.noConflict();  //Not to conflict with other scripts
jQuery(document).ready(function($) {
    $( "#datepicker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat : 'yy-mm-dd',
        defaultDate: new Date(),
    }).datepicker("setDate", date);
  } );
</script>


@endsection
