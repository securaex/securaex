<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/home/exportExcel', 'HomeController@exportExcel');
/*
Route::get('database/clean', 'DatabaseController@clean');
*/
Route::get('consignments/exportExcel', 'ConsignmentController@exportExcel');
Route::post('consignments/repeat/{id}', 'ConsignmentController@repeat');
Route::delete('consignments/reset/{id}', 'ConsignmentController@reset');
Route::post('consignments/resetbranch/{id}', 'ConsignmentController@resetbranch');

Route::resource('consignments', 'ConsignmentController');

Route::get('bagging/{id}/editStatus', 'BaggingController@editStatus');
Route::put('/bagging/updateStatus/{id}', 'BaggingController@updateStatus');
Route::get('bagging/{id}/verifyBag', 'BaggingController@verifyBag');
Route::put('/bagging/updateBagVerification/{id}', 'BaggingController@updateBagVerification');
Route::resource('bagging', 'BaggingController');
Route::resource('customers', 'CustomerController');


Route::resource('in-scan', 'InscanController');
Route::resource('out-scan', 'OutscanController');
Route::resource('out-scan-pod', 'OutscanpodController');

Route::resource('status', 'StatusController');
Route::get('rto/listing', 'RtoController@listing');
Route::post('rto/receive/{id}', 'RtoController@receive');
Route::post('rto/complete/{id}', 'RtoController@complete');

Route::get('rto/upload', 'RtoController@upload');
Route::post('rto/rto_process', 'RtoController@rto_process');

Route::get('rto/final-status', 'RtoController@final_status');
Route::post('rto/final-status-process', 'RtoController@final_status_process');

Route::resource('rto', 'RtoController');

Route::resource('pod', 'PodController');
Route::resource('podsheets', 'PodSheetsController');

Route::resource('bags', 'BagController');
Route::resource('consignment-verification', 'ConsignmentVerificationController');
Route::resource('consignment-verify', 'ConsignmentVerifyController');

Route::get('bagupdate/manage', 'BagUpdateController@manage');
Route::post('bagupdate/dispatchbag', 'BagUpdateController@dispatchbag');
Route::get('bagupdate/verify', 'BagUpdateController@verify');
Route::post('bagupdate/verifybag', 'BagUpdateController@verifybag');
Route::resource('bagupdate', 'BagUpdateController');



Route::match(array('GET', 'POST'),'tracking/consignment', 'TrackingController@consignment');
Route::post('tracking/exportExcel', 'TrackingController@exportExcel');

Route::resource('tracking', 'TrackingController');


Route::get('api/importExport', 'MaatwebsiteDemoController@importExport');
Route::get('api/downloadExcel/{type}', 'MaatwebsiteDemoController@downloadExcel');
Route::post('api/importExcel', 'MaatwebsiteDemoController@importExcel');

Route::get('shipment/history', 'ShipmentHistoryController@index');
Route::post('shipment/importExcel', 'ShipmentHistoryController@importExcel');


Route::get('/ajax-search-consignments', 'AjaxController@searchConsignments');
Route::get('/ajax-verify-consignment', 'AjaxController@verifyConsignment');
