<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*Forward Consignment Api Code*/

//Route::group(['middleware' => ['auth:api']], function () {
Route::post('/pushData', 'MaatwebsiteDemoController@pushData');
   // });

// get list of tasks
Route::get('getconsignment','WebserviceController@index');
// get specific task
Route::get('getaconsignment/{awb}','WebserviceController@showAwb');
Route::get('getconsignment/{id}','WebserviceController@show');
// delete a task
//Route::delete('deleteconsignments/{id}','WebserviceController@destroy');
// update existing task
Route::put('updateconsignment','WebserviceController@store');
// create new task
Route::post('addconsignment','WebserviceController@store');


/*Reverse Consignment Api Code*/

//Route::group(['middleware' => ['auth:api']], function () {
Route::post('/pushReverseData', 'MaatwebsiteDemoController@pushReverseData');
   // });

// get list of tasks
Route::get('getreverseconsignments','WebserviceController@indexReverse');
// get specific task
Route::get('getreverseconsignment/{id}','WebserviceController@showReverse');
// delete a task
//Route::delete('deletereverseconsignments/{id}','WebserviceController@destroyReverse');
// update existing task
Route::put('updatereverseconsignment','WebserviceController@storeReverse');
// create new task
Route::post('addreverseconsignment','WebserviceController@storeReverse');

